#include "stdint.h"
#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "stm32f30x.h"
#include "sabertooth.h"

void init_sabertooth(GPIO_TypeDef *GPIOx, uint16_t pin, uint8_t pinSource)
{
	if(GPIOx == GPIOA)
	{
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	}
	else if(GPIOx == GPIOB)
	{
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	}
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	
	GPIO_PinAFConfig(GPIOx, pinSource, GPIO_AF_7);

	// Use either PA2 or PB3
	GPIO_InitStruct.GPIO_Pin = pin;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOx, &GPIO_InitStruct);
	
	USART_InitStruct.USART_BaudRate = 9600;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStruct);
	USART_Cmd(USART2, ENABLE);
}

// Power is from 0 - 63. I do NOT recommend going above 25
void motor_speed(uint8_t motor, uint8_t power, uint8_t direction)
{
	uint8_t command;
	uint8_t magnitude;
	power = power << 1;
	magnitude = power >> 1;

	if(direction == 2)
	{
		command = 63 - magnitude;
		if(motor == 2)
		{
			command-=2;
		}
		else
		{
			command+=1;
		}
	}
	else if(direction == 1)
	{
		command = 64 + magnitude;
		if(motor == 2)
		{
			command++;
		}
	}
	if(motor == 2)
		{
			command |= 0x80;
		}

	//command = constrain(command, 1, 254);
	USART2_putc(command);
}

void USART2_putc(char c)
{
	while((USART2->ISR & USART_FLAG_TXE) == 0);
	USART2->TDR = c;
	return;
}

int8_t constrain(int8_t input, int8_t lowerBound, int8_t upperBound)
{
	if(input > upperBound)
	{
		return upperBound;
	}
	else if(input < lowerBound)
	{
		return lowerBound;
	}
	else
	{
		return (int8_t)input;
	}
}

uint8_t get_direction(int8_t input)
{
//	if()
//	{
//		return 1;
//	}
//	else if()
//	{
//		return 2;
//	}
	return 0;
}
