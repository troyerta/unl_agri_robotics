#include "stdint.h"
#include "stdio.h"
#include "main.h"
#include "stm32f30x.h"
#include "led.h"
#include "usart.h"
#include "vector.h"

#define MAX_SPEED         1
#define WHEEL_RADIUS      6.25f
#define WHEEL_CIRCUM      (float)(2f*3.14159f*WHEEL_RADIUS)
#define TIMCOUNTS_PER_RPM 3.245566f // Basically the motor's response to the duty cycle given to motor driver
#define RPM_PER_M_PER_S   152.79f   // Due to wheel circumference
//#define RPM_PER_M_PER_S   (float)((MAX_SPEED*100)/WHEEL_CIRCUM)
#define TIM_PERIOD_TOP    96000
#define TIM_DUTY_50       48000
#define TIM_MIN           42000
#define TIM_MAX           54000

static char msg[200];

// 32-Bit TIM2 as PWM generator
void TIM2_Init_PWM(void)
{
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
  
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Prescaler = 1;
  TIM_TimeBaseStructure.TIM_Period = 96000 - 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up ;
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

  TIM_OCInitTypeDef TIM_OCInitStructure;

  // Adjust the Pulse from 32,000 for full reverse, 48,000 for stop, and 64,000 for full forward
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 48000; // preset pulse width to half of pwm_period
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // Pulse polarity
  TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
  
  // Setup two channels
  // Channel 3, pin PA9
  TIM_OC3Init(TIM2, &TIM_OCInitStructure);
  TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable );

  // Channel 4, pin PA10
  TIM_OC4Init(TIM2, &TIM_OCInitStructure);
  TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable );

  // Startup the timer
  TIM_ARRPreloadConfig(TIM2, DISABLE);
  TIM_CtrlPWMOutputs(TIM2, ENABLE);
  TIM_Cmd(TIM2, ENABLE);
  
  PWM_Pins_Init();
}

// Setup PA9 and PA10 as TIM2 outputs, CH3, and CH4 respectively
void PWM_Pins_Init(void)
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  GPIO_InitTypeDef GPIO_InitStruct;
  
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_10 );  // Ch3
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_10 ); // Ch4

}

// The number range from 18,000 (full reverse)
// to 27,000 (stop) to 36,000 (full forward)
// is placed into the capture/compare register
void TIM2_PWM3_setPeriod(uint32_t period)
{
  TIM2->CCR3 = period;
}

void TIM2_PWM4_setPeriod(uint32_t period)
{
  TIM2->CCR4 = period;
}

/* Function that maps m/s to motor RPM to a PWM value
 * Units are Revolutions per minute
 *
 *
 */
void VECTOR_CMD(float M1Speed, float M2Speed)
{
  int32_t magnitude1;
  int32_t magnitude2;
  uint32_t magnitude1_fixed;
  uint32_t magnitude2_fixed;
  float rpm1;
  float rpm2;
  
  rpm1 = (float)(M1Speed*RPM_PER_M_PER_S);
  rpm2 = (float)(M2Speed*RPM_PER_M_PER_S);
  
  magnitude1 = (uint32_t)((int32_t)(rpm1*TIMCOUNTS_PER_RPM) + 48000);
  magnitude2 = (uint32_t)((int32_t)(rpm2*TIMCOUNTS_PER_RPM) + 48000);
  
  magnitude1_fixed = constrain_PWM(magnitude1, 42000, 54000);
  magnitude2_fixed = constrain_PWM(magnitude2, 42000, 54000); 
  
  TIM2_PWM3_setPeriod(magnitude1_fixed);
  TIM2_PWM4_setPeriod(magnitude2_fixed);
}

// min is 32,000 at minimum, and max is 64,000 at maximum
uint32_t constrain_PWM(uint32_t input, uint32_t min, uint32_t max)
{
  uint32_t output;
  
  if(input < min)
  {
    output = min;
  }
  else if(input > max)
  {
    output = max;
  }
  else
  {
    output  = input;
  }
  
  return output;
}

void printState_Vector(void)
{
  snprintf(msg, 200, "%sVector Motor Driver:%s\r\nCLK\t\t32 MHz\r\nPeriod\t\t96000\r\nCCR3\t\t%u\r\nCCR4\t\t%u\r\n", UPurple, none, TIM2->CCR3, TIM2->CCR4);
  USART1_puts(msg);
}
