#include "main.h"
#include "usart.h"
#include "stm32f30x.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "can.h"
#include "utils.h"
#include "led.h"
#include "i2c.h"
#include "sabertooth.h"
#include "controller.h"
#include "vector.h"
#include "adc.h"
#include "tim3.h"

#include "FuzzyRule.h"
#include "FuzzyComposition.h"
#include "Fuzzy.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzyOutput.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzySet.h"
#include "FuzzyRuleAntecedent.h"

#define SIZE (uint8_t)120

uint8_t status;
uint16_t dist = 0;
uint8_t i = 0;
uint16_t j = 0;
uint16_t CAN1_ID;
uint8_t CAN1_DATA0;
uint8_t CAN1_DATA1;
uint8_t CAN1_DATA2;
uint8_t CAN1_DATA3;
uint8_t CAN1_DATA4;
uint8_t CAN1_DATA5;
uint8_t CAN1_DATA6;
uint8_t CAN1_DATA7;

// 10% of max fwd speed
//const uint16_t bias = VECTOR_FULL_FWD / (uint16_t)10;

float distR = 10.33f;
float distL = 25.46f;
float DistInput = 0.0f;
float rowInput = 0.0f;
float steering = 0.0f;

__IO uint32_t doneSampling = 0;;
char msg[SIZE];
__IO uint16_t ADC1ConvertedValue = 0;
__IO uint16_t ADC1ConvertedVoltage = 0;
__IO float ADC1ConvertedVoltageF = 0.0f;

uint32_t unitDelay = 200;
CanTxMsg TxMessage;
CanRxMsg RxMessage;
uint8_t buf_size;
uint16_t currentTime;
uint16_t previousTime;
uint16_t deltaTime;
uint16_t frameCounter;
uint32_t run_flag = 0;

volatile uint8_t data0;

void print_data(void);

Fuzzy* fuzzy = new Fuzzy();

int main(void)
{
	RCC_ClocksTypeDef SYS_Clocks;
	RCC_GetClocksFreq(&SYS_Clocks);

	if(SysTick_Config(SYS_Clocks.HCLK_Frequency / 1000))
	{
		while(1);
	}
	Local_LED_Setup();
  Status_LED_Setup();
  LED(RED,0);
	init_USART();
	clr_term();

	snprintf(msg, SIZE, "%s%s%s Tyler Troyer - Motor CAN Node 0V2 - Target: STMicroelectronics STM32F303K8T6 %u MHz \r\n%s", none, Back_Blue, On_Red, SYS_Clocks.SYSCLK_Frequency/1000000, none);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\n%sSystem Clocks:\r\n", none);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tSYSCLK:%dM\r\n",SYS_Clocks.SYSCLK_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tHCLK:%dM\r\n",SYS_Clocks.HCLK_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tPCLK1:%dM\r\n",SYS_Clocks.PCLK1_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tPCLK2:%dM\r\n",SYS_Clocks.PCLK2_Frequency/1000000);
	USART1_puts(msg);
	delay(1000);
	snprintf(msg, SIZE, "Setting up CAN Peripheral... \r\n");
	USART1_puts(msg);
  delay(200);
	
  status = CAN1_Config(0);
	if(status != CAN_InitStatus_Success)
	{
    while(status != CAN_InitStatus_Success)
    {
      status = CAN1_Config(0);
      snprintf(msg, SIZE, "\rCAN Config() returned %u     ", status);
      USART1_puts(msg);
      //print_CAN_state(CAN1);
      LED(RED,1);
    }
    // CAN ready to go
    LED(RED,0);
	}
  
  //TxMessage.StdId = 201; // Corn Row Follower
  TxMessage.StdId = 203; // Motor Node 1
  //TxMessage.StdId = 202; // Motor Node 2
  TxMessage.IDE = CAN_Id_Standard;
  TxMessage.DLC = 8;
  TxMessage.RTR = CAN_RTR_Data;
  TxMessage.Data[0] = 0;
  TxMessage.Data[1] = 0;
  TxMessage.Data[2] = 0;
  TxMessage.Data[3] = 0;
  TxMessage.Data[4] = 0;
  TxMessage.Data[5] = 0;
  TxMessage.Data[6] = 0;
  TxMessage.Data[7] = 0;

	snprintf(msg, SIZE, "Done!\r\n");
	USART1_puts(msg);
  delay(200);
	snprintf(msg, 50, "main_thread()\r\n\n");
	USART1_puts(msg);
  
  TIM3_Config();
  ADCInit();
  
  while(1)
  {
    print_data();
    delay(200);
  }
}

void print_data(void)
{
  snprintf(msg, 50, "%s%s", Green, "\f-------System Monitor-------");
  USART1_puts(msg);
  //printState_Can();
  printState_ADC();
  //snprintf(msg, 50, "%s\r%s", Green, "---------------------------\r\n");
  //USART1_puts(msg);
}


