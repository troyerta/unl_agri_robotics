#include "i2c.h"
#include "stm32f30x.h"
#include "stm32f30x_i2c.h"
#include "usart.h"
#include "stdint.h"
#include "stdio.h"
#include "utils.h"

#define DD delay(100);
#define SLAVE_ADDRESS 0x62

void init_I2C(void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	I2C_DeInit(I2C1);
	
	GPIO_InitTypeDef GPIO_InitStruct;
	I2C_InitTypeDef I2C_InitStruct;
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	// Connect the Pins to the right AF peripheral
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_4);	// PB6 is SCL
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_4);	// PB7 is SDA
	
	I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStruct.I2C_AnalogFilter = I2C_AnalogFilter_Disable;
	I2C_InitStruct.I2C_DigitalFilter = 0x00;
	I2C_InitStruct.I2C_OwnAddress1 = 0x00;
	I2C_InitStruct.I2C_Ack = I2C_Ack_Disable;
	I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStruct.I2C_Timing = 0x0030081D;
	I2C_Init(I2C1, &I2C_InitStruct);
	
	/* Timing Notes:
		For 8 MHz I2C clk:
		
		Timing Register I2C_TIMINGR
		Field		Value		Field Position
		PRESC		0				31:28
		SCLL		0x9			7:0
		SCLH		0x3			15:8
		SDADEL	0x1			19:16
		SCLDEL	0x3			23:20
		
		So set bits 21, 20, 16, 9, 8, 3, 0
	*/
	
	I2C_Cmd(I2C1, ENABLE);
	
}

/* This function issues a start condition and
 * transmits the slave address + R/W bit
 *
 * Parameters:
 * 		I2Cx --> the I2C peripheral e.g. I2C1
 * 		address --> the 7 bit slave address
 * 		direction --> the tranmission direction can be:
 * 						I2C_Direction_Tranmitter for Master transmitter mode
 * 						I2C_Direction_Receiver for Master receiver
 */
void I2C_start(uint8_t address, uint16_t direction)
{
	// wait until I2C is not busy anymore
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
	//USART1_putc(48);//0
	// Need to set direction before START generation
	I2C_MasterRequestConfig(I2C1, direction);
	//USART1_putc(49);//1
	// Send I2C START condition
	I2C_GenerateSTART(I2C1, ENABLE);
	//USART1_putc(50);//2
	// Slave has acknowledged start condition
	// Wait for hardware to clear bit 
  while(I2C1->CR2 & I2C_CR2_START);

	//USART1_putc(51);//3
	// Load device address to be sent
	I2C1->TXDR = address;

	// Wait for address to be sent
	while (!(I2C1->ISR & I2C_ISR_TC));

	//USART1_putc(52);//4
	/* wait for I2C1 EV6, check if
	 * either Slave has acknowledged Master transmitter or
	 * Master receiver mode, depending on the transmission
	 * direction
	 */
	if(direction == I2C_Direction_Transmitter){
		while(!I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY | I2C_FLAG_ADDR | I2C_FLAG_TXE | I2C_FLAG_TC));
	}
	else if(direction == I2C_Direction_Receiver){
		while(!I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY | I2C_FLAG_ADDR));
	}
	//__enable_irq();
}

/* This function transmits one byte to the slave device
 * Parameters:
 *		I2Cx --> the I2C peripheral e.g. I2C1
 *		data --> the data byte to be transmitted
 */
void I2C_write(uint8_t data)
{
	I2C_SendData(I2C1, data);
	while(!I2C_GetFlagStatus(I2C1, I2C_ISR_BUSY | I2C_ISR_TXIS | I2C_ISR_TXE | I2C_ISR_TC));
}

/* This function reads one byte from the slave device
 * and acknowledges the byte (requests another byte)
 */
uint8_t I2C_read_ack(void)
{
	// enable acknowledge of recieved data
	I2C_AcknowledgeConfig(I2C1, ENABLE);
	// wait until one byte has been received
	while( !I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY | I2C_FLAG_RXNE) );
	// read data from I2C data register and return data byte
	uint8_t data = I2C_ReceiveData(I2C1);
	return data;
}

/* This function reads one byte from the slave device
 * and doesn't acknowledge the received data, which allows another byte to be received
 */
uint8_t I2C_read_nack(void)
{
	// disable acknowledge of received data
	// nack also generates stop condition after last byte received
	// see reference manual for more info
	I2C_AcknowledgeConfig(I2C1, DISABLE);
	I2C_GenerateSTOP(I2C1, ENABLE);
	// wait until one byte has been received
	while( !I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY | I2C_FLAG_RXNE) );
	// read data from I2C data register and return data byte
	uint8_t data = I2C_ReceiveData(I2C1);
	return data;
}

/* This funtion issues a stop condition and therefore
 * releases the bus
 */
void I2C_stop(void)
{
	// Send I2C STOP Condition
	I2C_GenerateSTOP(I2C1, ENABLE);
	while(I2C1->CR1 & I2C_ISR_STOPF);
}

void i2c_write_byte(uint8_t addr, uint8_t data)
{
	// Master requests a write transfer
	I2C1->CR2 &= ~(I2C_CR2_RD_WRN);
	
	// Start Generation and NBYTES = 2
	I2C1->CR2 |= I2C_CR2_START |  (2 << 16);
	
	// Wait for hardware to clear bit 
  while(I2C1->CR2 & I2C_CR2_START);
	
	// Load tx data register with device address
  I2C1->TXDR = addr;
	
	// Wait for TXDR to become emtpy, address is getting sent on bus while we wait
  while (!(I2C1->ISR & I2C_ISR_TXE));

	// Load DR wtih data package
  I2C1->TXDR = data;
	
	// Wait while reg address is sent on bus
  while (!(I2C1->ISR & I2C_ISR_TXE));
	
	// Set STOP condition
  I2C1->CR2 |= I2C_CR2_STOP;
  while(I2C1->CR2 & I2C_CR2_STOP);
}

uint8_t i2c_read_byte(uint8_t addr)
{
	
    uint8_t data = 0;

		// Master requests a write transfer
    I2C1->CR2 &= ~(I2C_CR2_RD_WRN);
	
		// NBYTES = 0;
    I2C1->CR2 &= ~(0xff << 16);
	
		// Make START condition and NBYTES = 1
    I2C1->CR2 |= I2C_CR2_START | (1 << 16);
	
		// Wait for hardware to clear bit
    while(I2C1->CR2 & I2C_CR2_START);

		// Load device address to be sent
    I2C1->TXDR = addr;
	
		// Wait for it to be sent
    while (!(I2C1->ISR & I2C_ISR_TXE));

		// Master requests a read transfer
    I2C1->CR2 |= I2C_CR2_RD_WRN;
		
		// Create START condition and NBYTES = 1
    I2C1->CR2 |= I2C_CR2_START | (1 << 16);
    while(I2C1->CR2 & I2C_CR2_START);
		
		// Wait for received data to get copied into RX data reg
    while (!(I2C1->ISR & I2C_ISR_RXNE));
		
		// Save the received data
    data = I2C1->RXDR;
		
		// Generate STOP condition
    I2C1->CR2 |= I2C_CR2_STOP;
    while(I2C1->CR2 & I2C_CR2_STOP);
		
    return data;
}

// Writes a new value to some register in the LIDAR device
void lidar_write(uint8_t reg_addr, uint8_t value)
{
	//printf("lidar_write\t");
	I2C_start(SLAVE_ADDRESS << 1, I2C_Direction_Transmitter);
	I2C_write(reg_addr);
	I2C_write(value);
	I2C_stop();
	//printf("OK\r\n");
	if(I2C_GetFlagStatus(I2C1, I2C_ISR_NACKF) == SET)
	{
		//printf("NACK\r\n");
	}
}

uint16_t lidar_get_distance(void)
{
	uint8_t data[2];
	uint16_t distance = 0;
	
	// Address the left lidar
	I2C_start(SLAVE_ADDRESS << 1, I2C_Direction_Transmitter);
	I2C_write(0x8f);
	// Stop the transmission
	I2C_stop();
	
	if(I2C_GetFlagStatus(I2C1, I2C_ISR_NACKF) == SET)
	{
		//printf("NACK\r\n");
	}
	else
	{
		I2C_start(SLAVE_ADDRESS << 1, I2C_Direction_Receiver);
		data[0] = I2C_read_ack();
		data[1] = I2C_read_nack();
		distance = (data[0] << 8) + data[1];
	}
	return distance;
}

void reset_lidar(void)
{
	//printf("Resetting Lidar device... ");
	lidar_write(0x00, 0x00);
	//printf("Done.\n\r");
}

void init_lidar(void)
{
	//printf("LIDAR init: Reset Lidar device\n\r");
	delay(25);
	lidar_write(0x00, 0x00);
	//printf("Setting Ts: Normal\n\r");
	delay(25);
	// Reset and init normally
	lidar_write(0x00, 0x00);
	delay(25);
	// Low noise, low sensitivity mode
	lidar_write(0x1C, 0x20);
	delay(25);
	lidar_write(0x1C, 0x20);
	delay(25);
	// 0x02 min, try 0x13 for 100 Hz
	lidar_write(0x45, 0x13);
	//printf("\tSetting Mode pin: free running PWM out\n\r");
	//delay(25);
	// Enable Mode pin low
	//lidar_write(0x04, 0x21);
	//printf("\tNum samples: infinite\n\r");
	delay(25);
	// Infinite number of readings
	lidar_write(0x11, 0xFF);
	//printf("\tInit aquisitions\n\r");
	delay(25);
	// Initiate reading distance
	lidar_write(0x00, 0x04);
	delay(25);
	//printf("\tLIDAR ready..\n\r");
}
