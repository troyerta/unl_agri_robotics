#include "main.h"
#include "usart.h"
#include "stm32f30x.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "can.h"
#include "utils.h"
#include "led.h"
#include "i2c.h"
#include "sabertooth.h"
#include "controller.h"
#include "vector.h"
#include "adc.h"
#include "tim3.h"
#include "kalman.h"

#define INT_MAX 400.0f

//#include "FuzzyRule.h"
//#include "FuzzyComposition.h"
//#include "Fuzzy.h"
//#include "FuzzyRuleConsequent.h"
//#include "FuzzyOutput.h"
//#include "FuzzyInput.h"
//#include "FuzzyIO.h"
//#include "FuzzySet.h"
//#include "FuzzyRuleAntecedent.h"

#define SIZE (uint8_t)120
#define RX_ID	201

uint8_t status;
uint16_t dist = 0;
uint8_t i = 0;
uint16_t j = 0;
uint16_t CAN1_ID;
uint8_t CAN1_DATA0;
uint8_t CAN1_DATA1;
uint8_t CAN1_DATA2;
uint8_t CAN1_DATA3;
uint8_t CAN1_DATA4;
uint8_t CAN1_DATA5;
uint8_t CAN1_DATA6;
uint8_t CAN1_DATA7;

uint16_t distR = 0;
uint16_t distL = 79;
float distR_f = 0.0f;
float distL_f = 0.0f;
float right_est = 0.0f;
float left_est = 0.0f;

float distInput = 0.0f;
float steeringOutput = 0.0f;

//float fuzzyLUT[] = 
//{
//    -42.000000f, -41.266266f, -40.566960f, 
//    -39.899713f, -39.276636f, -38.679780f, 
//    -38.107527f, -37.571178f, -37.055172f, 
//    -36.558376f, -36.091438f, -35.640523f, 
//    -35.204819f, -34.794466f, -34.396887f, 
//    -34.011494f, -33.648036f, -33.294862f, 
//    -32.951542f, -32.627536f, -32.311874f, 
//    -32.004237f, -31.713887f, -31.430345f, 
//    -31.153374f, -30.892111f, -30.636424f, 
//    -30.386139f, -30.150327f, -29.919094f, 
//    -29.692308f, -29.479034f, -29.269521f, 
//    -29.063670f, -28.870588f, -28.680590f, 
//    -28.493601f, -28.318814f, -28.146547f, 
//    -27.976744f, -27.818720f, -27.662743f, 
//    -27.508772f, -27.366279f, -27.225434f, 
//    -27.086207f, -26.958262f, -26.831627f, 
//    -26.706282f, -26.592113f, -26.478968f, 
//    -26.366834f, -26.265851f, -26.165651f, 
//    -26.066225f, -25.977998f, -25.890351f, 
//    -25.803279f, -25.727520f, -25.652174f, 
//    -25.577236f, -25.513791f, -25.450621f, 
//    -25.387722f, -25.336559f, -25.285561f, 
//    -25.234727f, -25.195931f, -25.157219f, 
//    -25.118590f, -25.092365f, -25.066169f, 
//    -25.040000f, -25.026667f, -25.013333f, 
//    -25.000000f, -24.512902f, -24.037962f, 
//    -23.574730f, -23.133570f, -22.702160f, 
//    -22.280179f, -21.876537f, -21.480760f, 
//    -21.092619f, -20.719751f, -20.353248f, 
//    -19.992948f, -19.645357f, -19.302918f, 
//    -18.965517f, -18.638648f, -18.315933f, 
//    -17.997294f, -17.687304f, -17.380634f, 
//    -17.077230f, -16.780822f, -16.487022f, 
//    -16.195795f, -15.910083f, -15.626359f, 
//    -15.344603f, -15.067013f, -14.790858f, 
//    -14.516129f, -14.244311f, -13.973425f, 
//    -13.703466f, -13.435229f, -13.167450f, 
//    -12.900128f, -12.633376f, -12.366624f, 
//    -12.099872f, -11.832550f, -11.564771f, 
//    -11.296534f, -11.026575f, -10.755689f, 
//    -10.483871f, -10.209142f, -9.932987f, 
//    -9.655397f, -9.373641f, -9.089917f, 
//    -8.804205f, -8.512978f, -8.219178f, 
//    -7.922770f, -7.619366f, -7.312696f, 
//    -7.002706f, -6.684067f, -6.361352f, 
//    -6.034483f, -5.697082f, -5.354643f, 
//    -5.007052f, -4.646752f, -4.280249f, 
//    -3.907381f, -3.519240f, -3.123463f, 
//    -2.719821f, -2.297840f, -1.866430f, 
//    -1.425270f, -0.962038f, -0.487098f, 
//    -0.000000f, 0.487098f, 0.962038f, 
//    1.425270f, 1.866430f, 2.297840f, 
//    2.719821f, 3.123463f, 3.519240f, 
//    3.907381f, 4.280249f, 4.646752f, 
//    5.007052f, 5.354643f, 5.697082f, 
//    6.034483f, 6.361352f, 6.684067f, 
//    7.002706f, 7.312696f, 7.619366f, 
//    7.922770f, 8.219178f, 8.512978f, 
//    8.804205f, 9.089917f, 9.373641f, 
//    9.655397f, 9.932987f, 10.209142f, 
//    10.483871f, 10.755689f, 11.026575f, 
//    11.296534f, 11.564771f, 11.832550f, 
//    12.099872f, 12.366624f, 12.633376f, 
//    12.900128f, 13.167450f, 13.435229f, 
//    13.703466f, 13.973425f, 14.244311f, 
//    14.516129f, 14.790858f, 15.067013f, 
//    15.344603f, 15.626359f, 15.910083f, 
//    16.195795f, 16.487022f, 16.780822f, 
//    17.077230f, 17.380634f, 17.687304f, 
//    17.997294f, 18.315933f, 18.638648f, 
//    18.965517f, 19.302918f, 19.645357f, 
//    19.992948f, 20.353248f, 20.719751f, 
//    21.092619f, 21.480760f, 21.876537f, 
//    22.280179f, 22.702160f, 23.133570f, 
//    23.574730f, 24.037962f, 24.512902f, 
//    25.000000f, 25.013333f, 25.026667f, 
//    25.040000f, 25.066169f, 25.092365f, 
//    25.118590f, 25.157219f, 25.195931f, 
//    25.234727f, 25.285561f, 25.336559f, 
//    25.387722f, 25.450621f, 25.513791f, 
//    25.577236f, 25.652174f, 25.727520f, 
//    25.803279f, 25.890351f, 25.977998f, 
//    26.066225f, 26.165651f, 26.265851f, 
//    26.366834f, 26.478968f, 26.592113f, 
//    26.706282f, 26.831627f, 26.958262f, 
//    27.086207f, 27.225434f, 27.366279f, 
//    27.508772f, 27.662743f, 27.818720f, 
//    27.976744f, 28.146547f, 28.318814f, 
//    28.493601f, 28.680590f, 28.870588f, 
//    29.063670f, 29.269521f, 29.479034f, 
//    29.692308f, 29.919094f, 30.150327f, 
//    30.386139f, 30.636424f, 30.892111f, 
//    31.153374f, 31.430345f, 31.713887f, 
//    32.004237f, 32.311874f, 32.627536f, 
//    32.951542f, 33.294862f, 33.648036f, 
//    34.011494f, 34.396887f, 34.794466f, 
//    35.204819f, 35.640523f, 36.091438f, 
//    36.558376f, 37.055172f, 37.571178f, 
//    38.107527f, 38.679780f, 39.276636f, 
//    39.899713f, 40.566960f, 41.266266f, 
//    42.000000f,
//};

__IO uint32_t doneSampling = 0;;
char msg[SIZE];
__IO uint16_t ADC1ConvertedValue = 0;
__IO uint16_t ADC1ConvertedVoltage = 0;
__IO float ADC1ConvertedVoltageF = 0.0f;

uint32_t unitDelay = 200;
CanTxMsg TxMessage;
CanRxMsg RxMessage;
uint8_t buf_size;
uint16_t currentTime;
uint16_t previousTime;
uint16_t deltaTime;
uint16_t frameCounter;
uint32_t run_flag = 0;
uint8_t msg_counter1 = 0;
uint8_t msg_counter2 = 0;
int32_t steerMag = 0;
volatile uint8_t remote_flags[2];
volatile uint32_t time;
volatile uint8_t data0;
uint32_t update_tx_data = 0;

void process_data(void);
void print_data(void);
float conditional_zoh_left(float input, float zoh_thresholdL, float zoh_thresholdH);
float conditional_zoh_right(float input, float zoh_thresholdL, float zoh_thresholdH);
float filter_left(float measurement);
float filter_right(float measurement);
float filter_error(float measurement);
float run_controlLaw(float input);
void scheduler_checkTime(void);

//Fuzzy* fuzzy = new Fuzzy();

int main(void)
{
	RCC_ClocksTypeDef SYS_Clocks;
	RCC_GetClocksFreq(&SYS_Clocks);

	if(SysTick_Config(SYS_Clocks.HCLK_Frequency / 1000))
	{
		while(1);
	}
	
  Local_LED_Setup();
  Status_LED_Setup();
	init_USART();
	clr_term();

	snprintf(msg, SIZE, "%s%s%s Tyler Troyer - Motor CAN Node 0V2 - Target: STMicroelectronics STM32F303K8T6 %u MHz \r\n%s", none, Back_Blue, On_Red, SYS_Clocks.SYSCLK_Frequency/1000000, none);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\n%sSystem Clocks:\r\n", none);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tSYSCLK:%dM\r\n",SYS_Clocks.SYSCLK_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tHCLK:%dM\r\n",SYS_Clocks.HCLK_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tPCLK1:%dM\r\n",SYS_Clocks.PCLK1_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tPCLK2:%dM\r\n",SYS_Clocks.PCLK2_Frequency/1000000);
	USART1_puts(msg);
	delay(1000);
	snprintf(msg, SIZE, "Setting up CAN Peripheral... ");
	USART1_puts(msg);
	
  status = CAN1_Config(RX_ID);
	if(status != CAN_InitStatus_Success)
	{
    while(status != CAN_InitStatus_Success)
    {
      status = CAN1_Config(RX_ID);
      snprintf(msg, SIZE, "\rCAN Config() returned %u     ", status);
      USART1_puts(msg);
      //print_CAN_state(CAN1);
      LED(RED,1);
    }
    // CAN ready to go
    LED(RED,0);
	}
  
  TxMessage.StdId = 200;
  TxMessage.IDE = CAN_Id_Standard;
  TxMessage.DLC = 8;
  TxMessage.RTR = CAN_RTR_Data;
  TxMessage.Data[0] = 0;
  TxMessage.Data[1] = 0;
  TxMessage.Data[2] = 0;
  TxMessage.Data[3] = 0;
  TxMessage.Data[4] = 0;
  TxMessage.Data[5] = 0;
  TxMessage.Data[6] = 0;
  TxMessage.Data[7] = 0;

	snprintf(msg, SIZE, "Done!\r\n");
	USART1_puts(msg);
	
	snprintf(msg, 50, "main_thread()\r\n\n");
	USART1_puts(msg);
  frameCounter = 1;
  
  snprintf(msg, 50, "rawDist,zoh,estimate\r\n");
	USART1_puts(msg);

  // Systick Interrupt counts the time for us while we wait for the loop to run
  while(1)
  {
    scheduler_checkTime();
    //snprintf(msg, 50, " t=%u\r\n", time);
    // Run at multiples of the main loop period, 1/1mS = 1000Hz
    
    if(deltaTime >= 1)
    {
      frameCounter++;
      //snprintf(msg, 50, " f=%u\r\n", frameCounter);
      //USART1_puts(msg);
      // 1000Hz / 2 = 500 Hz
      // We're sampling the CAN_RX buffer at twice the distance sensor rate, we'll catch it in good time
      if(frameCounter % 1 == 0)
      {
        process_data();
      }
      // Print state at 10Hz
      if(frameCounter % 100 == 0)
      {
        print_data();
      }
      previousTime = currentTime;
      frameCounter = (frameCounter + 1) % 1000;
    }
  }
}

void process_data(void)
{
  // Did messages arrive?
  if(!can_rx_fifo_empty())
  {
    buf_size = can_rx_fifo_size();
    
    // Process each message
    for(i = 0; i < buf_size; i++)
    {
      if(can_fifo_read(&RxMessage))
      {
        if(RxMessage.StdId == RX_ID)
        {
          // Read CAN message contents and constrain them for the fuzzy system
          // Run fuzzy inferencing
          //distInput = f_constrain((float)((float)distR - (float)distL), -150.0f, 150.0f);
          //distInput = (float)((float)distR - (float)distL);
          //fuzzy->setInput(1, distInput);
          //fuzzy->fuzzify();
          //steeringOutput = f_constrain(fuzzy->defuzzify(1), -150.0f, 150.0f);
          //steeringOutput = fuzzy->defuzzify(1);
          
          distR = RxMessage.Data[0];
          distL = RxMessage.Data[1];
          
          distL_f = float(distL);
          distR_f = float(distR);
          
          // Zoh filter
          distL_f = conditional_zoh_left(distL_f, 15.0f, 130.0f);
          distR_f = conditional_zoh_right(distR_f, 15.0f, 130.0f);
          
          // Kalman Estimation
          left_est = filter_left(distL_f);
          right_est = filter_right(distR_f);
          
          // Use filtered signals to run the proportional controller
          distInput = right_est - left_est;
          distInput = f_constrain(distInput, -100.0f, 100.0f);
          
          // This should become a function call to a math equation
          //steeringOutput = fuzzyLUT[(distR - distL) + 150];
          steeringOutput = run_controlLaw(distInput);
           
          steeringOutput = f_constrain(steeringOutput, -127.0f, 127.0f);
          update_tx_data = 1;
          
          steerMag = (int32_t)steeringOutput;
          TxMessage.Data[0] = (uint8_t)(steerMag + 127);
          TxMessage.Data[1] = (uint8_t)left_est;
          TxMessage.Data[2] = (uint8_t)right_est;
          steerMag = (int32_t)distInput;
          TxMessage.Data[3] = (uint8_t)(steerMag + 127);
          TxMessage.Data[4] = 0;
          TxMessage.Data[5] = 0;
          TxMessage.Data[6] = 0;
          TxMessage.Data[7] = 0;

          // Constrain that output
          // Tx on the CAN bus
          // pet the watch dog counter
          
        }
        else
        {
          
        }
      }
      else
      {
        break;
      }
    }
    if(update_tx_data)
    {
      can_tx(CAN1,&TxMessage);
      update_tx_data = 0;
    }
  }
  else
  {
    // No message seen in the same task loop period (2 mS)
    // Increment the watchdog counter
    
  }
  // Measure the watchdog counter
  // If too big, we have been missing CAN messages for awhile now
  // Turn on red LED to panic
  // if(watchdogCounter >= 1000)
  // Maybe tx zero/neutral value?
}

float conditional_zoh_left(float input, float zoh_thresholdL, float zoh_thresholdH)
{
  static float last_good_value = 75.0f;
  float result;
  
  if(input >= zoh_thresholdH)
  {
    result = last_good_value;
  }
  else if(input <= zoh_thresholdL)
  {
    result = last_good_value;
  }
  else
  {
    result = input;
    last_good_value = input;
  }
  
  return result;
}

float conditional_zoh_right(float input, float zoh_thresholdL, float zoh_thresholdH)
{
  static float last_good_value = 75.0f;
  float result;
  
  if(input >= zoh_thresholdH)
  {
    result = last_good_value;
  }
  else if(input <= zoh_thresholdL)
  {
    result = last_good_value;
  }
  else
  {
    result = input;
    last_good_value = input;
  }
  
  return result;
}

float filter_left(float measurement)
{
  // Data is in 32 bit floating point format
  static float Q = {(0.01f)};
  static float R = {(10.0f)};
  static float x = {(75.0f)};
  static float P = {(50.0f)};
  // Intermediate values for internal calculation
  static float xp = {0.0f};
  static float K = {0.0f};
  static float Pp = {0.0f};

  xp = x;
  Pp = P + Q;
  K = Pp * (1.0f / (Pp + R));
  x = xp + (K * (measurement - xp));
  P = Pp - (K * Pp);

  return x;
}

float filter_right(float measurement)
{
  // Data is in 32 bit floating point format
  static float Q = {(0.01f)};
  static float R = {(10.0f)};
  static float x = {(75.0f)};
  static float P = {(50.0f)};
  // Intermediate values for internal calculation
  static float xp = {0.0f};
  static float K = {0.0f};
  static float Pp = {0.0f};

  xp = x;
  Pp = P + Q;
  K = Pp * (1.0f / (Pp + R));
  x = xp + (K * (measurement - xp));
  P = Pp - (K * Pp);

  return x;
}

float filter_error(float measurement)
{
  // Data is in 32 bit floating point format
  static float Q = {(0.01f)};
  static float R = {(100.0f)};
  static float x = {(75.0f)};
  static float P = {(50.0f)};
  // Intermediate values for internal calculation
  static float xp = {0.0f};
  static float K = {0.0f};
  static float Pp = {0.0f};

  xp = x;
  Pp = P + Q;
  K = Pp * (1.0f / (Pp + R));
  x = xp + (K * (measurement - xp));
  P = Pp - (K * Pp);

  return x;
}

float run_controlLaw(float input)
{
  float output = 0.0f;
  static float integral = 0.0f;
  float p_term = 0.0f;
  float i_term = 0.0f;
//  if(input < 0.0f)
//  {
//    if(input <= -50.0f)
//    {
//      output = (-1.50*input) - 50.0f;
//    }
//    else
//    {
//      output = -0.5 * input;
//    }
//  }
//  else
//  {
//    if(input >= 50.0f)
//    {
//      output = (-1.50*input) + 50.0f;
//    }
//    else
//    {
//      output = -0.5 * input;
//    }
//  }
  integral += input;
  
  if(integral > INT_MAX)
  {
    integral = INT_MAX;
  }
  else if(integral < -INT_MAX)
  {
    integral = -INT_MAX;
  }
  
  //p_term = -0.40f * input;
  p_term = -0.50f * input;
  i_term = -0.001f * integral;
  
  output = p_term + i_term;

  return output;
}

void print_data(void)
{
  snprintf(msg, 50, "%s%s", Green, "\f-------System Monitor-------");
  USART1_puts(msg);
  printState_Can();
  //printState_Controller(distInput,steeringOutput);
  snprintf(msg, 100, "%s\r\nFuzzy Controller:%s\r\nInput R-L\t\t%3.1f\r\nOutput\t\t\t%3.1f\r\n", UPurple, none, distInput, steeringOutput);
  USART1_puts(msg);
  //printState_ADC();
  //printState_Vector();
  //snprintf(msg, 50, "%s\r%s", Green, "---------------------------\r\n");
  //USART1_puts(msg);
}

// Samples the time variable for us
void scheduler_checkTime(void)
{
  currentTime = time;
  
  if(currentTime < previousTime)
  {
    deltaTime = currentTime + 1000 - previousTime;
  }
  else
  {
    deltaTime = currentTime - previousTime;
  }
}

// Quick square root - real quick
float Q_rsqrt(float number);
float Q_rsqrt(float number)
{
  long i; 
  float x2, y; 
  const float threehalfs = 1.5f; 
  x2 = number * 0.5F; 
  y = number; 
  i = *(long*) &y; // evil floating point bit level hacking 
  i = 0x5f3759df - (i >> 1); // what the fuck? 
  y = *(float*) &i; 
  y = y * (threehalfs - (x2 * y * y)); // 1st iteration return y;
  return y;
}


