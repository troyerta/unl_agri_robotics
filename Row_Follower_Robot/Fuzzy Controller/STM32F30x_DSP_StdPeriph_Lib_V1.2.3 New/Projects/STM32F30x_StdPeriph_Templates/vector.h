#define VECTOR_FULL_FWD (uint16_t)36000
#define VECTOR_STOP		 (uint16_t)27000
#define VECTOR_FULL_REV (uint16_t)18000

#ifdef __CPLUSPLUS
extern "C" {
#endif
void TIM2_Init_PWM(void);
void PWM_Pins_Init(void);
void TIM2_PWM3_setPeriod(uint32_t period);
void TIM2_PWM4_setPeriod(uint32_t period);
void VECTOR_CMD(float M1Speed, float M2Speed);
uint32_t constrain_PWM(uint32_t input, uint32_t min, uint32_t max);
void printState_Vector(void);
#ifdef __CPLUSPLUS
}
#endif
