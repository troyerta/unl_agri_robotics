#include "stm32f30x.h"
#include "stm32f30x_can.h"
#include "utils.h"
#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "usart.h"
#include "led.h"
#include "can.h"
#include "string.h"

#define CAN_FIFO_LEN          64
#define CAN_TX	GPIO_Pin_12
#define CAN_RX	GPIO_Pin_11
#define CAN_DBG_MSG_SIZE      60

extern CanRxMsg RxMessage;
extern CanTxMsg TxMessage;
static CanRxMsg LastRxMsg;
extern volatile uint8_t data0;

static char msg[CAN_DBG_MSG_SIZE];
static CanRxMsg rx_fifo[CAN_FIFO_LEN];
//static CanTxMsg tx_fifo[CAN_FIFO_LEN];
static uint16_t rx_head;
static uint16_t rx_tail;
//static uint16_t tx_head = 0;
//static uint16_t tx_tail = 0;


CAN_InitTypeDef        CAN_InitStructure;
CAN_FilterInitTypeDef  CAN_FilterInitStructure;

uint8_t CAN1_Config(uint32_t filterID)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
	uint8_t status = 0;
  
  /* CAN GPIOs configuration **************************************************/

  /* Enable GPIO clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* Connect CAN pins */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_9);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_9);
  
  /* Configure CAN RX and TX pins */
  GPIO_InitStructure.GPIO_Pin = CAN_TX | CAN_RX;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* CAN configuration ********************************************************/  
  /* Enable CAN clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
  
  /* CAN register init */
  CAN_DeInit(CAN1);
  CAN_StructInit(&CAN_InitStructure);

  /* CAN cell init */
  CAN_InitStructure.CAN_TTCM = DISABLE;         // Time-triggered communication mode, (time stamps added to last two bytes of data)
  CAN_InitStructure.CAN_ABOM = DISABLE;         // Automatic Bus-off management
  CAN_InitStructure.CAN_AWUM = DISABLE;         // Automatic wakeup mode
  CAN_InitStructure.CAN_NART = DISABLE;         // Non-automatic retransimssion mode
  CAN_InitStructure.CAN_RFLM = DISABLE;         // Feature to optionally lock the FIFO contents from being overwritten by new msgs if FIFO is full
  CAN_InitStructure.CAN_TXFP = DISABLE;         // Tx mailbox contents by priority, not FIFO
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal; // Apart from loopback and silent modes
  CAN_InitStructure.CAN_SJW = CAN_SJW_4tq;      // How many time quanta to adjust by for synchronization? Max 2 - why? Because.

  /*
   * How to calculate this shit:
   * BF = (APB1_Clk/Desired_Baudrate);
   * # of Quanta per bit = TQ = BF/Prescalar;
   * Given that TQ = (1 + BS1 + BS2),
   * Choose BS1 and BS2 such that 
   * BS1/(BS1+BS2) ~ 72%
   * for
   * BS1 = 1,....,16
   * BS2 = 1,....,8
   * 
   * So get your APB1_clk rate from the system
   * and calculate BF using your desired baudrate.
   * Then pick a prescalar such that 8 < TQ < 25
   * Finally, set BS1 ~= 0.72 * TQ
   * and BS2 = (TQ - 1) - BS1
   * You're done! Baudrate is set, sampling point is near optimal
   */
   
  /* CAN Baudrate = 500kBps (CAN clocked at 45 MHz) */
//  CAN_InitStructure.CAN_BS1 = CAN_BS1_10tq;      // 10tq / (15tq - 1tq) = 71.4% 
//  CAN_InitStructure.CAN_BS2 = CAN_BS2_4tq;       // 1tq + 10tq + 4tq = 15tq
//  CAN_InitStructure.CAN_Prescaler = 6;           // (45MHz / 500,000Baud) / 6 = 15tq per bit
//  CAN_Init(CAN2, &CAN_InitStructure);
    
  /* CAN Baudrate = 250kBps (CAN clocked at 32 MHz) */
  CAN_InitStructure.CAN_BS1 = CAN_BS1_11tq;
  CAN_InitStructure.CAN_BS2 = CAN_BS2_4tq;
  CAN_InitStructure.CAN_Prescaler = 8;
  status = CAN_Init(CAN1, &CAN_InitStructure);

  /* CAN Baudrate = 250kBps (CAN clocked at 18 MHz) */
//  CAN_InitStructure.CAN_BS1 = CAN_BS1_12tq;
//  CAN_InitStructure.CAN_BS2 = CAN_BS2_5tq;
//  CAN_InitStructure.CAN_Prescaler = 4;
//  status = CAN_Init(CAN1, &CAN_InitStructure);

  /* CAN filter init */
  CAN_FilterInitStructure.CAN_FilterNumber = 0;
	
	if(filterID != (uint16_t)0)
	{
    CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
    CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh = (filterID) << 5;
	}
	else
	{
    CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
    CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	}
	
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);
  
  // Enable the TX complete interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  rx_head = 0;
  rx_tail = 0;
  memset(&rx_fifo[0], 0, CAN_FIFO_LEN);
  
  /* Enable FIFO 0 message pending Interrupt */
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
	
	return status;
}

extern "C"
{
void USB_LP_CAN1_RX0_IRQHandler(void)
{
  if(CAN_GetITStatus(CAN1, CAN_IT_FMP0) != RESET)
  {
    CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);
    CanRxMsg tempMsg;
    CAN_Receive(CAN1, 0, &tempMsg);
    can_rx_fifo_write(tempMsg);
    copyCanPacket(&LastRxMsg, &tempMsg);
    //LastRxMsg.Data[4] = 0;
    //LastRxMsg.Data[5] = 0;
    //LastRxMsg.Data[6] = 0;
    //LastRxMsg.Data[7] = 0;
    data0 = tempMsg.Data[0];
    //LED(GPIO_Pin_6, ON);
  }
}
}

/* Return 1 if true, 0 otherwise. */
uint8_t can_rx_fifo_empty(void)
{
  if (rx_head == rx_tail)
  {
    return 1;
  }
  return 0;
}

/* Return 1 if true, 0 otherwise. */
uint8_t can_rx_fifo_full(void)
{
  if(((rx_tail+1)%CAN_FIFO_LEN) == rx_head)
  {
    return 1;
  }
  return 0;
}

/*
 * Append a message to our CAN message fifo.
 * Returns 1 if msg successfully appended to fifo.
 * Returns 0 if fifo is full.
 */
uint8_t can_rx_fifo_write(CanRxMsg msg)
{
  if(can_rx_fifo_full())
  {
    return 0;
  }
  rx_fifo[rx_tail] = msg;
  rx_tail = (rx_tail+1) % CAN_FIFO_LEN;
  return 1;
}

/*
 * Read 1 msg from fifo.
 * Returns 0 if fifo is empty, otherwise 1.
 */
uint8_t can_fifo_read(CanRxMsg *msg)
{
  if(can_rx_fifo_empty())
  {
    return 0;
  }
  *msg = rx_fifo[rx_head];
  rx_head = (rx_head+1) % CAN_FIFO_LEN;
  return 1;
}

uint8_t can_rx_fifo_size(void)
{
  uint8_t value;
  uint16_t head;
  uint16_t tail;
  
  CAN_ITConfig(CAN1, CAN_IT_FMP0, DISABLE);
  head = rx_head;
  tail = rx_tail;
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
  
  if(tail >= head)
  {
    value = tail - head;
  }
  else
  {
    value = CAN_FIFO_LEN + tail - head;
  }
  
  return value;
}

void CAN1_TX_IRQHandler(void)
{
//  if(CAN_GetITStatus(CAN1, CAN_IT_TME) != RESET)
//  {
//    // Transmission was successful
//    CAN_ClearITPendingBit(CAN1, CAN_IT_TME);
//    CAN_ITConfig(CAN1, CAN_IT_TME, DISABLE);
//    
//    //if(txTail == txHead)
//    // If buffer is empty, turn off interrupts
//    if(numTxElements() == 0)
//    {
//      CAN_ITConfig(CAN1, CAN_IT_TME, DISABLE);
//    }
//    // Something(s) in the the buffer at the tail position
//    // Place it in the tx mailbox and request a tx over CAN
//    else
//    {
//      CAN_Transmit(CAN1, &txBuf[txTail]);
//      txTail++;
//      
//      if(txTail == bufSize)
//      {
//        txTail = 0;
//      }
//      
//      if(numTxElements())
//      {
//        CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);
//      }
//    }
//  }
}


void can_tx(CAN_TypeDef *CANx, CanTxMsg* TxMessage)
{
		CAN_Transmit(CANx, TxMessage);
		while(CAN_TransmitStatus(CANx, 0) == CAN_TxStatus_Pending);
}


void print_CAN_state(CAN_TypeDef *CANx)
{
	const uint8_t size = 30;
	char dbgMsg[size];
	
	snprintf(dbgMsg, size, "\n%sCAN Module State:\r\n\n", Cyan);
	USART1_puts(dbgMsg);
  snprintf(dbgMsg, size, "\tMSR \t\t\t\t= 0x%x\r\n", CANx->MSR);
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tESR \t\t\t\t= 0x%x\r\n", CANx->ESR);
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tFlags \t\t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_LEC));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tError Warning \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_EWG));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tError Passive \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_EPV));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tBus-off \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_BOF));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tLast Error Code \t\t= %u\r\n", CAN_GetLastErrorCode(CANx));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tReceive Error Counter \t\t= %u\r\n", CAN_GetReceiveErrorCounter(CANx));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tTransmit Error Counter \t\t= %u\r\n%s", CAN_GetLSBTransmitErrorCounter(CANx), none);
	USART1_puts(dbgMsg);
	
	uint8_t error = CAN_GetLastErrorCode(CAN1);
	
	if(error & CAN_ErrorCode_NoErr)
	{
		snprintf(dbgMsg, size, "\nCAN_ErrorCode_NoErr\r\n");
		USART1_puts(dbgMsg);
	}
	else
	{
		if(error & CAN_ErrorCode_StuffErr)
		{
			snprintf(dbgMsg, size, "\nCAN_ErrorCode_StuffErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_FormErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_FormErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_ACKErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_ACKErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_BitRecessiveErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_BitRecessiveErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_BitDominantErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_BitDominantErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_CRCErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_CRCErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_SoftwareSetErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_SoftwareSetErr\r\n");
			USART1_puts(dbgMsg);
		}
	}
}

void copyCanPacket(CanRxMsg* ToMessage, CanRxMsg* FromMessage)
{
  ToMessage->IDE = FromMessage->IDE;
  ToMessage->RTR = FromMessage->RTR;
  ToMessage->StdId = FromMessage->StdId;
  ToMessage->ExtId = FromMessage->ExtId;
  ToMessage->DLC = FromMessage->DLC;
  ToMessage->Data[0] = FromMessage->Data[0];
  ToMessage->Data[1] = FromMessage->Data[1];
  ToMessage->Data[2] = FromMessage->Data[2];
  ToMessage->Data[3] = FromMessage->Data[3];
  ToMessage->Data[4] = FromMessage->Data[4];
  ToMessage->Data[5] = FromMessage->Data[5];
  ToMessage->Data[6] = FromMessage->Data[6];
  ToMessage->Data[7] = FromMessage->Data[7];
}

void printState_Can(void)
{
  uint32_t rxState = (CAN1->MSR & 0x00000200) >> 9;
  uint32_t txState = (CAN1->MSR & 0x00000100) >> 8;
  uint32_t bufSize = can_rx_fifo_size();
  uint32_t LEC = (CAN1->MSR & 0x00000070) >> 4;
  
  snprintf(msg, CAN_DBG_MSG_SIZE, "%s\r\n\nCAN1 Module:%s\r\n", UCyan, none);
  USART1_puts(msg);

  if(CAN1->ESR)
  {
    snprintf(msg, CAN_DBG_MSG_SIZE, "%sESR\t\t%u%s\r\n", Red, CAN1->ESR, none);
    USART1_puts(msg);
  }
 
  snprintf(msg, CAN_DBG_MSG_SIZE, "Buffered Msgs\t\t%u/%u\r\n", bufSize, CAN_FIFO_LEN);
  USART1_puts(msg);
  
  if(LEC)
  {
    snprintf(msg, CAN_DBG_MSG_SIZE, "%sLEC\t\t%u%s\r\n", Red, LEC, none);
    USART1_puts(msg);
  }
  
  snprintf(msg, CAN_DBG_MSG_SIZE, "Last Rx Msg\t\t0x%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t\r\n", LastRxMsg.StdId, 
                                                                                        LastRxMsg.DLC, 
                                                                                        LastRxMsg.Data[0], 
                                                                                        LastRxMsg.Data[1], 
                                                                                        LastRxMsg.Data[2], 
                                                                                        LastRxMsg.Data[3], 
                                                                                        LastRxMsg.Data[4], 
                                                                                        LastRxMsg.Data[5], 
                                                                                        LastRxMsg.Data[6], 
                                                                                        LastRxMsg.Data[7]);
  USART1_puts(msg);
}
