#include "stdint.h"
#include "stm32f30x.h"

void reset_lidar(I2C_TypeDef* I2Cx);
void init_lidar(I2C_TypeDef* I2Cx);
void lidar_write(I2C_TypeDef* I2Cx, uint8_t reg_addr, uint8_t value);
uint16_t lidar_get_distance(I2C_TypeDef* I2Cx);
