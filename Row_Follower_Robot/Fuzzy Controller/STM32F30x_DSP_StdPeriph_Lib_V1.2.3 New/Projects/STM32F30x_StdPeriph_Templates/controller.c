#include "stdint.h"
#include "stdio.h"
#include "main.h"
#include "stm32f30x.h"
#include "controller.h"
#include "usart.h"

#include "FuzzyRule.h"
#include "FuzzyComposition.h"
#include "Fuzzy.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzyOutput.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzySet.h"
#include "FuzzyRuleAntecedent.h"

#define CONT_DBG_MSG_SIZE   100

static char msg[CONT_DBG_MSG_SIZE];
Fuzzy* fuzzy;



void fuzzy_controller_init(void)
{
  // 1 FuzzyInput
  FuzzyInput* distDiff = new FuzzyInput(1);

  // 1 FuzzyOutput
  FuzzyOutput* speedDiff = new FuzzyOutput(1);

  /*************************************
   * Input 1
   ************************************/
  // Three Sets for "difference between R and L distances"
  FuzzySet* rightClose = new FuzzySet(-150.0f, -150.0f, -150.0f, -10.0f);
  FuzzySet* centered   = new FuzzySet(-150.0f, -10.0f, 10.0f, 150.0f);
  FuzzySet* leftClose  = new FuzzySet(10.0f, 150.0f, 150.0f, 150.0f);


  // Add these memberships to input set
  distDiff->addFuzzySet(rightClose);
  distDiff->addFuzzySet(centered);
  distDiff->addFuzzySet(leftClose);

  /*************************************
   * Output
   ************************************/
  // Seven sets for steering modes to take (close ones narrow far ones wider)
  FuzzySet* right    = new FuzzySet(-30.0f, -30.0f, -25.0f, 0.0f);
  FuzzySet* straight = new FuzzySet(-24.0f, 0.0f, 0.0f, 24.0f);
  FuzzySet* left     = new FuzzySet(0.0f, 25.0f, 30.0f, 30.0f);

  // Add these memberships to our output
  speedDiff->addFuzzySet(right);
  speedDiff->addFuzzySet(straight);
  speedDiff->addFuzzySet(left);


  /************************************************************************************************************
   * Inputs and Output are now established, we need to add them to our fuzzy system
   ***********************************************************************************************************/
  fuzzy->addFuzzyInput(distDiff);
  fuzzy->addFuzzyOutput(speedDiff);


  /************************************************************************************************************
   * Fuzzy Rule Establishment
   ***********************************************************************************************************/
  // These are the 10 possible INPUT states
  // These antecdents are combos of sets from the inputs defining system states/conditions
  // Here we declare them and give them names
  FuzzyRuleAntecedent* ifRight    = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifLeft     = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifCentered = new FuzzyRuleAntecedent();

  // Now we fill each antecedent with the combo of sets which define it
//  ifHighToleranceCentered->joinWithAND(highTolerance, centered);
//  ifHighTolRight->joinWithAND(highTolerance, right);
//  ifHighTolTooRight->joinWithAND(highTolerance, tooFarRight);
//  ifHighTolLeft->joinWithAND(highTolerance, left);
//  ifHighTolTooLeft->joinWithAND(highTolerance, tooFarLeft);
//  ifLowTolCentered->joinWithAND(lowTolerance, centered);
//  ifLowTolLeft->joinWithAND(lowTolerance, left);
//  ifLowTolTooLeft->joinWithAND(lowTolerance, tooFarLeft);
//  ifLowTolRight->joinWithAND(lowTolerance, right);
//  ifLowTolTooRight->joinWithAND(lowTolerance, tooFarRight);
//  ifCentered->joinWithOR(ifHighToleranceCentered, ifLowTolCentered);  // do we need this one?

  ifRight->joinSingle(rightClose);
  ifLeft->joinSingle(leftClose);
  ifCentered->joinSingle(centered);

  // These are the 7 possible OUTPUT states, need to make this 5, since two of them are technically identical
  FuzzyRuleConsequent* thenKeepCentered = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSteerLeft    = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSteerRight   = new FuzzyRuleConsequent();

  // Add the appropriate sets to the output states we defined above
  thenKeepCentered->addOutput(straight);
  thenSteerLeft->addOutput(left);
  thenSteerRight->addOutput(right);
  
  // Building FuzzyRules - NOTICE, some of these could be combined with OR since they
  // share the same output consequence, rules 2&3&, 4&5, 1&6(since HighTolKeepCentered and LowTolKeepCentered are both the "goStraight" output set
  FuzzyRule* fuzzyRule1 = new FuzzyRule(1, ifCentered, thenKeepCentered);
  FuzzyRule* fuzzyRule2 = new FuzzyRule(2, ifRight, thenSteerLeft);
  FuzzyRule* fuzzyRule3 = new FuzzyRule(3, ifLeft, thenSteerRight);

  // Add these rules to our controller
  fuzzy->addFuzzyRule(fuzzyRule1);
  fuzzy->addFuzzyRule(fuzzyRule2);
  fuzzy->addFuzzyRule(fuzzyRule3);
}

void printState_Controller(float input, float output)
{
  float in = input;
  float out = output;
  
  snprintf(msg, CONT_DBG_MSG_SIZE, "%s\r\nFuzzy Controller:%s\r\nInput R-L\t\t%3.1f\r\nOutput\t\t\t%3.1f\r\n", UPurple, none, in, out);
  USART1_puts(msg);
}
