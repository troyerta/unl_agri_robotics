#include "stm32f30x.h"
#include "stdint.h"
#include "stdio.h"
#include "usart.h"

void init_USART(void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_7);  // Tx
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_7);  // Rx
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	USART_InitStruct.USART_BaudRate = 115200;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStruct);
	USART_Cmd(USART1, ENABLE);
	
	USART1_putc('\f');

}

char USART1_getc(void)
{
	while((USART1->ISR & USART_FLAG_RXNE) == 0);
	return USART1->RDR;
}

void USART1_putc(char c)
{
	while((USART1->ISR & USART_FLAG_TXE) == 0);
	USART1->TDR = c;
	return;
}

void USART1_puts(char *s)
{
	while(*s)
	{
		// wait until data register is empty
		while(!(USART1->ISR & USART_FLAG_TXE));
		USART_SendData(USART1, *s);
		*s++;
	}
//	while(!(USART1->ISR & USART_FLAG_TXE));
//	USART_SendData(USART1, '\r');
//	while(!(USART1->ISR & USART_FLAG_TXE));
//	USART_SendData(USART1, '\n');
}

void clr_term(void)
{
	USART1_putc('\f');
}
