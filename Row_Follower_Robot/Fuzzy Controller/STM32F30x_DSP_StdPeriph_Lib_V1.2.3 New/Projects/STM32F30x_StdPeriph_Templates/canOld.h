#ifdef __CPLUSPLUS
extern "C" {
#endif
uint8_t can_init(uint32_t id);
void clear_rx_packet(void);
void print_CAN_state(CAN_TypeDef *CANx);
void can_tx(void);
  
#ifdef __CPLUSPLUS
}
#endif
