#include "stdint.h"
#include "stdio.h"
#include "main.h"
#include "stm32f30x.h"
#include "led.h"
#include "utils.h"
#include "usart.h"

static __IO  uint32_t uwTimingDelay;
static char msg[50];

void delay(__IO uint32_t nTime)
{
	uwTimingDelay = nTime;
	
	while(uwTimingDelay != 0);
}

void TimingDelay_Decrement(void)
{
	if(uwTimingDelay != 0x0000)
	{
		uwTimingDelay--;
	}
}

void stop(uint8_t blink)
{
	if(blink)
	{
		while(1)
		{
			//sequence_led(GREEN, 70);
			//sequence_led(YELLOW, 70);
			//sequence_led(ORANGE, 70);
			sequence_led(RED, 70);
		}
	}
	else
	{
		while(1);
	}
}

int8_t f_round(float input)
{
	if(input >= 0.0f)
	{
		return (int8_t)(input + 0.5f);
	}
	else
	{
		return (int8_t)(input - 0.5f);
	}
}

void trigger_pin_setup(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable the GPIOA peripheral */ 
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  
  /* Configure MCO1 pin(PA8) in alternate function */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;  
  GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void trigger_pin(uint16_t state)
{
	if(state)
	{
		GPIO_SetBits(GPIOB, GPIO_Pin_1);
	}
	else
	{
		GPIO_ResetBits(GPIOB, GPIO_Pin_1);
	}
}

float f_constrain(float input, float lower, float upper)
{
  float temp = 0.0f;
  
	if(input < lower)
	{
		temp = lower;
	}
	else if(input > upper)
	{
		temp = upper;
	}
	else
	{
		temp = input;
	}
  
  return temp;
}

void error(const char *c, uint8_t halt)
{
  snprintf(msg, 50, "\r\n\n%sError: ", Red);
  USART1_puts(msg);
  while(*(c))
  {
    USART1_putc(*c);
    c++;
  }
  snprintf(msg, 50, "\r\n\n\n");
  USART1_puts(msg);
  if(halt)
  {
    delay(400);
    snprintf(msg, 50, "CPU Stopped  :(%s\r\n", none);
    USART1_puts(msg);
    while(1)
    {
      // Blink RED LED forever
    }
  }
}
