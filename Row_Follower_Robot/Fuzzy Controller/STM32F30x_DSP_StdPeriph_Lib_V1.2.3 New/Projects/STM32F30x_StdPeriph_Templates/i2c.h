
#include "stdlib.h"
#include "stdio.h"
#include "stm32f30x_i2c.h"
#include "stdint.h"

#ifdef __CPLUSPLUS
extern "C" {
#endif

void init_I2C(void);

void I2C_start(uint8_t address, uint16_t direction);
void I2C_write(uint8_t data);
uint8_t I2C_read_ack(void);
uint8_t I2C_read_nack(void);
void I2C_stop(void);
void i2c_write_byte(uint8_t addr, uint8_t data);
uint8_t i2c_read_byte(uint8_t addr);

void lidar_write(uint8_t reg_addr, uint8_t value);
uint16_t lidar_get_distance(void);
void reset_lidar(void);
void init_lidar(void);

void printI2CSettings(GPIO_InitTypeDef GPIO_InitStruct, I2C_InitTypeDef I2C_InitStruct);
#ifdef __CPLUSPLUS
}
#endif
