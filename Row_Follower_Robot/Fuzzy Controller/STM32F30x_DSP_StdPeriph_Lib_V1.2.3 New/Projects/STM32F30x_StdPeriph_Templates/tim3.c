#include "stdint.h"
#include "main.h"
#include "stm32f30x.h"
#include "tim3.h"
#include "stdio.h"
#include "led.h"
#include "utils.h"

// TIM3 is 16 bits
// 64 MHz Clock down to 1 Mhz
// So for 200 Hz, go for 1M * 0.005 ~= 5000 for the period
// Need to enable the interrupt for the sake of can tx
uint32_t freq = 250;

void TIM3_Config(void)
{
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Prescaler = 64 - 1; // TIM3CLK_Frequency (MHz) down to 1 MHz (adjust per your clock)
  TIM_TimeBaseStructure.TIM_Period =  4000 - 1; //  down to 250 Hz (4 ms)
  //TIM_TimeBaseStructure.TIM_Period =  5050 - 1; //  down to 200 Hz (5 ms)
  //TIM_TimeBaseStructure.TIM_Period =  65535 - 1; //  down to something slow 15.25 Hz
  //TIM_TimeBaseStructure.TIM_Period =  20000 - 1; //  down to 50 Hz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
  
  // Generate an internal signal on timer update
  // We'll use this to trigger the ADC periodically
  TIM_SelectOutputTrigger(TIM3, TIM_TRGOSource_Update);
  // Enable the Timer Update interrupt
//  NVIC_InitTypeDef NVIC_InitStructure;
//  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  //NVIC_Init(&NVIC_InitStructure);
  
	/* TIM IT enable */
	//TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	/* TIM2 enable counter */
  
  TIM_OCInitTypeDef TIM_OCInitStructure;

  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 0; // preset pulse width to half of pwm_period
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // Pulse polarity
  TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
  
  // Setup two channels
  // Channel 1, pin PB4
  TIM_OC1Init(TIM3, &TIM_OCInitStructure);
  TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

  // Channel 2, pin PB5
  TIM_OC2Init(TIM3, &TIM_OCInitStructure);
  TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

  // Startup the timer
  TIM_ARRPreloadConfig(TIM2, DISABLE);
  TIM_CtrlPWMOutputs(TIM2, ENABLE);
  An_PWM_Pins_Init();
	TIM_Cmd(TIM3, ENABLE);
}

void PeriodicSampling(uint32_t state)
{
  if(state)
  {
    TIM_Cmd(TIM3, ENABLE);
  }
  else
  {
    TIM_Cmd(TIM3, DISABLE);
  }
}

// Send a CAN msg with global distance data
// Then
// Start a new ADC conversion
//extern "C"
//{
//void TIM3_IRQHandler(void)
//{
//  if(TIM_GetITStatus(TIM3, TIM_FLAG_Update) != RESET)
//  {
//    trigger_pin(HIGH);
//    TIM_ClearITPendingBit(TIM3, TIM_FLAG_Update);
//    //ADC_StartConversion(ADC1);
//    trigger_pin(LOW);
//  }
//}
//}

uint32_t getSamplingFreq(void)
{
  return freq;
}


// Setup PA0 and PA1 as TIM3 outputs, CH1, and CH2 respectively
void An_PWM_Pins_Init(void)
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  GPIO_InitTypeDef GPIO_InitStruct;
  
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStruct);
  
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_2 );  // Ch1
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_2 ); // Ch2

}

// The number range from 18,000 (full reverse)
// to 27,000 (stop) to 36,000 (full forward)
// is placed into the capture/compare register
void TIM3_PWM1_setPeriod(uint32_t period)
{
  TIM3->CCR1 = period;
}

void TIM3_PWM2_setPeriod(uint32_t period)
{
  TIM3->CCR2 = period;
}
