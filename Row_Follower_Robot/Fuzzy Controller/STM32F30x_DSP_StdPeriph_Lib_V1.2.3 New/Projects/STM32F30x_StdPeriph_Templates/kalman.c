#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "arm_math.h"
#include "can.h"
#include "kalman.h"
#include "core_cm4.h"
#include "utils.h"

// Same as number of measurements, sensors
#define SYS_M   1
// Same as number of states in the state variable model
#define SYS_N   1


/***********************************************************************************************************************
* Data Structures and Initializations for Kalman Filtering - Scalars and Matrices
***********************************************************************************************************************/
// Incoming data is in Q11.5 format, format the static objects as such
#ifdef FULL_KALMAN
#ifdef Q15_KALMAN
// Data is in Q11.5
q15_t A_elem = {(1 << 5)};
q15_t H_elem = {(1 << 5)};
q15_t Q_elem = {(1 << 5)};
q15_t R_elem = {(10 << 5)};
q15_t x_elem = {(0 << 5)};
q15_t P_elem = {(20 << 5)};
// Intermediate values for internal calculation
q15_t xp_elem = {0};
q15_t Pp_elem = {0};

// State Transition Matrix - NxN
static arm_matrix_instance_q15 A = {SYS_N, SYS_N, &A_elem};
// State to Measurement Matrix - MxN
static arm_matrix_instance_q15 H = {SYS_M, SYS_N, &H_elem};
// Diagonal System Covariance Matrix - NxN
static arm_matrix_instance_q15 Q = {SYS_N, SYS_N, &Q_elem};
// Diagonal Sensor Covariance Matrix - MxM
static arm_matrix_instance_q15 R = {SYS_M, SYS_M, &R_elem};
// State Variable Inital Condition - Nx1 vector
static arm_matrix_instance_q15 x = {SYS_N, 1, &x_elem};
// Inital guess for error covariance - NxN matrix
static arm_matrix_instance_q15 P = {SYS_N, SYS_N, &P_elem};
// For internal calculation Nx1
static arm_matrix_instance_q15 xp = {SYS_N, 1, &xp_elem};
// NxN
static arm_matrix_instance_q15 Pp = {SYS_N, SYS_N, &Pp_elem};
#endif
#ifdef F32_KALMAN
// Data is in Q11.5
float32_t A_elem = {(1.0f)};
float32_t H_elem = {(1.0f)};
float32_t Q_elem = {(1.0f)};
float32_t R_elem = {(10.0f)};
float32_t x_elem = {(0.0f)};
float32_t P_elem = {(20.0f)};
// Intermediate values for internal calculation
float32_t xp_elem = {0.0f};
float32_t Pp_elem = {0.0f};

// State Transition Matrix - NxN
static arm_matrix_instance_f32 A = {SYS_N, SYS_N, &A_elem};
// State to Measurement Matrix - MxN
static arm_matrix_instance_f32 H = {SYS_M, SYS_N, &H_elem};
// Diagonal System Covariance Matrix - NxN
static arm_matrix_instance_f32 Q = {SYS_N, SYS_N, &Q_elem};
// Diagonal Sensor Covariance Matrix - MxM
static arm_matrix_instance_f32 R = {SYS_M, SYS_M, &R_elem};
// State Variable Inital Condition - Nx1 vector
static arm_matrix_instance_f32 x = {SYS_N, 1, &x_elem};
// Inital guess for error covariance - NxN matrix
static arm_matrix_instance_f32 P = {SYS_N, SYS_N, &P_elem};
// For internal calculation Nx1
static arm_matrix_instance_f32 xp = {SYS_N, 1, &xp_elem};
// NxN
static arm_matrix_instance_f32 Pp = {SYS_N, SYS_N, &Pp_elem};
#endif
#endif
#ifdef SIMPLE_KALMAN
#ifdef Q15_KALMAN
// Data is in Q11.5
q15_t A_elem = {(1 << 5)};
q15_t H_elem = {(1 << 5)};
q15_t Q_elem = {(1 << 5)};
q15_t R_elem = {(10 << 5)};
q15_t x_elem = {(0 << 5)};
q15_t P_elem = {(20 << 5)};
// Intermediate values for internal calculation
q15_t xp_elem = {0};
q15_t Pp_elem = {0};
#endif
#ifdef F32_KALMAN
// Data is in 32 bit floating point format
static float32_t A = {(1.0f)};
static float32_t H = {(1.0f)};
static float32_t Q = {(1.0f)};
static float32_t R = {(10.0f)};
static float32_t x = {(0.0f)};
static float32_t P = {(20.0f)};
// Intermediate values for internal calculation
static float32_t xp = {0.0f};
static float32_t K = {0.0f};
static float32_t Pp = {0.0f};
#endif
  //A = (q15_t)(1 << 5); // = 1 in Q11.5 Fixed point
  //H = (1 << 5);
  //Q = (1 << 3); // .125 for Q11.5 Fixed point
  //R = (50 << 5);
  //x = 0;
  //P = (20 << 5);
#endif

/***********************************************************************************************************************
* Q15 Kalman Function
***********************************************************************************************************************/
q15_t Kalman(q15_t input)
{

#ifdef FULL_KALMAN
    // Set the inital conidtions
    if(arm_mat_mult_q15(&A, &x, &xp, NULL) != ARM_MATH_SUCCESS)
    {
      // print error out
      while(1);
    }
    
#endif  
#ifdef SIMPLE_KALMAN
  
    
  //q15_t xp;
  //q15_t Pp;
  
  // Calculate initial guess
  //arm_mult_q15(&A, &x, &xp, 1);
#endif

  return 0;
}


/***********************************************************************************************************************
* F32 Kalman Function
***********************************************************************************************************************/
float32_t Kalmanf(float32_t input)
{

#ifdef FULL_KALMAN
    // Set the inital conidtions
    if(arm_mat_mult_f32(&A, &x, &xp, NULL) != ARM_MATH_SUCCESS)
    {
      // print error out
      while(1);
    }
    
#endif  
#ifdef SIMPLE_KALMAN
  
  // Start prediction process 
    
  // These other lines are commneted out
  // for the extremely simple 1D Kalman filter case where
  // A = 1 and H = 1 - a 1D smoothing filter
  //xp = A*x;
  xp = x;
  //Pp = (A*P*A)+Q;
  Pp = P + Q;
  //K = Pp * H * (1/((H*Pp*H) + R));
  K = Pp * (1.0f / (Pp + R));
  //x = xp + (K * (input - (H * xp)));
  x = xp + (K * (input - xp));
  //P = Pp - (K * H * Pp);  
  P = Pp - (K * Pp);
    
#endif

  return x;
}
