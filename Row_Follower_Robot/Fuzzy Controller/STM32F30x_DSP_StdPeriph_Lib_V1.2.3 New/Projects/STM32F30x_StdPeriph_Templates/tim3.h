#ifdef __CPLUSPLUS
extern "C" {
#endif
void TIM3_Config(void);
void PeriodicSampling(uint32_t state);
uint32_t getSamplingFreq(void);
void An_PWM_Pins_Init(void);
void TIM3_PWM1_setPeriod(uint32_t period);
void TIM3_PWM2_setPeriod(uint32_t period);
#ifdef __CPLUSPLUS
}
#endif
