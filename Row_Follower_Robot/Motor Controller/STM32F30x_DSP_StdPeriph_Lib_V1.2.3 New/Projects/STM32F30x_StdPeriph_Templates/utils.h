#ifdef __CPLUSPLUS
extern "C" {
#endif
void delay(__IO uint32_t nTime);
void TimingDelay_Decrement(void);
void stop(uint8_t blink);
int8_t f_round(float input);
void trigger_pin_setup(void);
void trigger_pin(uint16_t state);
float f_constrain(float input, float lower, float upper);
void error(const char *c, uint8_t halt);
#ifdef __CPLUSPLUS
}
#endif
