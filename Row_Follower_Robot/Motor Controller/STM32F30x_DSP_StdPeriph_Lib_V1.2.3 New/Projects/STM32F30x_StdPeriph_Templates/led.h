#define ON	 (uint32_t)1
#define OFF  (uint32_t)0
#define HIGH (uint32_t)1
#define LOW	 (uint32_t)0

#define STATUS_LED	GPIO_Pin_1
#define RED			    GPIO_Pin_1

void Status_LED_Setup(void);
void Local_LED_Setup(void);
void LOCAL_LED(uint16_t state);
void LED(uint16_t led, uint16_t state);
void sequence_led(uint16_t led, uint32_t time);
