#include "main.h"
#include "usart.h"
#include "stm32f30x.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "can.h"
#include "utils.h"
#include "led.h"
#include "i2c.h"
#include "sabertooth.h"
#include "controller.h"
#include "vector.h"
#include "adc.h"
#include "tim3.h"

//#include "FuzzyRule.h"
//#include "FuzzyComposition.h"
//#include "Fuzzy.h"
//#include "FuzzyRuleConsequent.h"
//#include "FuzzyOutput.h"
//#include "FuzzyInput.h"
//#include "FuzzyIO.h"
//#include "FuzzySet.h"
//#include "FuzzyRuleAntecedent.h"

#define FWD 1
#define RWD 2
#define SIZE (uint8_t)120

#define RX_ID	      200
//#define FWD_SPEED   50.0f
#define FWD_SPEED   75.0f

uint8_t status;
uint16_t dist = 0;
uint8_t i = 0;
uint16_t j = 0;
uint16_t CAN1_ID;
uint8_t CAN1_DATA0;
uint8_t CAN1_DATA1;
uint8_t CAN1_DATA2;
uint8_t CAN1_DATA3;
uint8_t CAN1_DATA4;
uint8_t CAN1_DATA5;
uint8_t CAN1_DATA6;
uint8_t CAN1_DATA7;

// 10% of max fwd speed
//const uint16_t bias = VECTOR_FULL_FWD / (uint16_t)10;

float distR = 10.33f;
float distL = 25.46f;
float DistInput = 0.0f;
float rowInput = 0.0f;
float steering = 0.0f;

__IO uint32_t doneSampling = 0;;
char msg[SIZE];
__IO uint16_t ADC1ConvertedValue = 0;
__IO uint16_t ADC1ConvertedVoltage = 0;
__IO float ADC1ConvertedVoltageF = 0.0f;

uint32_t unitDelay = 200;
CanTxMsg TxMessage;
CanTxMsg TxMessage2;
CanTxMsg startRec;
CanTxMsg stopRec;
CanRxMsg RxMessage;
uint8_t buf_size;
uint16_t currentTime;
uint16_t previousTime;
uint16_t deltaTime;
uint16_t frameCounter;
uint32_t run_flag = 0;
uint8_t msg_counter1 = 0;
uint8_t msg_counter2 = 0;
volatile uint8_t remote_flags[2];
uint8_t lastRotateFlag = 0;

uint32_t recState = 0;

volatile uint8_t data0;

void process_data(void);
void rotateRobot(void);
void print_data(void);
void scheduler_checkTime(void);
void drive(uint32_t TIM_Period_start, uint32_t TIM_Period_stop);

//Fuzzy* fuzzy = new Fuzzy();

int main(void)
{
	RCC_ClocksTypeDef SYS_Clocks;
	RCC_GetClocksFreq(&SYS_Clocks);

	if(SysTick_Config(SYS_Clocks.HCLK_Frequency / 1000))
	{
		while(1);
	}
	
  Local_LED_Setup();
  Status_LED_Setup();
	init_USART();
	clr_term();

	snprintf(msg, SIZE, "%s%s%s Tyler Troyer - Motor CAN Node 0V2 - Target: STMicroelectronics STM32F303K8T6 %u MHz \r\n%s", none, Back_Blue, On_Red, SYS_Clocks.SYSCLK_Frequency/1000000, none);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\n%sSystem Clocks:\r\n", none);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tSYSCLK:%dM\r\n",SYS_Clocks.SYSCLK_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tHCLK:%dM\r\n",SYS_Clocks.HCLK_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tPCLK1:%dM\r\n",SYS_Clocks.PCLK1_Frequency/1000000);
	USART1_puts(msg);
	snprintf(msg, SIZE, "\tPCLK2:%dM\r\n",SYS_Clocks.PCLK2_Frequency/1000000);
	USART1_puts(msg);
	delay(1000);
	snprintf(msg, SIZE, "Setting up CAN Peripheral... ");
	USART1_puts(msg);
	
  status = CAN1_Config(RX_ID);
	if(status != CAN_InitStatus_Success)
	{
    while(status != CAN_InitStatus_Success)
    {
      status = CAN1_Config(RX_ID);
      snprintf(msg, SIZE, "\rCAN Config() returned %u     ", status);
      USART1_puts(msg);
      //print_CAN_state(CAN1);
      LED(RED,1);
    }
    // CAN ready to go
    LED(RED,0);
	}
  
  TxMessage.StdId = 210;
  TxMessage.IDE = CAN_Id_Standard;
  TxMessage.DLC = 8;
  TxMessage.RTR = CAN_RTR_Data;
  TxMessage.Data[0] = 0;
  TxMessage.Data[1] = 20;
  TxMessage.Data[2] = 30;
  TxMessage.Data[3] = 40;
  TxMessage.Data[0] = 50;
  TxMessage.Data[1] = 60;
  TxMessage.Data[2] = 70;
  TxMessage.Data[3] = 80; 
  
  TxMessage2.StdId = 211;
  TxMessage2.IDE = CAN_Id_Standard;
  TxMessage2.DLC = 4;
  TxMessage2.RTR = CAN_RTR_Data;
  TxMessage2.Data[0] = 10;
  TxMessage2.Data[1] = 20;
  TxMessage2.Data[2] = 30;
  TxMessage2.Data[3] = 40;
  
  startRec.StdId = 500;
  startRec.IDE = CAN_Id_Standard;
  startRec.DLC = 8;
  startRec.RTR = CAN_RTR_Data;
  startRec.Data[0] = 10;
  startRec.Data[1] = 10;
  startRec.Data[2] = 10;
  startRec.Data[3] = 10;
  startRec.Data[0] = 10;
  startRec.Data[1] = 10;
  startRec.Data[2] = 10;
  startRec.Data[3] = 10;
  
  stopRec.StdId = 501;
  stopRec.IDE = CAN_Id_Standard;
  stopRec.DLC = 8;
  stopRec.RTR = CAN_RTR_Data;
  stopRec.Data[0] = 10;
  stopRec.Data[1] = 10;
  stopRec.Data[2] = 10;
  stopRec.Data[3] = 10;
  stopRec.Data[0] = 10;
  stopRec.Data[1] = 10;
  stopRec.Data[2] = 10;
  stopRec.Data[3] = 10; 

	snprintf(msg, SIZE, "Done!\r\n");
	USART1_puts(msg);
	
	snprintf(msg, 50, "main_thread()\r\n\n");
	USART1_puts(msg);
  
  TIM2_Init_PWM();
  TIM3_Config();
  ADCInit();
 
  while(1)
  {
    process_data();
    print_data();
    delay(100);
  }
}

void process_data(void)
{ 
  if(!can_rx_fifo_empty())
  {
    buf_size = can_rx_fifo_size();
    
    for(i = 0; i < buf_size; i++)
    {
      if(can_fifo_read(&RxMessage))
      {
        if(RxMessage.StdId == RX_ID)
        {
          if(remote_flags[0])
          {
            // Drive forward according to the control node's advice
            VECTOR_CMD(FWD_SPEED + (float)(RxMessage.Data[0] - 127), FWD_SPEED - (1.5f*(float)(RxMessage.Data[0] - 127)));
            
            if(recState == 0)
            {
              // Send recording start msg to logger
              CAN_Transmit(CAN1, &startRec);
              recState = 1;
            }
          }
          else
          {
            // Forget about it
            VECTOR_CMD(0.0f, 0.0f);
            
            if(recState)
            {
              // Send recording start msg to logger
              CAN_Transmit(CAN1, &stopRec);
              recState = 0;
            }
          }
        }
      }
      else
      {
        break;
      }
    }
  }
}

void rotateRobot(void)
{
  VECTOR_CMD(150.0f, 0.0f);
  delay(4000);
  VECTOR_CMD(0.0f, 0.0f);
}

void print_data(void)
{
  snprintf(msg, 50, "%s%s", Green, "\f-------System Monitor-------");
  USART1_puts(msg);
  printState_Can();
  printState_ADC();
  printState_Vector();
  //snprintf(msg, 50, "%s\r%s", Green, "---------------------------\r\n");
  //USART1_puts(msg);
}

void scheduler_checkTime(void)
{
  currentTime = TimeNow();
  
  if(currentTime < previousTime)
  {
    deltaTime = currentTime + 200 - previousTime;
  }
  else
  {
    deltaTime = currentTime - previousTime;
  }
}

void drive(uint32_t TIM_Period_start, uint32_t TIM_Period_stop)
{
   for(j = TIM_Period_start; j < TIM_Period_stop; j+=100)
  {
    TIM2_PWM3_setPeriod(j);
    TIM2_PWM4_setPeriod(j);
    print_data();
    delay(50);
  }
  print_data();
  while(getReading1() > 2000)
  {
    TIM2_PWM3_setPeriod(TIM_Period_stop);
    TIM2_PWM4_setPeriod(TIM_Period_stop);
    print_data();
    delay(100);
  }
  for(j = TIM_Period_stop; j > TIM_Period_start; j-=100)
  {
    TIM2_PWM3_setPeriod(j);
    TIM2_PWM4_setPeriod(j);
    print_data();
    delay(50);
  }
}
