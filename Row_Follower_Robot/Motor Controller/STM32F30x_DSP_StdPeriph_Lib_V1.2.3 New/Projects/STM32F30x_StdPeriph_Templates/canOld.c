#include "stdint.h"
#include "main.h"
#include "stdio.h"
#include "usart.h"
#include "stm32f30x.h"
#include "led.h"
#include "utils.h"
#include "can.h"

#define CAN_TX	GPIO_Pin_12
#define CAN_RX	GPIO_Pin_11

extern uint16_t CAN1_ID;
extern uint8_t CAN1_DATA0;
extern uint8_t CAN1_DATA1;
extern uint8_t CAN1_DATA2;
extern uint8_t CAN1_DATA3;
extern uint8_t CAN1_DATA4;
extern uint8_t CAN1_DATA5;
extern uint8_t CAN1_DATA6;
extern uint8_t CAN1_DATA7;

extern CanTxMsg TxMessage;

uint8_t can_init(uint32_t id)
{
	uint8_t status = 0;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_9);	// RX
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_9); // TX
	
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = CAN_TX|CAN_RX;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	//GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	CAN_InitTypeDef CAN;
	CAN_DeInit(CAN1);
	CAN_StructInit(&CAN);
	
	CAN.CAN_Prescaler = 4;
	
//#define CAN_Mode_Normal             ((uint8_t)0x00)  /*!< normal mode */	// 
//#define CAN_Mode_LoopBack           ((uint8_t)0x01)  /*!< loopback mode */	// ok
//#define CAN_Mode_Silent             ((uint8_t)0x02)  /*!< silent mode */	// ok
//#define CAN_Mode_Silent_LoopBack    ((uint8_t)0x03)  /*!< loopback combined with silent mode */ // ok
	CAN.CAN_Mode = CAN_Mode_Normal;

	// for 250k @ 18MHz
	CAN.CAN_SJW = CAN_SJW_4tq;
	CAN.CAN_BS1 = CAN_BS1_15tq;
	CAN.CAN_BS2 = CAN_BS2_2tq;
	CAN.CAN_TTCM = DISABLE;
	CAN.CAN_ABOM = DISABLE;
	CAN.CAN_AWUM = DISABLE;
	CAN.CAN_NART = DISABLE;
	CAN.CAN_RFLM = DISABLE;
	CAN.CAN_TXFP = DISABLE;
	status = CAN_Init(CAN1, &CAN);
	
	CAN_FilterInitTypeDef FilterSet;
	FilterSet.CAN_FilterIdHigh = (uint32_t)id << 5;	// for Fuzzy node msg acceptance
	//FilterSet.CAN_FilterIdHigh = 0x0000;
	FilterSet.CAN_FilterIdLow = 0x0000;
	FilterSet.CAN_FilterMaskIdHigh = 0x0000;
	FilterSet.CAN_FilterMaskIdLow = 0x0000;
	FilterSet.CAN_FilterFIFOAssignment = 0;
	FilterSet.CAN_FilterNumber = 0;
	FilterSet.CAN_FilterMode = CAN_FilterMode_IdList;
	FilterSet.CAN_FilterScale = CAN_FilterScale_16bit;
	FilterSet.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&FilterSet);
	
	return status;
}

void clear_rx_packet(void)
{
	CAN1_ID = 0;
	CAN1_DATA0 = 0;
	CAN1_DATA1 = 0;
	CAN1_DATA2 = 0;
	CAN1_DATA3 = 0;
	CAN1_DATA4 = 0;
	CAN1_DATA5 = 0;
	CAN1_DATA6 = 0;
	CAN1_DATA7 = 0;
}

void print_CAN_state(CAN_TypeDef *CANx)
{
	const uint8_t size = 30;
	char dbgMsg[size];
	
	snprintf(dbgMsg, size, "\n%sCAN Module State:\r\n\n", Cyan);
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tESR \t\t\t\t= 0x%x\r\n", CANx->ESR);
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tFlags \t\t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_LEC));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tError Warning \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_EWG));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tError Passive \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_EPV));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tBus-off \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_BOF));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tLast Error Code \t\t= %u\r\n", CAN_GetLastErrorCode(CANx));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tReceive Error Counter \t\t= %u\r\n", CAN_GetReceiveErrorCounter(CANx));
	USART1_puts(dbgMsg);
	snprintf(dbgMsg, size, "\tTransmit Error Counter \t\t= %u\r\n%s", CAN_GetLSBTransmitErrorCounter(CANx), none);
	USART1_puts(dbgMsg);
	
	uint8_t error = CAN_GetLastErrorCode(CAN1);
	
	if(error & CAN_ErrorCode_NoErr)
	{
		snprintf(dbgMsg, size, "\nCAN_ErrorCode_NoErr\r\n");
		USART1_puts(dbgMsg);
	}
	else
	{
		if(error & CAN_ErrorCode_StuffErr)
		{
			snprintf(dbgMsg, size, "\nCAN_ErrorCode_StuffErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_FormErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_FormErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_ACKErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_ACKErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_BitRecessiveErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_BitRecessiveErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_BitDominantErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_BitDominantErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_CRCErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_CRCErr\r\n");
			USART1_puts(dbgMsg);
		}
		if(error & CAN_ErrorCode_SoftwareSetErr)
		{
			snprintf(dbgMsg, size, "CAN_ErrorCode_SoftwareSetErr\r\n");
			USART1_puts(dbgMsg);
		}
	}
}

void can_tx(void)
{	
	CAN_Transmit(CAN1, &TxMessage);
	
	trigger_pin(HIGH);
	while(CAN_TransmitStatus(CAN1, 0) == CAN_TxStatus_Pending);
	trigger_pin(LOW);
		
		
//		if(CAN_TransmitStatus(CAN1, 0) == CAN_TxStatus_Ok)
//		{
//			LED(RED, OFF);
//			print_CAN_state(CAN1);
//			USART1_puts("\nTx OK\r\n");
//			stop(0);
//		}
//		else if(CAN_TransmitStatus(CAN1, 0) == CAN_TxStatus_Failed)
//		{
//			LED(ORANGE, ON);
//			print_CAN_state(CAN1);
//			USART1_puts("\nTx Failed\r\n");
//			stop(1);
//		}
//				else if(CAN_TransmitStatus(CAN1, 0) == CAN_TxStatus_Pending)
//		{
//			LED(GREEN, ON);
//			print_CAN_state(CAN1);
//			USART1_puts("\nTx Pending\r\n");
//			stop(1);
//		}
//				else if(CAN_TransmitStatus(CAN1, 0) == CAN_TxStatus_NoMailBox)
//		{
//			LED(GREEN, ON);
//			print_CAN_state(CAN1);
//			USART1_puts("\nNo Tx Mailbox Available\r\n");
//			stop(1);
//		}
//		else
//		{
//			print_CAN_state(CAN1);
//			USART1_puts("\nProgram halted\r\n");
//			stop(1);
//		}
}
