#include "stdint.h"
#include "main.h"
#include "stm32f30x.h"
#include "controller.h"

#include "FuzzyRule.h"
#include "FuzzyComposition.h"
#include "Fuzzy.h"
#include "FuzzyRuleConsequent.h"
#include "FuzzyOutput.h"
#include "FuzzyInput.h"
#include "FuzzyIO.h"
#include "FuzzySet.h"
#include "FuzzyRuleAntecedent.h"

static Fuzzy* fuzzy;

void fuzzy_controller_init(void)
{
  // 2 FuzzyInputs
  FuzzyInput* rowWidth = new FuzzyInput(1);
  FuzzyInput* distDiff = new FuzzyInput(2);

  // 1 FuzzyOutput
  FuzzyOutput* motorSpeedDiff = new FuzzyOutput(1);
	
	  /*************************************
   * Input 1
   ************************************/
  // Two "crossing ramp" sets for rowWidth i.e. "tolerance" of the row
  FuzzySet* lowTolerance  = new FuzzySet(0, 0, 0, 120);
  FuzzySet* highTolerance = new FuzzySet(0, 120, 120, 120);

  // Add these memberships to input set
  rowWidth->addFuzzySet(lowTolerance);
  rowWidth->addFuzzySet(highTolerance);

  /*************************************
   * Input 2
   ************************************/
  // Five Sets for "difference between R and L distances"
  FuzzySet* tooFarRight   = new FuzzySet(-60, -60, -54, -30);
  FuzzySet* right         = new FuzzySet(-54, -30, -30, 0);
  FuzzySet* centered      = new FuzzySet(-30, 0, 0, 30);
  FuzzySet* left          = new FuzzySet(0, 30, 30, 54);
  FuzzySet* tooFarLeft    = new FuzzySet(30, 54, 60, 60);

  // Add these memberships to input set
  distDiff->addFuzzySet(tooFarRight);
  distDiff->addFuzzySet(right);
  distDiff->addFuzzySet(centered);
  distDiff->addFuzzySet(left);
  distDiff->addFuzzySet(tooFarLeft);

  /*************************************
   * Output
   ************************************/
  // Seven sets for steering modes to take (close ones narrow far ones wider)
  FuzzySet* hardRight   = new FuzzySet(-30, -30, -30, -15);
  FuzzySet* nomRight    = new FuzzySet(-30, -15, -15, -5);
  FuzzySet* lightRight  = new FuzzySet(-15, -5, -5, 0);
  FuzzySet* goStraight  = new FuzzySet(-5, 0, 0, 5);
  FuzzySet* lightLeft   = new FuzzySet(0, 5, 5, 15);
  FuzzySet* nomLeft     = new FuzzySet(5, 15, 15, 30);
  FuzzySet* hardLeft    = new FuzzySet(15, 30, 30, 30);

  // Add these memberships to our output
  motorSpeedDiff->addFuzzySet(hardRight);
  motorSpeedDiff->addFuzzySet(nomRight);
  motorSpeedDiff->addFuzzySet(lightRight);
  motorSpeedDiff->addFuzzySet(goStraight);
  motorSpeedDiff->addFuzzySet(lightLeft);
  motorSpeedDiff->addFuzzySet(nomLeft);
  motorSpeedDiff->addFuzzySet(hardLeft);

  /************************************************************************************************************
   * Inputs and Output are now established, we need to add them to our fuzzy system
   ***********************************************************************************************************/
  fuzzy->addFuzzyInput(rowWidth);
  fuzzy->addFuzzyInput(distDiff);
  fuzzy->addFuzzyOutput(motorSpeedDiff);


  /************************************************************************************************************
   * Fuzzy Rule Establishment
   ***********************************************************************************************************/
  // These are the 10 possible INPUT states
  // These antecdents are combos of one set from input1 and one set from input2
  // Here we declare them and give them names
  FuzzyRuleAntecedent* ifHighToleranceCentered  = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifHighTolRight           = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifHighTolTooRight        = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifHighTolLeft            = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifHighTolTooLeft         = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifLowTolCentered         = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifLowTolLeft             = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifLowTolTooLeft          = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifLowTolRight            = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifLowTolTooRight         = new FuzzyRuleAntecedent();
  FuzzyRuleAntecedent* ifCentered               = new FuzzyRuleAntecedent();  // why this one?

  // Now we fill each antecedent with the combo of sets which define it
  ifHighToleranceCentered->joinWithAND(highTolerance, centered);
  ifHighTolRight->joinWithAND(highTolerance, right);
  ifHighTolTooRight->joinWithAND(highTolerance, tooFarRight);
  ifHighTolLeft->joinWithAND(highTolerance, left);
  ifHighTolTooLeft->joinWithAND(highTolerance, tooFarLeft);
  ifLowTolCentered->joinWithAND(lowTolerance, centered);
  ifLowTolLeft->joinWithAND(lowTolerance, left);
  ifLowTolTooLeft->joinWithAND(lowTolerance, tooFarLeft);
  ifLowTolRight->joinWithAND(lowTolerance, right);
  ifLowTolTooRight->joinWithAND(lowTolerance, tooFarRight);
  ifCentered->joinWithOR(ifHighToleranceCentered, ifLowTolCentered);  // do we need this one?

  // These are the 7 possible OUTPUT states, need to make this 5, since two of them are technically identical
  FuzzyRuleConsequent* thenKeepCentered         = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSpeedLightLeft       = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSpeedNomLeft         = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSpeedHardLeft        = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSpeedLightRight      = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSpeedNomRight        = new FuzzyRuleConsequent();
  FuzzyRuleConsequent* thenSpeedHardRight       = new FuzzyRuleConsequent();

  // Add the appropriate sets to the output states we defined above
  thenKeepCentered->addOutput(goStraight);
  thenSpeedNomLeft->addOutput(nomLeft);
  thenSpeedNomRight->addOutput(nomRight);
  thenSpeedHardRight->addOutput(hardRight);
  thenSpeedHardLeft->addOutput(hardLeft);
  thenSpeedLightRight->addOutput(lightRight);
  thenSpeedLightLeft->addOutput(lightLeft);
  
  // Building FuzzyRules - NOTICE, some of these could be combined with OR since they
  // share the same output consequence, rules 2&3&, 4&5, 1&6(since HighTolKeepCentered and LowTolKeepCentered are both the "goStraight" output set
  FuzzyRule* fuzzyRule1   = new FuzzyRule(1, ifCentered, thenKeepCentered);
  FuzzyRule* fuzzyRule2   = new FuzzyRule(2, ifHighTolRight, thenSpeedLightLeft);
  FuzzyRule* fuzzyRule3   = new FuzzyRule(3, ifHighTolTooRight, thenSpeedNomLeft);
  FuzzyRule* fuzzyRule4   = new FuzzyRule(4, ifHighTolLeft, thenSpeedLightRight);
  FuzzyRule* fuzzyRule5   = new FuzzyRule(5, ifHighTolTooLeft, thenSpeedNomRight);
  FuzzyRule* fuzzyRule6   = new FuzzyRule(6, ifLowTolLeft, thenSpeedNomRight);
  FuzzyRule* fuzzyRule7   = new FuzzyRule(7, ifLowTolTooLeft, thenSpeedHardRight);
  FuzzyRule* fuzzyRule8   = new FuzzyRule(8, ifLowTolRight, thenSpeedNomLeft);
  FuzzyRule* fuzzyRule9  = new FuzzyRule(9, ifLowTolTooRight, thenSpeedHardLeft);

  // Add these rules to our controller
  fuzzy->addFuzzyRule(fuzzyRule1);
  fuzzy->addFuzzyRule(fuzzyRule2);
  fuzzy->addFuzzyRule(fuzzyRule3);
  fuzzy->addFuzzyRule(fuzzyRule4);
  fuzzy->addFuzzyRule(fuzzyRule5);
  fuzzy->addFuzzyRule(fuzzyRule6);
  fuzzy->addFuzzyRule(fuzzyRule7);
  fuzzy->addFuzzyRule(fuzzyRule8);
  fuzzy->addFuzzyRule(fuzzyRule9);
}
