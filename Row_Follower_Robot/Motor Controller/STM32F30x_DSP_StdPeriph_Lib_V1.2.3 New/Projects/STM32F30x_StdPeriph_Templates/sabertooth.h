#define SABER_M1_FULL_FWD 127
#define SABER_M1_STOP		  64
#define SABER_M1_FULL_REV 1

#define SABER_M2_FULL_FWD 255
#define SABER_M2_STOP		  192
#define SABER_M2_FULL_REV 128

//motor level to send when issuing full stop command
#define SABER_ALL_STOP  0

#ifdef __CPLUSPLUS
extern "C" {
#endif

void init_sabertooth(GPIO_TypeDef *GPIOx, uint16_t pin, uint8_t pinSource);
void motor_speed(uint8_t motor, uint8_t power, uint8_t direction);
void USART2_putc(char c);
int8_t constrain(int8_t input, int8_t lowerBound, int8_t upperBound);
uint8_t get_direction(int8_t input);
  
#ifdef __CPLUSPLUS
}
#endif
