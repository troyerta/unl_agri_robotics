#include "stdint.h"
#include "stm32f4xx.h"

// Select one:
//#define FULL_KALMAN   0
//#define SIMPLE_KALMAN 1

// Select one:
//#define Q15_KALMAN    0
//#define F32_KALMAN    1

// Define the correct function prototype
//#ifdef Q15_KALMAN
//q15_t Kalman(q15_t input);
//#endif
//#ifdef FLOAT_KALMAN
float32_t Kalmanf(float32_t input);
//#endif
