#include "adc.h"
#include "stm32f30x.h"
#include "usart.h"
#include "stdint.h"
#include "stdio.h"
#include "utils.h"
#include "can.h"
#include "vector.h"
#include "tim3.h"
  
__IO uint16_t calibration_value = 0;
extern CanTxMsg TxMessage;
extern __IO uint32_t doneSampling;
uint16_t ADC12ConvertedValue[2];
float voltages[2];
float speed1;
float speed2;
uint16_t distanceCm[2];
uint8_t tx_mbx;
static char dmsg[100];
uint8_t fullCounter = 0;
static uint16_t sampleCounter;
extern volatile uint8_t data0;
extern volatile uint8_t remote_flags[2];

uint32_t getReading1(void)
{
  return ADC12ConvertedValue[0];
}

uint32_t getReading2(void)
{
  return ADC12ConvertedValue[1];
}

float getVoltage1(void)
{
  return voltages[0];
}

float getVoltage2(void)
{
  return voltages[1];
}

void ADCInit(void)
{
  ADC_InitTypeDef       ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  
  /* Configure the ADC clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2);
  
  /* Enable ADC1 clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);
      
  SetupAdcGpio();
  
  CalibrateAdc();
  
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_Circular;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;
  ADC_CommonInit( ADC1, &ADC_CommonInitStructure );
  
  ADC_StructInit( &ADC_InitStructure );
  ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Disable;
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_4;
  ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_RisingEdge;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
  ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
  ADC_InitStructure.ADC_NbrOfRegChannel = 2;
  ADC_Init(ADC1, &ADC_InitStructure);
  
  ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_181Cycles5); // PA0 // Analog 2
  ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 2, ADC_SampleTime_181Cycles5); // PA1 // Analog 1
  
  ADC_DMAConfig(ADC1, ADC_DMAMode_Circular);
  
  /* Enable ADC3 */
  ADC_Cmd(ADC1, ENABLE);
  
  /* wait for ADRDY */
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY));
  
  /* Enable the DMA Channel */
  SetupAdcDma();
  DMA_Cmd(DMA1_Channel1, ENABLE);
  /* Enable ADC3 DMA */
  ADC_DMACmd(ADC1, ENABLE);
  ADC_StartConversion(ADC1);
}

void SetupAdcGpio(void)
{
  GPIO_InitTypeDef    GPIO_InitStructure;
  
  /* GPIOC Periph clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* Configure ADC Channel1 and 2 as analog input */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void SetupAdcDma(void)
{
  /* DMA configuration -----------------------------------------------------*/
  DMA_InitTypeDef   DMA_InitStructure;

  /* DMA1 channel1 configuration -------------------------------------------*/
  DMA_DeInit(DMA1_Channel1);
  //DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &ADC1_2->CDR;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &ADC1->DR;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &ADC12ConvertedValue[0];
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = 2;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  //DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);

  //Enable the DMA "transfer complete" (TC) interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  DMA_ITConfig(DMA1_Channel1, DMA1_IT_TC1, ENABLE);
}

void CalibrateAdc(void)
{
  __IO uint16_t calibration_value_1 = 0;
  __IO uint16_t calibration_value_2 = 0;

  /* Setup SysTick Timer for 1 �sec interrupts  */
  if (SysTick_Config(SystemCoreClock / 1000000))
  { 
    /* Capture error */ 
    while (1)
    {}
  }

  /* ADC Calibration procedure */
  ADC_VoltageRegulatorCmd(ADC1, ENABLE);

  /* Insert delay equal to 10 �s */
  delay(10);


  ADC_SelectCalibrationMode(ADC1, ADC_CalibrationMode_Single);
  ADC_StartCalibration(ADC1);

  while(ADC_GetCalibrationStatus(ADC1) != RESET);
  calibration_value_1 = ADC_GetCalibrationValue(ADC1);
  
    /* Setup SysTick Timer for 1 �sec interrupts  */
  if (SysTick_Config(SystemCoreClock / 1000))
  { 
    /* Capture error */ 
    while (1)
    {}
  }
}

extern "C"
{
void DMA1_Channel1_IRQHandler(void)
{
  if(DMA_GetITStatus(DMA1_IT_TC1) != RESET)
  {
    DMA_ClearITPendingBit(DMA1_IT_TC1);
    
    if(ADC12ConvertedValue[0] > 2000)
    {
      remote_flags[0] = 1;
    }
    else
    {
      remote_flags[0] = 0;
    }
    if(ADC12ConvertedValue[1] > 2000)
    {
      remote_flags[1] = 1;
    }
    else
    {
      remote_flags[1] = 0;
    }
    
    // Update the PWM pin values for each channel
    TIM3_PWM1_setPeriod((uint32_t)((float)ADC12ConvertedValue[0]*1.2332f));
    TIM3_PWM2_setPeriod((uint32_t)((float)ADC12ConvertedValue[1]*1.2332f));

    doneSampling = 1;
    sampleCounter = (sampleCounter + 1) % 200;
  }
}
}

void printState_ADC(void)
{
  snprintf(dmsg, 100, "%s\r\nRemote Control Watcher:%s\r\n", UYellow, none);
  USART1_puts(dmsg);
  snprintf(dmsg, 100, "A:\r\n\tCounts\t\t%u\r\n\tA Flag\t\t%u\r\n", ADC12ConvertedValue[0], remote_flags[0]);
  USART1_puts(dmsg);
  snprintf(dmsg, 100, "\rB:\r\n\tCounts\t\t%u\r\n\tB Flag\t\t%u\r\n\n", ADC12ConvertedValue[1], remote_flags[1]);
  USART1_puts(dmsg);
}

uint16_t TimeNow(void)
{
  return sampleCounter;
}
