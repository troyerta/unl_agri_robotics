
#include "stdlib.h"
#include "stdio.h"
#include "stm32f30x_i2c.h"
#include "stdint.h"

#ifdef __CPLUSPLUS
extern "C" {
#endif

//void ADC_Config(void);
//void DMA_Config(void);
void ADCInit(void);
void SetupAdcGpio(void);
void SetupAdcDma(void);
void CalibrateAdc(void);
uint32_t getReading1(void);
uint32_t getReading2(void);
float getVoltage1(void);
float getVoltage2(void);
void printState_ADC(void);
uint16_t TimeNow(void);
#ifdef __CPLUSPLUS
}
#endif
