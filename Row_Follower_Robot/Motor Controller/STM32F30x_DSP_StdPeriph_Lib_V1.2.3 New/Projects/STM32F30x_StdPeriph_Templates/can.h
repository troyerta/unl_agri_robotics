#ifdef __CPLUSPLUS
extern "C" {
#endif
uint8_t CAN1_Config(uint32_t filterID);
void can_tx(CAN_TypeDef *CANx, CanTxMsg* TxMessage);

uint8_t can_rx_fifo_empty(void);
uint8_t can_rx_fifo_full(void);
uint8_t can_rx_fifo_write(CanRxMsg msg);
uint8_t can_fifo_read(CanRxMsg *msg);
uint8_t can_rx_fifo_size(void);
void copyCanPacket(CanRxMsg* ToMessage, CanRxMsg* FromMessage);
void print_CAN_state(CAN_TypeDef *CANx);
void printState_Can(void);
#ifdef __CPLUSPLUS
}
#endif
