/*
 * usart.h
 *
 *  Created on: Oct 12, 2015
 *      Author: ttroyer2
 */

#ifndef USART_H_
#define USART_H_

/********************************************************************************************************************
 *
 * Function Prototypes
 *
 *******************************************************************************************************************/
void USART1_Configuration(void);
void USART2_Configuration(void);
void USART3_Configuration(void);
void USART6_Configuration(void);
void UART4_Configuration(void);
void UART5_Configuration(void);
void USART6_putc(char c);
void USART3_putc(char c);
void USART2_putc(char c);
void USART1_putc(char c);
void UART4_putc(char c);
void UART5_putc(char c);
uint8_t USART2_getc(void);
char USART3_getc(void);
char USART1_getc(void);
char UART4_getc(void);
char UART5_getc(void);
char USART6_getc(void);

#endif /* USART_H_ */
