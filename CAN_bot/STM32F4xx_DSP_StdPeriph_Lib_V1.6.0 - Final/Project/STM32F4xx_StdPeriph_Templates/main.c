/**
  *****************************************************************************
  * @file		 2 Channel Lidar to Analog Converter main.c
  * @author	 Tyler Troyer
	* @version V2.2.1
	* @date		 06-February-2016
	* @brief	 Main program body
	*****************************************************************************
  * @attention
	*
	* Non-RTX version
	* I2C1 and I2C2 are bus masters to left and right lidars respectively.
	* Lidar readings are mapped to real centimeters via look-up table.
	* cM Results are mapped to 8 bit DAC scale via LUT that assumes no V offset.
	* Sampling rate is directly affected by the loop delay, is therefore approximated.
	* nopDelay() can fine tune Fs, making it more accurate at system clock resolution.
	* 1/180M = 6 nS resolution
	* Further revisions have no need for RTX until heavy processing is required
	*****************************************************************************
	*/

/**************************************************************************************************************
* Includes
***************************************************************************************************************/
#include "main.h"
#include "usart.h"
#include "pwmInput.h"
#include "tim2.h"
#include "i2c.h"
#include "utils.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "lidar.h"
#include "dac.h"
#include "can.h"
#include "init.h"
#include "map.h"
#include "watchdog.h"
#include "stats.h"
#include "arm_math.h"

/**************************************************************************************************************
* Defines
***************************************************************************************************************/
#define VERBOSE 	(uint8_t)1
#define QUIET			(uint8_t)0
#define WATCHDOG	(uint8_t)1
#define NO_IDWG		(uint8_t)0
#define BOUND			(uint16_t)1
#define HOLD_FILT (uint16_t)2
#define L_CM_MAP	(uint16_t)1
#define R_CM_MAP	(uint16_t)2
#define DAC_MAP		(uint16_t)0
#define NUM				20

/**************************************************************************************************************
* Global variables
***************************************************************************************************************/
CanTxMsg TxMessage;
uint8_t i = 0;

uint16_t CAN1_ID;
uint8_t CAN1_DATA0;
uint8_t CAN1_DATA1;
uint8_t CAN1_DATA2;
uint8_t CAN1_DATA3;
uint8_t CAN1_DATA4;
uint8_t CAN1_DATA5;
uint8_t CAN1_DATA6;
uint8_t CAN1_DATA7;

void clear_rx_packet(void);

/**************************************************************************************************************
* Main thread
***************************************************************************************************************/
int main(void)
{
  RCC_ClocksTypeDef SYS_Clocks;
	// init will setup all peripherals + a 2s watchdog timer
	// init function mentions if watchdog was source of reset
	if(init(QUIET, NO_IDWG))
	{
		printf("\n\nRecovery Successful\r\n\n");
	}
	LED_Setup();
	LED(OFF);
	initButton();
	USART3_putc('\f');
	RCC_GetClocksFreq(&SYS_Clocks);
	printf("\r\nSYSCLK:%dM\r\n",SYS_Clocks.SYSCLK_Frequency/1000000);
	printf("HCLK:%dM\r\n",SYS_Clocks.HCLK_Frequency/1000000);
	printf("PCLK1:%dM\r\n",SYS_Clocks.PCLK1_Frequency/1000000);
	printf("PCLK2:%dM\r\n",SYS_Clocks.PCLK2_Frequency/1000000);	
	
	printf("CAN1_init().. ");
	if(CAN1_Config() == 0)
	{
		printf("Ack failed\r\n");
		while(1);
	}
	printf("Done\r\n");
	//printf("CAN BTR = 0x%x\r\n", CAN1->BTR);

  TxMessage.StdId = 0x322;
  TxMessage.ExtId = 0x01;
  TxMessage.RTR = CAN_RTR_DATA;
  TxMessage.IDE = CAN_ID_STD;
  TxMessage.DLC = 8;
	TxMessage.Data[0] = 0;
	TxMessage.Data[1] = 1;
	TxMessage.Data[2] = 2;
	TxMessage.Data[3] = 3;
	TxMessage.Data[4] = 4;
	TxMessage.Data[5] = 5;
	TxMessage.Data[6] = 6;
	TxMessage.Data[7] = 7;

	while(1)
	{
		LED(OFF);
		//printf("\r^\t ");
		CanRxMsg RxMessage;
		
		if(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13) == RESET)
		{
			if(i == 255)
			{
				i = 0;
			}
			i++;
			TxMessage.Data[0] = i;
			LED(ON);
			CAN_Transmit(CAN1, &TxMessage);
			printf("Tx\r\n");
			delay(100);
		}
		if(CAN_GetFlagStatus(CAN1, CAN_FLAG_FMP0) == ENABLE)
		{
			CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
			CAN1_ID = RxMessage.StdId;
			CAN1_DATA0 = RxMessage.Data[0];
			CAN1_DATA1 = RxMessage.Data[1];
			CAN1_DATA2 = RxMessage.Data[2];
			CAN1_DATA3 = RxMessage.Data[3];
			CAN1_DATA4 = RxMessage.Data[4];
			CAN1_DATA5 = RxMessage.Data[5];
			CAN1_DATA6 = RxMessage.Data[6];
			CAN1_DATA7 = RxMessage.Data[7];
			
			printf("\f");
			printf("Rx\r\n");
			printf("0x%x\t", CAN1_ID);
			printf("%u\t", CAN1_DATA0);
			printf("%u\t", CAN1_DATA1);
			printf("%u\t", CAN1_DATA2);
			printf("%u\t", CAN1_DATA3);
			printf("%u\t", CAN1_DATA4);
			printf("%u\t", CAN1_DATA5);
			printf("%u\t", CAN1_DATA6);
			printf("%u\r\n", CAN1_DATA7);
		}
		
		delay(50);
		//IWDG_ReloadCounter();
	}
}

void clear_rx_packet(void)
{
	CAN1_ID = 0;
	CAN1_DATA0 = 0;
	CAN1_DATA1 = 0;
	CAN1_DATA2 = 0;
	CAN1_DATA3 = 0;
	CAN1_DATA4 = 0;
	CAN1_DATA5 = 0;
	CAN1_DATA6 = 0;
	CAN1_DATA7 = 0;
}
