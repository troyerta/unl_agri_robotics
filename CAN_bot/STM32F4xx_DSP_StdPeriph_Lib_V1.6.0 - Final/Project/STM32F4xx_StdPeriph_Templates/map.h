#include "stdint.h"
#include "main.h"
#include "stdint.h"
#include "string.h"
#include "math.h"

// raw or cm
uint16_t boundToRange(uint16_t type, uint16_t input, uint16_t range_min, uint16_t range_max);
// right, left, and voltage
uint16_t lookUpTable(uint16_t map, uint16_t input);
