#include "lidar.h"
#include "i2c.h"
#include "utils.h"

#define SLAVE_ADDRESS 0x62

void reset_lidar(I2C_TypeDef* I2Cx)
{
	#ifdef U_DEBUG
	printf("\tResetting Lidar device... ");
	#endif
	lidar_write(I2Cx, 0x00, 0x00);
	#ifdef U_DEBUG
	printf("Done.\n\r");
	#endif
}

void init_lidar(I2C_TypeDef* I2Cx)
{
	#ifdef U_DEBUG
	if(I2Cx == I2C1)
	{
		printf("\tLIDAR init: Reset LEFT Lidar device\n\r");
	}
	else if(I2Cx == I2C3)
	{
		printf("\tLIDAR init: Reset RIGHT Lidar device\n\r");
	}
	#endif
	delay(25);
	lidar_write(I2Cx, 0x00, 0x00);
	#ifdef U_DEBUG
	printf("\tSetting Ts: Normal\n\r");
	#endif
	delay(25);
	// Reset and init normally
	lidar_write(I2Cx, 0x00, 0x00);
	delay(25);
	// Low noise, low sensitivity mode
	lidar_write(I2Cx, 0x1C, 0x20);
	delay(25);
	lidar_write(I2Cx, 0x1C, 0x20);
	delay(25);
	// 0x02 min, try 0x13 for 100 Hz
	lidar_write(I2Cx, 0x45, 0x02);
	#ifdef U_DEBUG
	printf("\tSetting Mode pin: free running PWM out\n\r");
	#endif
	delay(25);
	// Enable Mode pin low
	lidar_write(I2Cx, 0x04, 0x21);
	#ifdef U_DEBUG
	printf("\tNum samples: infinite\n\r");
	#endif
	delay(25);
	// Infinite number of readings
	lidar_write(I2Cx, 0x11, 0xFF);
	#ifdef U_DEBUG
	printf("\tInit aquisitions\n\r");
	#endif
	delay(25);
	// Initiate reading distance
	lidar_write(I2Cx, 0x00, 0x04);
	delay(25);
	#ifdef U_DEBUG
	if(I2Cx == I2C1)
	{
		printf("\tLEFT LIDAR ready..\n\r");
	}
	else if(I2Cx == I2C3)
	{
		printf("\tRIGHT LIDAR ready..\n\r");
	}
	#endif
}

void lidar_write(I2C_TypeDef* I2Cx, uint8_t reg_addr, uint8_t value)
{
		// Address the device
		I2C_start(I2Cx, SLAVE_ADDRESS << 1, I2C_Direction_Transmitter);
		// Address the register to modify
		I2C_write(I2Cx, reg_addr);
		// Send new register value
		I2C_write(I2Cx, value);
		I2C_stop(I2Cx);
	
		#ifdef U_DEBUG
		if(I2C_GetFlagStatus(I2Cx, I2C_FLAG_AF) == SET)
		{
			if(I2Cx == I2C1)
			{
				printf("nack left..\n\r");
			}
			else
			{
				printf("nack right..\n\r");
			}
		}
		#endif
}

uint16_t lidar_get_distance(I2C_TypeDef* I2Cx)
{
		uint8_t data[2];
		uint16_t distance = 0;
		
		// Address the left lidar
		I2C_start(I2Cx, SLAVE_ADDRESS << 1, I2C_Direction_Transmitter);
		I2C_write(I2Cx, 0x8f);
		// Stop the transmission
		I2C_stop(I2Cx);
		
		// Make sure the thing responded with an ack
		if(I2C_GetFlagStatus(I2Cx, I2C_FLAG_AF) == SET)
		{
			#ifdef U_DEBUG
			if(I2Cx == I2C1)
			{
				printf("nack read left..\n\r");
			}
			else
			{
				printf("nack read right..\n\r");
			}
			#endif
		}
		// Pull in the incoming bytes
		else
		{
			I2C_start(I2Cx, SLAVE_ADDRESS << 1, I2C_Direction_Receiver);
			// Address the register to read from
			data[0] = I2C_read_ack(I2Cx);
			data[1] = I2C_read_nack(I2Cx);
			distance = (data[0] << 8) + data[1];
		}
		return distance;
}
