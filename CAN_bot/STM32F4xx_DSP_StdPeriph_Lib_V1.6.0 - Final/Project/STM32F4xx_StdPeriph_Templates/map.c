#include "utils.h"
#include "stdint.h"
#include "map.h"

uint16_t left_cm_map[40] = 
{
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
};

uint16_t right_cm_map[40] = 
{
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
};

// linearly interpolate between these points to gain a large resolution need 8 bits minimum 255/40 = 6.375, 
uint16_t dac_map[40] = 
{
         6,     13,     19,     26,     32,     38,     45,     51,     57,     64,     70,     77,     83,     89,     96,    102, 
       108,    115,    121,    128,    134,    140,    147,    153,    159,    166,    172,    179,    185,    191,    198,    204, 
       210,    217,    223,    230,    236,    242,    249,    255
};

// raw or cm
uint16_t boundToRange(uint16_t type, uint16_t input, uint16_t range_min, uint16_t range_max)
{
	static uint16_t memory = 40;
	uint16_t output = 0;
	
	// RAW readings from lidar sent here
	if(type == 1)
	{
		if(input <= range_min)
		{
			output = range_min;
		}
		else if(input > range_max)
		{
			output = range_max;
		}
		else
		{
			output = input;
		}
		
		return output;
	}
	// RAW readings
	// replace out of "range_max" cm readings with last output
	else if(type == 2)
	{
		// Do zero order hold filtering
		if(input <= range_min)
		{
			output = range_min;
		}
		else if(input >= range_max)
		{
			//output = range_max;
			output = memory;
		}
		else
		{
			output = memory = input;
		}
		
		return output;
	}
	else
	{
		return 0;
	}
}
// right, left, and voltage
uint16_t lookUpTable(uint16_t map, uint16_t input)
{
	if(map == 1) 	// Use left cm map
	{
		return left_cm_map[input];
	}
	else if(map == 2)  // Use right cm map
	{
		return right_cm_map[input];
	}
	else if(map == 0)	 // Use DAC map, then return interpolated value back
	{
		return dac_map[input];
	}
	else	// something is wrong
	{
		return 0;
	}
}



