#include "stdint.h"
#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "math.h"

#define ON		(uint32_t)1
#define OFF		(uint32_t)0
#define HIGH 	(uint32_t)1
#define LOW		(uint32_t)0

void f2s(float input, char* str, uint8_t afterpoint);
void delay(__IO uint32_t nTime);
void TimingDelay_Decrement(void);

void LED_Setup(void);
void init_GPIOs(void);
void LED(uint32_t state);
void GPIO9(uint32_t state);
void GPIO6(uint32_t state);
void GPIO7(uint32_t state);
void init_input_pin(void);

void EXTILine3_Config(void);
void EXTILine4_Config(void);

void init_triggerPin(void);
void trigger_pin(uint32_t state);
void init_enablePin(void);
void enable_pin(uint32_t state);
void printDone(void);

void startSetup(void);
void endSetup(void);

void initButton(void);
void init_ack(void);

int16_t map(int16_t input, int16_t minIn, int16_t maxIn, int16_t minOut, int16_t maxOut);
