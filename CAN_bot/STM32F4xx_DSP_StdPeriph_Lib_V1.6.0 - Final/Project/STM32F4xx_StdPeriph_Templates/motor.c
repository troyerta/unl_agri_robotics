#include "utils.h"
#include "stdio.h"
#include "usart.h"
#include "motor.h"

void init_motor(void)
{
	// Setup pins and USART here
		USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	// Configure USART1 //
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);
	USART_Cmd(USART1, ENABLE);

	// USART GPIO Configure for PA9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Connect USART pin to AF //
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);

}

void motor(uint8_t motor, int8_t power)
{
	int command, magnitude;
	// constrain and map values
	magnitude = abs(power) >> 1;
	
	if(motor == 1)
	{
		command = power < 0 ? 63 - magnitude : 64 + magnitude;
	}
	else if(motor == 2)
	{
		command = power < 0 ? 191 - magnitude : 192 + magnitude;
	}
	else
	{
		printf("invalid motor write...\r\n");
		while(1);
	}
	
	// write final value to serial port
	// add 127 to values meant for motor 2
	while((USART1->SR & USART_FLAG_TXE) == 0);
	USART1->DR = command;
}

int8_t absolute_value(int8_t input)
{
	uint8_t signMask = 0x80;
	uint8_t valueMask = 0x7F;
	
	if(input & signMask)
	{
		return(input & valueMask);
	}
	else
	{
		return(input);
	}
}
