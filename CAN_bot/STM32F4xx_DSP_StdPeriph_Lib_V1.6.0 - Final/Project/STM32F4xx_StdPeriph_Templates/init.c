#include "main.h"
#include "usart.h"
#include "pwmInput.h"
#include "tim2.h"
#include "i2c.h"
#include "utils.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "lidar.h"
#include "dac.h"
#include "watchdog.h"
#include "motor.h"

uint8_t init(uint8_t verbose, uint8_t watchdog)
{
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	uint8_t recovery = 0;
		
	// Capture error
	if(SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000))
	{
		while(1);
	}
		
	//USART1_Configuration();
	//USART2_Configuration();
	USART3_Configuration();
	//UART4_Configuration();
	//UART5_Configuration();
	//USART6_Configuration();
	printf("\r\n\r\n");
	delay(1);
	printf("Init...\r\n");
	delay(10);
		
	if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET)
	{
		// Light an LED or something
		printf("\n\nIWDG RESET\r\n");
		RCC_ClearFlag();
		recovery = 1;
	}

	if(watchdog)
	{
		watchdogInit();
		IWDG_Enable();
	}
		// Now we must pet the watchdog within every 2s forever
		// failure to setup I2C bus will result in a reset until the bus works again.
		// use IWDG_ReloadCounter();
	
	if(verbose)
	{
		printf("booting...");
		delay(150);
		printf("\n\rcomms established\n\r");
		delay(150);
		startSetup();
		delay(150);
		printf("timer set\n\r");
		delay(150);
		printf("Pin config.. ");

		init_GPIOs();
		init_triggerPin();
		trigger_pin(HIGH);
		GPIO9(LOW);
		GPIO6(LOW);
		GPIO7(LOW);

		printDone();

		printf("I2C bus config..\n\r");

		init_I2C1();
		init_I2C3();

		printf("waiting for logic capture.. ");
		delay(200);
		printDone();


		printf("Lidar LEFT config..\n\r");
		GPIO9(HIGH);

		init_lidar(LEFT);

		printf("Lidar RIGHT config..\n\r");

		init_lidar(RIGHT);

		printf("DAC config..\n\r");

		dac_init();

		printf("DAC is ready\n\r");

		GPIO9(LOW);
		endSetup();

		printf(" ready.\r\n\n");
	}
	else
	{
//		init_GPIOs();
//		init_triggerPin();
//		trigger_pin(HIGH);
//		init_ack();	// Pull down present on this one
//		init_enablePin();
//		enable_pin(HIGH);
//		
//		GPIO9(LOW);
//		GPIO6(LOW);
//		GPIO7(LOW);

		//init_I2C1();
		//init_I2C3();

		//init_lidar(LEFT);
		//init_lidar(RIGHT);
		//dac_init();
		//init_motor();
		//printf("ready.\r\n\n");
	}
	return recovery;
}
