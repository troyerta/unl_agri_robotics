

uint16_t max(uint16_t *array, uint16_t n);
uint16_t min(uint16_t *array, uint16_t n);
float std_dev(uint16_t *array, uint16_t n, float mean);
float mean(uint16_t *array, uint16_t n);

