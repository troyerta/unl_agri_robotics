/**
  ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Templates/stm32f4xx_it.c 
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    04-September-2015
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include "main.h"
#include "usart.h"
#include "pwmInput.h"
#include "tim2.h"
#include "i2c.h"
#include "utils.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "lidar.h"

/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

//extern volatile uint16_t lidar_left_distance;
//extern volatile uint16_t lidar_right_distance;
//extern volatile uint32_t reading_left_done;
//extern volatile uint32_t reading_right_done;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  TimingDelay_Decrement();
	//IWDG_ReloadCounter();
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/
void TIM2_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
		//TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		
		//GPIO6(HIGH);

		//lidar_left_distance = lidar_get_distance(LEFT);

		//reading_left_done = 1;
		//GPIO6(LOW);
	}
}

void EXTI0_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line3) != RESET)
	{
		//GPIO6(HIGH);
		// Service the interrupt
		//NVIC_DisableIRQ(EXTI0_IRQn);
		//lidar_left_distance = lidar_get_distance(LEFT);

		//reading_left_done = 1;
		//GPIO6(LOW);
		
		// Clear the EXTI line 3 pending bit
		//EXTI_ClearITPendingBit(EXTI_Line3);
	}
}

void EXTI1_IRQ_Handler(void)
{
	if(EXTI_GetITStatus(EXTI_Line4) != RESET)
	{
		//GPIO7(HIGH);
		
		// Service the interrupt
		//NVIC_DisableIRQ(EXTI1_IRQn);
		//lidar_right_distance = lidar_get_distance(RIGHT);
		
		//reading_right_done = 1;
		//GPIO7(LOW);
		
		// Clear the EXTI line 3 pending bit
		//EXTI_ClearITPendingBit(EXTI_Line4);
	}
}

void EXTI3_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line3) != RESET)
	{
		//GPIO6(HIGH);
		// Service the interrupt
		//NVIC_DisableIRQ(EXTI0_IRQn);
		//lidar_left_distance = lidar_get_distance(LEFT);

		//reading_left_done = 1;
		//GPIO6(LOW);
		
		// Clear the EXTI line 3 pending bit
		//EXTI_ClearITPendingBit(EXTI_Line3);
	}
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
