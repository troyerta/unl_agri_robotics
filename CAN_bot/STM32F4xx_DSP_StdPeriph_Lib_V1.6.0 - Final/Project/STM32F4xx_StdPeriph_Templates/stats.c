#include "utils.h"
#include "stdio.h"
#include "stdint.h"
#include "arm_math.h"

uint16_t max(uint16_t *array, uint16_t n)
{
	uint16_t i = 0;
	uint16_t max = 0;
	
	for(i = 0; i < n; i++)
	{
		if(array[i] > max)
		{
			max = array[i];
		}
	}
	return max;
}

uint16_t min(uint16_t *array, uint16_t n)
{
	uint16_t i = 0;
	uint16_t min = 65535;
	for(i = 0; i < n; i++)
	{
		if(array[i] < min)
		{
			min = array[i];
		}
	}
	return min;
}

float std_dev(uint16_t *array, uint16_t n, float mean)
{
	uint16_t i;
	float accum = 0.0;
	float dev_temp;
	
	for(i = 0; i < n; i++)
	{
		accum += ((float)array[i]-(float)mean)*((float)array[i]-(float)mean);
	}	
			
	dev_temp = sqrtf(accum / n);
	
	return dev_temp;
}
		
float mean(uint16_t *array, uint16_t n)
{
	uint32_t work;
	uint16_t i;
	float mean_temp;
	
	for(i = 0; i < n; i++)
	{
		work += array[i];
	}
	mean_temp = (float)work / (float)n;
	
	return mean_temp;
}
	
	
	
	

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
