/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f4xx.h"

// Replace the I2C type with linguisitc meaning
#define LEFT	I2C1
#define RIGHT I2C3

void TimingDelay_Decrement(void);

#endif /* __MAIN_H */
