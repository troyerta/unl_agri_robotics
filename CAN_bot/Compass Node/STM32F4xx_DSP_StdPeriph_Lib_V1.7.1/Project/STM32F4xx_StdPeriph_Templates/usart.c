/*
 * usart.c

 *
 *  Created on: Oct 12, 2015
 *      Author: ttroyer2
 */

/********************************************************************************************************************
 *
 * Includes
 *
 *******************************************************************************************************************/
#include "stm32f4xx.h"
#include "stdint.h"
#include "stdio.h"
//#include "prototypes.h"
#include "usart.h"

/********************************************************************************************************************
 *
 * Function Implementations
 *
 *******************************************************************************************************************/
void USART2_Configuration(void)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	// Configure USART //
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART2, &USART_InitStructure);
	USART_Cmd(USART2, ENABLE);

	// USART GPIO Configure for PA2 and PA3
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3; // PA.2 USART2_TX, PA.3 USART2_RX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Connect USART pins to AF //
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	printf("\r\n\r\n");
}

uint8_t USART2_getc(void)
{
	while(USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET);
    return(uint8_t)USART_ReceiveData(USART2);
}

void USART2_putc(char c)
{
	USART_SendData(USART2, c);
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
}

void USART3_Configuration(void)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	// Configure USART //
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART3, &USART_InitStructure);
	USART_Cmd(USART3, ENABLE);

	// USART GPIO Configure for PC10 and PC11
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11; // PC.10 USART3_TX, PC.11 USART3_RX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	// Connect USART pins to AF //
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_USART3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_USART3);

	printf("\r\n\r\n");
}

char USART3_getc(void)
{
	//while(USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == RESET);
   // return (uint8_t)USART_ReceiveData(USART3);
	while((USART3->SR & USART_FLAG_RXNE) == 0);
	return USART3->DR;
}

void USART3_putc(char c)
{
	//USART_SendData(USART3, c);
	//while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);
	while((USART3->SR & USART_FLAG_TXE) == 0);
	USART3->DR = c;
	return;
}

void USART6_Configuration(void)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);

	// Configure USART //
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART6, &USART_InitStructure);
	USART_Cmd(USART6, ENABLE);

	// USART GPIO Configure for PG14 and PG9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_9; // PG.14 USART6_TX, PG.9 USART6_RX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOG, &GPIO_InitStructure);

	// Connect USART pins to AF //
	GPIO_PinAFConfig(GPIOG, GPIO_PinSource14, GPIO_AF_USART6);
	GPIO_PinAFConfig(GPIOG, GPIO_PinSource9, GPIO_AF_USART6);

	printf("\r\n\r\n");
}

uint8_t USART6_getcTimeout(uint8_t* cp)
{
	//while(USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == RESET);
   // return (uint8_t)USART_ReceiveData(USART3);
  volatile uint8_t error = 0;
  volatile uint32_t timeout = 8000000;
	while(((USART3->SR & USART_FLAG_RXNE) == 0) && (--timeout > 0));
  
  *cp = USART6->DR;
  
  if(timeout == 0)
  {
    error = 1;
  }
  else
  {
    error = 0;
  }
  
	return error;
}

void USART6_putc(char c)
{
	//USART_SendData(USART3, c);
	//while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);
	while((USART6->SR & USART_FLAG_TXE) == 0);
	USART6->DR = c;
	return;
}
