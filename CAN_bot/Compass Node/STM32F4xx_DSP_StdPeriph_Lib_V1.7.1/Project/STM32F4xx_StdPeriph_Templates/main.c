#include "main.h"
#include "stdio.h"
#include "usart.h"
#include "utils.h"
#include "gps.h"
#include "v2x.h"
#include "tim2.h"
#include "can.h"
#include "arm_math.h"
#include "core_cm4.h"
#include "kalman.h"

CanTxMsg my;
  
uint16_t heading = 0;
int16_t headingEst = 0;
float32_t estimate = 0.0f;

// Parse GPS messages that come in - 
int main(void)
{
  RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
  LED_Setup();
  	// Capture error
	if(SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000))
	{
		while(1);
	}
  
  USART3_Configuration();
  v2x_init();
  TIM2_Config();
  
  LED(ON);
  delay(1500);
  LED(OFF);

  printf("\r\n\r\f");
	
	printf("Init...\r\n");
  
  printf("%s Tyler Troyer - CAN Node 0V2 - Target: STMicroelectronics STM32F446RET 180MHz \r\n%s", Green, none);
	printf("\n%sGetting clocks..\r\n", none);
	printf("\r\nSYSCLK:%dM\r\n", RCC_Clocks.SYSCLK_Frequency/1000000);
	printf("HCLK:%dM\r\n", RCC_Clocks.HCLK_Frequency/1000000);
	printf("PCLK1:%dM\r\n", RCC_Clocks.PCLK1_Frequency/1000000);
	printf("PCLK2:%dM\r\n", RCC_Clocks.PCLK2_Frequency/1000000);
  
  //int64_t tester = 10;
  /* Transmit Structure preparation */
  my.StdId = 0x321;
  my.ExtId = 0x01;
  my.RTR = CAN_RTR_DATA;
  my.IDE = CAN_ID_STD;
  my.DLC = 2;
  
  // Mandatory delay after startup for the compass
  delay(500);
  
  while(1)
  {
    // This timer overflows every 200mS for a run-rate of 5Hz
    while(TIM_GetFlagStatus(TIM2, TIM_FLAG_Update) == RESET);
    TIM_ClearFlag(TIM2, TIM_IT_Update);
    LED(ON);
    
    heading  = v2x_read();
    
    if(heading > 180)
    {
      headingEst = (int16_t)((int16_t)heading - 360);
    }
    else
    {
      headingEst = heading;
    }
    
    // Convert the result into fixed-point precision, (some kind of bit shifting operation that preserves the sign)
    // This is the precision that the controller node will work with
    
    // Do Kalman filter, then shift the value back again to normal range, From -180 - +180, to 0 - 359)
    estimate = Kalmanf((float32_t)headingEst);
    
    //if(estimate < 0.0f)
    //{
    //  estimate += 360.0f;
    //}
    
    //TxMessage.Data[0] = (uint8_t)(heading >> 8);
    //TxMessage.Data[1] = (uint8_t)(heading & 0xFF);
    //can1_txBuf_load(&TxMessage);
    
    //printf("%d,%3.2f\r\n", headingEst, estimate);
    printf("%3.2f\r\n", estimate);
    
    // Test print a 64 bit int..
    //printf("%lld\r\n", tester);
    //tester++;
    //tester *= -1;
    LED(OFF);
  }
}
