#include "main.h"
#include "utils.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "math.h"

static __IO uint32_t uwTimingDelay;
/*
void f2s(float input, char* str, uint8_t afterpoint)
{

	int d1 = input;            // Get the integer part (678).
	float f2 = input - d1;     // Get fractional part (678.0123 - 678 = 0.0123).
	int d2 = trunc(f2 * 10000);   // Turn into integer (123).
	//int d2 = (int)(f2 * 10000); // Or this one: Turn into integer.

	// Print as parts, note that you need 0-padding for fractional bit.
	// Since d1 is 678 and d2 is 123, you get "678.0123".
	sprintf (str, "%d.%04d\n", d1, d2);

}
*/
void delay(__IO uint32_t nTime)
{ 
  uwTimingDelay = nTime;

  while(uwTimingDelay != 0);
}

void TimingDelay_Decrement(void)
{
  if (uwTimingDelay != 0x00)
  { 
    uwTimingDelay--;
  }
}

// Nucleo LED is DISCONNECTED AT THIS TIME PB0 is LEd1 (Red)
void LED_Setup(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable the GPIOA peripheral */ 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  
  /* Configure MCO1 pin(PA8) in alternate function */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;  
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

void LED(uint32_t state)
{
	if(state)
	{
		GPIO_SetBits(GPIOA, GPIO_Pin_5);
	}
	else
	{
		GPIO_ResetBits(GPIOA, GPIO_Pin_5);
	}
}

void init_GPIOs(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	/* Enable the GPIOA peripheral */ 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	/* Enable the GPIOB peripheral */ 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* Configure PA9 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO9(HIGH);
	
	/* Configure PA2 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO6(HIGH);
	
	/* Configure PA3 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO7(HIGH);
}

void initButton(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	// Enable the GPIOC peripheral 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	// Configure PC_13 in input mode
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void GPIO9(uint32_t state)
{
	if(state)
	{
		GPIO_SetBits(GPIOA, GPIO_Pin_9);
	}
	else
	{
		GPIO_ResetBits(GPIOA, GPIO_Pin_9);
	}
}

void GPIO6(uint32_t state)
{
	if(state)
	{
		GPIO_SetBits(GPIOA, GPIO_Pin_6);
	}
	else
	{
		GPIO_ResetBits(GPIOA, GPIO_Pin_6);
	}
}

void GPIO7(uint32_t state)
{
	if(state)
	{
		GPIO_SetBits(GPIOA, GPIO_Pin_7);
	}
	else
	{
		GPIO_ResetBits(GPIOA, GPIO_Pin_7);
	}
}

// Use A10 as a mode pin input
void init_input_pin(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	// Configure PA10 in input mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}


// Setup PC0 as an interrupt input for Lidar1
void EXTILine3_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	// Enable GPIOB clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	// Enable SYSCFG clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
 
	// Configure PB3 pin as input floating
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
 
	// Connect EXTI Line3 to PB3 pin
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource3);
 
	// Configure EXTI Line3
	EXTI_InitStructure.EXTI_Line = EXTI_Line3;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
 
	// Enable and set EXTI Line3 Interrupt to the lowest priority
	NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

// Setup PC1 as interrupt pin for Lidar2
void EXTILine4_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	// Enable GPIOB clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	// Enable SYSCFG clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
 
	// Configure PB4 pin as input floating
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
 
	// Connect EXTI Line4 to PB4 pin
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource4);
 
	// Configure EXTI Line4
	EXTI_InitStructure.EXTI_Line = EXTI_Line4;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
 
	// Enable and set EXTI Line4 Interrupt to the lowest priority
	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

// Setup PB6 as output for mode trigger pin
void init_triggerPin(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	// Enable the GPIOB peripheral 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	// Configure PA9 in output pushpull mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void trigger_pin(uint32_t state)
{
	if(state)
	{
		GPIO_SetBits(GPIOB, GPIO_Pin_6);
	}
	else
	{
		GPIO_ResetBits(GPIOB, GPIO_Pin_6);
	}
}

void printDone(void)
{
	delay(500);
	printf("done\n\r");
}

void startSetup(void)
{
	uint8_t i;
	
	printf("\n\rStarting setup---->\n\r");
	
	for(i = 0; i < 50; i++)
	{
		printf("-");
	}
	printf("\n\r");
}

void endSetup(void)
{
	uint8_t i;
	
	for(i = 0; i < 40; i++)
	{
		printf("-");
	}
	
	printf("\n\r---->setup complete..\n\n\r");
}



int16_t map(int16_t input, int16_t minIn, int16_t maxIn, int16_t minOut, int16_t maxOut)
{
	int16_t inputRange = maxIn - minIn;
	int16_t outputRange  = maxOut - minOut;
	
	int16_t mappedOutput = (((input - minIn)*inputRange) / outputRange) + minOut;
	
	//uint16_t mappedOutput = (input - minIn)*(maxOut - minOut + 1) / (maxIn - minIn + 1) + minOut;
	//uint16_t mappedOutput = (input - minIn)*(maxOut - minOut) / (maxIn - minIn) + minOut;
	
	return (int16_t)mappedOutput;
}
