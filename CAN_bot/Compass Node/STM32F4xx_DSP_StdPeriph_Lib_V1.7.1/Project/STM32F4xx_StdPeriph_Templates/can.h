

uint8_t CAN1_Config(uint32_t filterID);
void print_CAN_state(CAN_TypeDef *CANx);
void can_tx(CAN_TypeDef *CANx, CanTxMsg* TxMessage);
void can1_txBuf_load(CanTxMsg *TxMessage);
uint8_t numTxElements(void);
