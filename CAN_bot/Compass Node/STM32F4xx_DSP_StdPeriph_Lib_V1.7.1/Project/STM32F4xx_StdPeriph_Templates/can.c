#include "utils.h"
#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "usart.h"
#include "can.h"
#include "string.h"

#define bufSize 32

CanTxMsg txBuf[bufSize];
static uint8_t txHead = 0;
static uint8_t txTail = 0;

CAN_InitTypeDef        CAN_InitStructure;
CAN_FilterInitTypeDef  CAN_FilterInitStructure;
//CanTxMsg TxMessage;

uint8_t CAN1_Config(uint32_t filterID)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
	uint8_t status = 0;
  
  /* CAN GPIOs configuration **************************************************/

  /* Enable GPIO clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOB, ENABLE);

  /* Connect CAN pins */
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1); // RX or use PA11
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1); // TX or use PA12
  
  /* Configure CAN RX and TX pins */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* CAN configuration ********************************************************/  
  /* Enable CAN clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
  
  /* CAN register init */
  CAN_DeInit(CAN1);
  CAN_StructInit(&CAN_InitStructure);

  /* CAN cell init */
  CAN_InitStructure.CAN_TTCM = DISABLE;         // Time-triggered communication mode, (time stamps added to last two bytes of data)
  CAN_InitStructure.CAN_ABOM = DISABLE;         // Automatic Bus-off management
  CAN_InitStructure.CAN_AWUM = DISABLE;         // Automatic wakeup mode
  CAN_InitStructure.CAN_NART = DISABLE;         // Non-automatic retransimssion mode
  CAN_InitStructure.CAN_RFLM = DISABLE;         // Feature to optionally lock the FIFO contents from being overwritten by new msgs if FIFO is full
  CAN_InitStructure.CAN_TXFP = DISABLE;         // Tx mailbox contents by priority, not FIFO
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal; // Apart from loopback and silent modes
  CAN_InitStructure.CAN_SJW = CAN_SJW_2tq;      // How many time quanta to adjust by for synchronization? Max 2 - why? Because.

  /*
   * How to calculate this shit:
   * BF = (APB1_Clk/Desired_Baudrate);
   * # of Quanta per bit = TQ = BF/Prescalar;
   * Given that TQ = (1 + BS1 + BS2),
   * Choose BS1 and BS2 such that 
   * BS1/(BS1+BS2) ~ 72%
   * for
   * BS1 = 1,....,16
   * BS2 = 1,....,8
   * 
   * So get your APB1_clk rate from the system
   * and calculate BF using your desired baudrate.
   * Then pick a prescalar such that 8 < TQ < 25
   * Finally, set BS1 ~= 0.72 * TQ
   * and BS2 = (TQ - 1) - BS1
   * You're done! Baudrate is set, sampling point is near optimal
   */
  CAN_InitStructure.CAN_BS1 = CAN_BS1_10tq;      // 10tq / (15tq - 1tq) = 71.4% 
  CAN_InitStructure.CAN_BS2 = CAN_BS2_4tq;       // 1tq + 10tq + 4tq = 15tq
  CAN_InitStructure.CAN_Prescaler = 6;           // (45MHz / 500,000Baud) / 6 = 15tq per bit
  CAN_Init(CAN2, &CAN_InitStructure);
    
//  /* CAN Baudrate = 250kBps (CAN clocked at 45 MHz) */
//  CAN_InitStructure.CAN_BS1 = CAN_BS1_10tq;
//  CAN_InitStructure.CAN_BS2 = CAN_BS2_4tq;
//  CAN_InitStructure.CAN_Prescaler = 6;
//  status = CAN_Init(CAN1, &CAN_InitStructure);

  /* CAN filter init */
  CAN_FilterInitStructure.CAN_FilterNumber = 0;
	
	if(filterID != (uint16_t)0)
	{
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = (filterID) << 5;
	}
	else
	{
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	}
	
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);
  
  // Enable the TX complete interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = CAN1_TX_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  /* Transmit Structure preparation */
//  TxMessage.StdId = 0x321;
//  TxMessage.ExtId = 0x01;
//  TxMessage.RTR = CAN_RTR_DATA;
//  TxMessage.IDE = CAN_ID_STD;
//  TxMessage.DLC = 1;
  
  /* Enable FIFO 0 message pending Interrupt */
  //CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);
	
	return status;
}

void CAN1_TX_IRQHandler(void)
{
  if(CAN_GetITStatus(CAN1, CAN_IT_TME) != RESET)
  {
    // Transmission was successful
    CAN_ClearITPendingBit(CAN1, CAN_IT_TME);
    CAN_ITConfig(CAN1, CAN_IT_TME, DISABLE);
    
    //if(txTail == txHead)
    // If buffer is empty, turn off interrupts
    if(numTxElements() == 0)
    {
      CAN_ITConfig(CAN1, CAN_IT_TME, DISABLE);
    }
    // Something(s) in the the buffer at the tail position
    // Place it in the tx mailbox and request a tx over CAN
    else
    {
      CAN_Transmit(CAN1, &txBuf[txTail]);
      txTail++;
      
      if(txTail == bufSize)
      {
        txTail = 0;
      }
      
      if(numTxElements())
      {
        CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);
      }
    }
  }
}

void can1_txBuf_load(CanTxMsg *TxMessage)
{
  // Write something to the buffer, enable the irq, then exit
  txBuf[txHead] = *TxMessage;
  txHead++;
  
  if(txHead == bufSize)
  {
    txHead = 0;
  }
  CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);
}

uint8_t numTxElements(void)
{
  uint8_t value;
  uint8_t head;
  uint8_t tail;
  
  if(CAN1->IER & CAN_IER_TMEIE)
  {
    CAN_ITConfig(CAN1, CAN_IT_TME, DISABLE);
    head = txHead;
    tail = txTail;
    CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);
  }
  else
  {
    head = txHead;
    tail = txTail;
  }
  
  if(head >= tail)
  {
    value = head - tail;
  }
  else
  {
    value = bufSize + head - tail;
  }
  
  return value;
}

//void print_CAN_state(CAN_TypeDef *CANx)
//{
//	const uint8_t size = 30;
//	char dbgMsg[size];
//	
//	snprintf(dbgMsg, size, "\n%sCAN Module State:\r\n\n", Cyan);
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tESR \t\t\t\t= 0x%x\r\n", CANx->ESR);
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tFlags \t\t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_LEC));
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tError Warning \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_EWG));
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tError Passive \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_EPV));
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tBus-off \t\t\t= %u\r\n", CAN_GetFlagStatus(CANx, CAN_FLAG_BOF));
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tLast Error Code \t\t= %u\r\n", CAN_GetLastErrorCode(CANx));
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tReceive Error Counter \t\t= %u\r\n", CAN_GetReceiveErrorCounter(CANx));
//	USART3_puts(dbgMsg);
//	snprintf(dbgMsg, size, "\tTransmit Error Counter \t\t= %u\r\n%s", CAN_GetLSBTransmitErrorCounter(CANx), none);
//	USART3_puts(dbgMsg);
//	
//	uint8_t error = CAN_GetLastErrorCode(CAN1);
//	
//	if(error & CAN_ErrorCode_NoErr)
//	{
//		snprintf(dbgMsg, size, "\nCAN_ErrorCode_NoErr\r\n");
//		USART3_puts(dbgMsg);
//	}
//	else
//	{
//		if(error & CAN_ErrorCode_StuffErr)
//		{
//			snprintf(dbgMsg, size, "\nCAN_ErrorCode_StuffErr\r\n");
//			USART3_puts(dbgMsg);
//		}
//		if(error & CAN_ErrorCode_FormErr)
//		{
//			snprintf(dbgMsg, size, "CAN_ErrorCode_FormErr\r\n");
//			USART3_puts(dbgMsg);
//		}
//		if(error & CAN_ErrorCode_ACKErr)
//		{
//			snprintf(dbgMsg, size, "CAN_ErrorCode_ACKErr\r\n");
//			USART3_puts(dbgMsg);
//		}
//		if(error & CAN_ErrorCode_BitRecessiveErr)
//		{
//			snprintf(dbgMsg, size, "CAN_ErrorCode_BitRecessiveErr\r\n");
//			USART3_puts(dbgMsg);
//		}
//		if(error & CAN_ErrorCode_BitDominantErr)
//		{
//			snprintf(dbgMsg, size, "CAN_ErrorCode_BitDominantErr\r\n");
//			USART3_puts(dbgMsg);
//		}
//		if(error & CAN_ErrorCode_CRCErr)
//		{
//			snprintf(dbgMsg, size, "CAN_ErrorCode_CRCErr\r\n");
//			USART3_puts(dbgMsg);
//		}
//		if(error & CAN_ErrorCode_SoftwareSetErr)
//		{
//			snprintf(dbgMsg, size, "CAN_ErrorCode_SoftwareSetErr\r\n");
//			USART3_puts(dbgMsg);
//		}
//	}
//}

void can_tx(CAN_TypeDef *CANx, CanTxMsg* TxMessage)
{
		CAN_Transmit(CANx, TxMessage);
	
		GPIO_SetBits(GPIOB, GPIO_Pin_0);
		//while(CAN_TransmitStatus(CANx, 0) != CAN_TxStatus_Ok);
		while(CAN_TransmitStatus(CANx, 0) == CAN_TxStatus_Pending);

		GPIO_ResetBits(GPIOB, GPIO_Pin_0);
//			if(CAN_TransmitStatus(CANx, 0) == CAN_TxStatus_Ok)
//			{
//				USART3_puts("\r\nTx OK\r\n");
//				while(1);
//			}
//			else if(CAN_TransmitStatus(CANx, 0) == CAN_TxStatus_Failed)
//			{
//				print_CAN_state(CANx);
//				USART3_puts("\r\nTx Failed\r\n");
//				while(1);
//			}
//			else if(CAN_TransmitStatus(CANx, 0) == CAN_TxStatus_Pending)
//			{
//				print_CAN_state(CAN1);
//				USART3_puts("\r\nTx Pending\r\n");
//				while(1);
//			}
//			else if(CAN_TransmitStatus(CANx, 0) == CAN_TxStatus_NoMailBox)
//			{
//				print_CAN_state(CANx);
//				USART3_puts("\r\nNo Tx Mailbox Available\r\n");
//				while(1);
//			}
//			else
//			{
//				print_CAN_state(CANx);
//				USART3_puts("\r\nProgram halted\r\n");
//				while(1);
//			}
}



