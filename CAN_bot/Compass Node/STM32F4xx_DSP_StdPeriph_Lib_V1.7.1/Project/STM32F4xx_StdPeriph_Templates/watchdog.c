#include "utils.h"
#include "stdio.h"
#include "watchdog.h"



void watchdogInit(void)
{
		IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
		
		// for a max of 4s timeout, write 0x07 for 8s and longer timeouts, 1024 ticking or 128 Hz ticking respectively
		IWDG_SetPrescaler(IWDG_Prescaler_32);
		/* Use the following number for the timeouts:
		5ms - 5
		10ms -10
		15ms - 15
		30ms - 31
		60ms - 61
		120ms - 123
		250ms - 255
		500ms - 511
		1s - 1023
		2s - 2047
		4s - 4095
		Use 0x07 prescaler for the following settings
		8s - 1023
		16s - 2047
		32s - 4095
		*/
		IWDG_SetReload(2047);
		IWDG_ReloadCounter();
}
