#include "main.h"
#include "stdint.h"
#include "v2x.h"
#include "utils.h"

// Private function prototypes
static uint16_t v2x_bb_read(void);
static void clock(void);
static void v2x_pin_init(void);
static void Clk_Pin_LOW(void);
static void Clk_Pin_HIGH(void);
static void PC_Pin_LOW(void);
static void PC_Pin_HIGH(void);
static void Cal_Pin_LOW(void);
static void Cal_Pin_HIGH(void);
static void Rst_Pin_LOW(void);
static void Rst_Pin_HIGH(void);
static void SS_Pin_LOW(void);
static void SS_Pin_HIGH(void);

void v2x_init(void)
{
  delay(20);
  
  // Set compass config with pins states
  v2x_pin_init();
  
  delay(20);
  
  // Restart the compass with the desired settings
  v2x_reset();
}

void v2x_reset(void)
{
  Rst_Pin_LOW();
  delay(15);
  Rst_Pin_HIGH();
}

void v2x_calibrate(uint8_t SRT_STP)
{
  Cal_Pin_LOW();
  delay(11);
  Cal_Pin_HIGH();
  // Turn on LED to indicate Calibration should be done
  // Wait for button press to leave this function and finish calibration
  LED(ON);
  while(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13));
  Cal_Pin_LOW();
  delay(11);
  Cal_Pin_HIGH();
  delay(100);
}

void clear_abort_cal(void)
{
  Cal_Pin_LOW();
  delay(11);
  Cal_Pin_HIGH();
}

uint16_t v2x_read(void)
{
  uint16_t data;
  
  // Signal the compass to start a measurement
  PC_Pin_LOW();
  delay(10);
  PC_Pin_HIGH();
  
  // Wait for EOC pin to go HIGH, then wait 11mS
  while(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_9));
  delay(10);
  
  SS_Pin_LOW();
  delay(5);
  data = v2x_bb_read();
  SS_Pin_HIGH();
  
  return data;
}

uint16_t v2x_bb_read(void)
{
  int16_t n, bit;
  uint16_t reading = 0;

  for(n=15; n>=0; n--)
  {
    clock();
    
    bit = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_12);
    //delay(1);
    //bit = (((GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_12)^0x80) >> 6) & 0x01);
    reading |= (bit << n);
  }

  return reading;
}

void clock(void)
{
  //delay(1);
  GPIOA->MODER |= GPIO_MODER_MODER11_0;   // Switch to output mode to go LOW
  delay(1);
  GPIOA->MODER &= ~(GPIO_MODER_MODER11);  // Move back to input mode for HIGH
  delay(1);
}

/*
 * Sets up P/C, Cal, CI, EOC, and RESET Pins on the compass
 * CLK   - PA11
 * DO    - PA12
 * P/C   - PB3
 * CAL   - PB4
 * CI    - PA8
 * EOC   - PA9
 * RESET - PB5
 * SS    - PB1
*/
void v2x_pin_init(void)
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  
  // PA11 as spi clk
  GPIOA->MODER &= ~(GPIO_MODER_MODER11);      // Clear mode bits to set pin as input
  GPIOA->OTYPER |= GPIO_OTYPER_OT_11;         // Set output type to open drain
  GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR11;  // Set speed to high
  GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR11);      // Disable pull up and pull down resistors
  GPIOA->BSRRH |= GPIO_BSRR_BR_11;            // Drive the pin low
  // Pin should now be HIGH
  
  // PA12 as data line, input only
  GPIOA->MODER &= ~(GPIO_MODER_MODER12);      // Clear mode bits to set pin as input
  GPIOA->OTYPER |= GPIO_OTYPER_OT_12;         // Set output type to open drain
  GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12;  // Set speed to high
  GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR12);      // Disable pull up and pull down resistors
  GPIOA->BSRRH |= GPIO_BSRR_BR_12;            // Drive the pin low
  // Pin should now be HIGH
  
  // PA8 as CI
  GPIOA->MODER &= ~(GPIO_MODER_MODER8);      // Clear mode bits to set pin as input
  GPIOA->OTYPER |= GPIO_OTYPER_OT_8;         // Set output type to open drain
  GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8;  // Set speed to high
  GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR8);      // Disable pull up and pull down resistors
  GPIOA->BSRRH |= GPIO_BSRR_BR_8;            // Drive the pin low
  // Pin should now be HIGH
  
  // PA9 as EOC
  GPIOA->MODER &= ~(GPIO_MODER_MODER9);      // Clear mode bits to set pin as input
  GPIOA->OTYPER |= GPIO_OTYPER_OT_9;         // Set output type to open drain
  GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9;  // Set speed to high
  GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR9);      // Disable pull up and pull down resistors
  GPIOA->BSRRH |= GPIO_BSRR_BR_9;            // Drive the pin low
  // Pin should now be HIGH
  
  // PB3 as PC
  GPIOB->MODER &= ~(GPIO_MODER_MODER3);      // Clear mode bits to set pin as input
  GPIOB->OTYPER |= GPIO_OTYPER_OT_3;         // Set output type to open drain
  GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR3;  // Set speed to high
  GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR3);      // Disable pull up and pull down resistors
  GPIOB->BSRRH |= GPIO_BSRR_BR_3;            // Drive the pin low
  // Pin should now be HIGH
  
  // PB4 as CAL
  GPIOB->MODER &= ~(GPIO_MODER_MODER4);      // Clear mode bits to set pin as input
  GPIOB->OTYPER |= GPIO_OTYPER_OT_4;         // Set output type to open drain
  GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR4;  // Set speed to high
  GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR4);      // Disable pull up and pull down resistors
  GPIOB->BSRRH |= GPIO_BSRR_BR_4;            // Drive the pin low
  // Pin should now be HIGH
  
  // PB5 as RESET
  GPIOB->MODER &= ~(GPIO_MODER_MODER5);      // Clear mode bits to set pin as input
  GPIOB->OTYPER |= GPIO_OTYPER_OT_5;         // Set output type to open drain
  GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5;  // Set speed to high
  GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR5);      // Disable pull up and pull down resistors
  GPIOB->BSRRH |= GPIO_BSRR_BR_5;            // Drive the pin low
  // Pin should now be HIGH
  
  // PB1 as Slave Select
  GPIOB->MODER &= ~(GPIO_MODER_MODER1);      // Clear mode bits to set pin as input
  GPIOB->OTYPER |= GPIO_OTYPER_OT_1;         // Set output type to open drain
  GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR1;  // Set speed to high
  GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR1);      // Disable pull up and pull down resistors
  GPIOB->BSRRH |= GPIO_BSRR_BR_1;            // Drive the pin low
  // Pin should now be HIGH
}

void Clk_Pin_LOW(void)
{
  // Swith OD pin to output mode
  GPIOA->MODER |= GPIO_MODER_MODER11_0;
}

void Clk_Pin_HIGH(void)
{
  // Switch OD pin to input mode
  GPIOA->MODER &= ~(GPIO_MODER_MODER11_0);
}

void PC_Pin_LOW(void)
{
  // Swith OD pin to output mode
  GPIOB->MODER |= GPIO_MODER_MODER3_0;
}
  
void PC_Pin_HIGH(void)
{
  // Switch OD pin to input mode
  GPIOB->MODER &= ~(GPIO_MODER_MODER3_0);
}

void Cal_Pin_LOW(void)
{
  // Swith OD pin to output mode
  GPIOB->MODER |= GPIO_MODER_MODER4_0;
}
  
void Cal_Pin_HIGH(void)
{
  // Switch OD pin to input mode
  GPIOB->MODER &= ~(GPIO_MODER_MODER4_0);
}

void Rst_Pin_LOW(void)
{
  // Swith OD pin to output mode
  GPIOB->MODER |= GPIO_MODER_MODER5_0;
}

void Rst_Pin_HIGH(void)
{
  // Switch OD pin to input mode
  GPIOB->MODER &= ~(GPIO_MODER_MODER5_0);
}

void SS_Pin_LOW(void)
{
  // Swith OD pin to output mode
  GPIOB->MODER |= GPIO_MODER_MODER1_0;
}

void SS_Pin_HIGH(void)
{
  // Switch OD pin to input mode
  GPIOB->MODER &= ~(GPIO_MODER_MODER1_0);
}


