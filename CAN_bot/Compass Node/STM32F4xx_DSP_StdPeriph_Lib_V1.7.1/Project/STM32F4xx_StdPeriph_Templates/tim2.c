#include "utils.h"
#include "stm32f4xx.h"
#include "tim2.h"

void TIM2_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
 
	/* TIM2 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 2002 - 1; // 1 MHz down to 100 Hz (10 ms)
	//TIM_TimeBaseStructure.TIM_Period = 5 - 1; // 1 MHz down to 10 kHz (10 ms)
	TIM_TimeBaseStructure.TIM_Prescaler = 9000 - 1; // 90 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	/* TIM IT enable */
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	/* TIM2 enable counter */
	TIM_Cmd(TIM2, ENABLE);
}
