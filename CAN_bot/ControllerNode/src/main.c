/*********************************************************************
* Includes
*********************************************************************/
#include "main.h"
#include "stdio.h"
#include "string.h"
#include "usart.h"
#include "stream.h"
#include "utils.h"
#include "tim2.h"
#include "tim3.h"
#include "can.h"
#include "arm_math.h"
#include "core_cm4.h"
#include "kalman.h"
#include "swiftNav.h"
#include "navigation.h"
#include "watchdog.h"
#include "sbp.h"
#include "message.h"
#include "serial_lcd.h"
#include "adc.h"

/*********************************************************************
* Local Defines
*********************************************************************/
/* Waypoint Coordinates are in mm
 * Velocity set point is in mm/s
 * Feedback gains are unit-less, derived from controller design
 */
#define WA_X  -2948
#define WA_Y   4632
#define WB_X  -240551
#define WB_Y   4145

//#define WA_X   1000
//#define WA_Y   -1000
//#define WB_X   1000
//#define WB_Y   1000

#define V_SET  1050

/* Not too bad at 1 m/s */
//#define FB_K1  -200.000f
//#define FB_K2  -15.6830f
//#define FB_K3  -10.9495f

/* Ok for 1 m/s */
//#define FB_K1  -245.000f
//#define FB_K2  -15.000f
//#define FB_K3  -11.000f

/* Run 1 @ 1m/s */
//#define FB_K1  -18.5000f
//#define FB_K2  -3.4000f
//#define FB_K3  -3.000f

/* Best gains for 1 m/s */
#define FB_K1  -30.6889f
#define FB_K2  -2.3253f
#define FB_K3  -2.4527f

/* for 2 m/s */
//#define FB_K1  -18.3670f
//#define FB_K2  -18.6880f
//#define FB_K3  -7.8099f

/* for 1 m/s regulation test, no step */
//#define FB_K1  -28.2287f
//#define FB_K2  -13.2621f
//#define FB_K3  -5.9065f

/*********************************************************************
* Global Variables
*********************************************************************/
uint8_t time_empty = 1;
uint8_t steer_empty = 1;
volatile uint8_t lcd_update_f = 0;
volatile uint8_t ser_update_f = 0;

volatile uint8_t send_logger_msg_f = 0;
volatile uint8_t log_start_f = 0;
volatile uint8_t log_stop_f = 0;
uint8_t man_log_mode = 0;

char msg[MAX_MSG_LEN];
char rj[30];
volatile uint16_t ADCConvertedValues[NUM_CHAN];
uint32_t ID_filters[] = { TIME_REF_ID, NED_POS_ID, NED_VEL_ID, STEER_ID, 0x0000 };
int str_i;
Rover_t rover;
uint32_t flags      = 1;
uint8_t mbx         = 0;
uint16_t timeout    = 0;
uint16_t timeoutMax = 5000;
uint8_t status      = 0;
uint8_t tx_done     = 0;
uint8_t buf_size    = 0;
uint32_t enabled    = 0;
uint16_t i;

uint16_t readingI = 0;
float readingF    = 0.0f;

int32_t ConversionTable[10] = {0};

NED_point_t posA;
NED_point_t posB;
NED_point_t posC;
NED_line_t line;

/* Transmit Structure preparation */
CanTxMsg TxBuf[6];
volatile uint8_t m = 0;
CanTxMsg FeedbackCmdMsg;
CanTxMsg startRecMsg;
CanTxMsg stopRecMsg;
CanTxMsg Data0Msg;
CanTxMsg Data1Msg;
CanTxMsg Data2Msg;
CanTxMsg Data3Msg;
CanRxMsg RxMessage;
CanRxMsg temp;

// Message Arrays
CanRxMsg time_msgs[CAN_FIFO_LEN];
CanRxMsg base_NED_msgs[CAN_FIFO_LEN];
CanRxMsg vel_NED_msgs[CAN_FIFO_LEN];
CanRxMsg heading_msgs[CAN_FIFO_LEN];
CanRxMsg steering_msgs[CAN_FIFO_LEN];

//CAN Rx Circular Buffers   head, tail, length,       data array
MsgBuf_t gpsTime_buf    = { 0,    0,    CAN_FIFO_LEN, &time_msgs[0] };
MsgBuf_t gpsPOS_buf     = { 0,    0,    CAN_FIFO_LEN, &base_NED_msgs[0] };
MsgBuf_t gpsVEL_buf     = { 0,    0,    CAN_FIFO_LEN, &vel_NED_msgs[0] };
MsgBuf_t heading_buf    = { 0,    0,    CAN_FIFO_LEN, &heading_msgs[0] };
MsgBuf_t steer_buf      = { 0,    0,    CAN_FIFO_LEN, &steering_msgs[0] };

/*********************************************************************
* Public Functions Prototypes
*********************************************************************/
void error(const char *c, uint8_t halt);
void clearMsg(CanRxMsg *msg);
void system_init(void);
void init_sys_variables(void);
void run(uint8_t inputState);
void initMode(void);
void manRdyMode(void);
void manRunMode(void);
void autoRdyMode(void);
void autoRunMode(void);
void eStopMode(void);
void waitForButtonRelease(void);
void can_flush(void);
void update(Rover_t *rover);
void consumeStateMsgs(Rover_t *rover);


/*
* Function: system_init
* Params:   none
* Returns:  none
* Info:     Starts all hardware modules, enables all system functions, prepares for main loop execution
*/
void system_init(void)
{
  RCC_ClocksTypeDef RCC_Clocks;
  RCC_GetClocksFreq(&RCC_Clocks);
	SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);

  // Serial Terminal
  init_USART3_DMA_Tx_Stream(230400);


  LED_Setup();

  snprintf(msg, MAX_MSG_LEN, "\f\r\n\r\n\f");
  snprintf(msg, MAX_MSG_LEN, "\f%s\r\nTyler Troyer\r\nState Space Controller Node\r\nVersion 1v0\r\nTarget: STM32F446RET 180MHz\r\n%s", Green, none); USART3_Stream(msg);
	snprintf(msg, MAX_MSG_LEN, "\n%sGetting clocks..\r\n", none); USART3_Stream(msg);
	snprintf(msg, MAX_MSG_LEN, "\r\nSYSCLK:%luM\r\n", RCC_Clocks.SYSCLK_Frequency/1000000); USART3_Stream(msg);
	snprintf(msg, MAX_MSG_LEN, "HCLK:\t%luM\r\n", RCC_Clocks.HCLK_Frequency/1000000); USART3_Stream(msg);
	snprintf(msg, MAX_MSG_LEN, "PCLK1:\t%luM\r\n", RCC_Clocks.PCLK1_Frequency/1000000); USART3_Stream(msg);
	snprintf(msg, MAX_MSG_LEN, "PCLK2:\t%luM\r\n\n", RCC_Clocks.PCLK2_Frequency/1000000); USART3_Stream(msg);
  LED(ON);
  delay(200);
  LED(OFF);
  snprintf(msg, MAX_MSG_LEN, "Trying to start CANbx\r\n"); USART3_Stream(msg);
  while(CAN1_Config(&ID_filters[0]) != CAN_InitStatus_Success && i <= 200)
  {
    error("Failed to start CAN_bx module", 1);
  }

  snprintf(msg, MAX_MSG_LEN, "CANbx 2.0b module started..\r\n");  USART3_Stream(msg);
  delay(250);
  snprintf(msg, MAX_MSG_LEN, "Waiting on lcd..\r\n");  USART3_Stream(msg);

  initButton();

  /* If this is a first power-up for the entire robot, wait for the GPS and LCD to boot up */
//  if(RCC_GetFlagStatus(RCC_FLAG_SFTRST) == SET)
//  {
//    /* No need to wait if the rest of the system is already powered on */
//    /* Go ahead and print to LCD right away */
//  }
//  else if(RCC_GetFlagStatus(RCC_FLAG_PORRST) == SET)
//  {
//    int i;
//
//    for(i = 0; i < 53; i++)
//    {
//      LED(1);
//      delay(100);
//      LED(0);
//      delay(100);
//    }
//  }
//  else if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST) == SET)
//  {
//    snprintf(msg, MAX_MSG_LEN, "Watchdog triggered reset\r\n");  USART3_Stream(msg);
//  }

  delay(3000);
  RCC_ClearFlag();

  /* E-Stop Button, remote switch, and joystick
   * DMA continuously updates ADCConvertedValues[]
   * array with enabled channels/pins */
  ADC_Configuration();

  /* LCD Display + LCD Update periodic interrupt timer */
  LCDInit();

  delay(1000);
  TIM2_Config();

  /* TTCAN Timer */
  TIM3_Config();

  /* Output pin for logic analyzer */
  init_triggerPin();

  snprintf(msg, MAX_MSG_LEN, "started..\r\n");  USART3_Stream(msg);
  watchdogInit();
}

/*
* Function: init_sys_variables
* Params:   none
* Returns:  none
* Info:     structs like rover and can messages can be more easily initialized at runtime, instead of statically
*/
void init_sys_variables(void)
{
  can_TxMsg_init(&FeedbackCmdMsg, FEEDACK_ID, FEEDACK_ID, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_TxMsg_init(&startRecMsg, SRT_LOG_ID, SRT_LOG_ID, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_TxMsg_init(&stopRecMsg, STP_LOG_ID, STP_LOG_ID, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_TxMsg_init(&Data0Msg, DATA0_ID, DATA0_ID, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_TxMsg_init(&Data1Msg, DATA1_ID, DATA1_ID, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_TxMsg_init(&Data2Msg, DATA2_ID, DATA2_ID, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_TxMsg_init(&Data3Msg, DATA3_ID, DATA3_ID, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_RxMsg_init(&RxMessage, 0, 0, CAN_Id_Standard, CAN_RTR_Data, 8, 0);
  can_RxMsg_init(&temp, 0, 0, CAN_Id_Standard, CAN_RTR_Data, 8, 0);

  i = 0;
  buf_size = 0;
  LED(OFF);

  /* Set waypoints and inital state */
  /* Need to write set__() functions for these to check bounds */
  rover.line.A.X = WA_X;
  rover.line.A.Y = WA_Y;
  rover.line.B.X = WB_X;
  rover.line.B.Y = WB_Y;
  rover.mode = INIT;

  /* 2.0 m/S MAX is allowed*/
  setAutoSpeed(&rover, V_SET);
}

/*********************************************************************
* Main Function (State Machine)
*********************************************************************/
int main(void)
{
  system_init();
  init_sys_variables();

  while(1)
  {
    run(rover.mode);
  }
}

/***************************************************************************************
* Interrupts - (See stream.c and stream2.c for serial buffer TX DMA interrupt handlers
***************************************************************************************/
/*
* Handler: SysTick_Handler
* Params:  none
* Returns: none
* Info:    Runs at 1 kHz, updates delays variable
*/
void SysTick_Handler(void)
{
  TimingDelay_Decrement();
}

/*
* Handler: CAN1_RX0_IRQHandler
* Params:  none
* Returns: none
* Info:    Sorts incoming can messages, and keeps track of system time
*          by starting TIM3 on time reference message reception
*/
void CAN1_RX0_IRQHandler(void)
{
  if(CAN_GetITStatus(CAN1, CAN_IT_FMP0) != RESET)
  {
    CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);
    CanRxMsg tempMsg;
    trigger_pin(1);
    volatile uint8_t num = CAN_MessagePending(CAN1, 0);

    if(num != 0)
    {
      CAN_Receive(CAN1, 0, &tempMsg);

      if(tempMsg.StdId == TIME_REF_ID)
      {
        TIM_SetAutoreload(TIM3, 664);  // Set TX window delay for 7 mS
        m = 0;
        send_logger_msg_f = 0;
        TIM_Cmd(TIM3, ENABLE);
        can_rx_fifo_write(&gpsTime_buf, tempMsg);
      }
      else if(tempMsg.StdId == NED_POS_ID)
      {
        can_rx_fifo_write(&gpsPOS_buf, tempMsg);
      }
      else if(tempMsg.StdId == NED_VEL_ID)
      {
        can_rx_fifo_write(&gpsVEL_buf, tempMsg);
      }
      else if(tempMsg.StdId == COMPASS_HEAD_ID)
      {
        can_rx_fifo_write(&heading_buf, tempMsg);
      }
      else if(tempMsg.StdId == STEER_ID)
      {
        can_rx_fifo_write(&steer_buf, tempMsg);
      }
      else
      {
        // Why are we here?
      }
      trigger_pin(0);
      num = CAN_MessagePending(CAN1, 0);
    }
  }
}

/*
* Handler: TIM3_IRQHandler
* Params:  none
* Returns: none
* Info:    Timer3 starts when time ref msg is Rx'd. TIM3 expires
*          and runs this interrupt, which TXs our CAN msgs at just
*          the right time. Handler re-enables the timer when there
*          is more than one CAN msg to TX in repeating mS periods
*/
void TIM3_IRQHandler(void)
{
  // It's finally time to try and TX during the allotted timeslot
  // First handle the TT msg queue (feedback and dataN msgs), then the sporadic msg queue (Logger start/stop msgs)
  if(TIM_GetITStatus(TIM3, TIM_IT_Update) == SET)
  {
    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);

    // Each message is 1 mS apart in time
    TIM_SetAutoreload(TIM3, 100);

    // This flag can only be set after the period message queue is already sent
    if(send_logger_msg_f) // Send logger msg, and clear flag
    {
      // Sending the last msg, the sporadic logger msg - no need to re-enable the TX timer
      CAN_Transmit(CAN1, &TxBuf[5]);
      send_logger_msg_f = 0;
    }
    else
    {
      CAN_Transmit(CAN1, &TxBuf[m]);
    }

    // Advance the index through the TxBuf[] message queue
    m++;

    // If we're done sending the entire periodic msg queue
    if(m >= NUM_MSGS - 1)
    {
      /* Wrap index pointer */
      m = 0;

      /* Read if sporadic msg flags were set by the application state machine
      *  If so, schedule another message to be sent by raising the send_logger_msg_f flag,
      *  Starting the 1mS timer, and clearing the application flags to acknowledge them
      * Next time we arrive in this ISR, we'll send whatever msg was placed in the
      * TxBuf[5] by the application, since we raise the send_logger_msg_f flag.
      */
      if(log_start_f || log_stop_f)
      {
        send_logger_msg_f = 1;
        TIM_Cmd(TIM3, ENABLE);
        log_start_f = 0;
        log_stop_f = 0;
      }
    }
    else
    {
      // Re-enable the time for the next message we need to transmit
      TIM_Cmd(TIM3, ENABLE);
    }
  }
}

/*
* Handler: TIM2_IRQHandler
* Params:  none
* Returns: none
* Info:    Periodically raises an LCD update flag variable if the LCD was updated
*          This signals the sysUpdate() function to update the LCD, and when not to.
*          Runs at 5 Hz, likely the fastest the LCD can work while at 19200 baud
*/
void TIM2_IRQHandler(void)
{
  if(TIM_GetITStatus(TIM2, TIM_IT_Update) == SET)
  {
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

    // Raise a signal that it's OK to run an LCD update
    // Update routine might be running, so make sure its complete before re-raising the flag
    if(lcd_update_f == 0)
    {
      lcd_update_f = 1;
    }
    if(ser_update_f == 0)
    {
      ser_update_f = 1;
    }
  }
}

/*********************************************************************
* Finite-State-Machine States (Robot Operation Modes)
*********************************************************************/
/*
* Function: initMode
* Params:   none
* Returns:  none
* Info:     Robot is waiting for user to move to MAN_RDY
*           Simply runs system I/O while waiting for
*           to move state machine
*/
void initMode(void)
{
  delay(100);
  LED(0);

  TxBuf[1] = Data0Msg;
  TxBuf[2] = Data1Msg;
  TxBuf[3] = Data2Msg;
  TxBuf[4] = Data3Msg;

  while(rover.mode == INIT)
  {
    update(&rover);
    setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);

    if(rover.E)
    {
      rover.mode = ESTOP;
    }
    else if(rover.D || rover.J)
    {
      while(rover.A || rover.B || rover.C || rover.D || rover.J)
      {
        update(&rover);
        setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);
      }
      rover.mode = MAN_RDY;
      break;
    }
  }
}

/*
* Function: manRdyMode
* Params:   none
* Returns:  none
* Info:     Monitors E-stop button
*           and user input to move between modes
*/
void manRdyMode(void)
{
  /* We just moved into this state because of a button press
  *  Wait for button to be released
  *  Definitely be updating the system in the meantime */
  waitForButtonRelease();

  while(rover.mode == MAN_RDY)
  {
    update(&rover);
    setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);

    if(rover.E)
    {
      rover.mode = ESTOP;
    }
    else if(rover.D || rover.J)
    {
      rover.mode = AUTO_RDY;
    }
    else if(rover.A)
    {
      rover.mode = MAN_RUN;
    }
    else if(rover.C)
    {
      //man_log_mode = 1;
      rover.mode = MAN_RUN;
    }
  }
}

/*
* Function: manRunMode
* Params:   none
* Returns:  none
* Info:     Moves robot according to joystick position
*           and monitors E-stop button for safety
*/
void manRunMode(void)
{
  waitForButtonRelease();

  //uint8_t firstRun = 1;
  //uint8_t wasRunning = 0;

  while(rover.mode == MAN_RUN)
  {
    update(&rover);
    setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);

//    if(firstRun && man_log_mode)
//    {
//      /* TIM3 ISR finds this flag, and this Can Msg in the Tx Buffer later on */
//      log_start_f = 1;
//      TxBuf[5] = startRecMsg;
//      firstRun = 0;
//    }
//    else
//    {
//      wasRunning = 1;
//    }

    // Do Manual Driving Mode things
    if(ser_update_f)
    {
      snprintf(msg, MAX_MSG_LEN, "\rX,Y\t%d   \t%d   \t", rover.joystickX, rover.joystickY); USART3_Stream(msg);
      ser_update_f = 0;
    }

    // Feedback message is already written to, just override some of the contents
    // With manual control cmds instead of automatic ones
    //rover.speed_setPoint = getJoystickDriveCmd(joystickY);
    //rover.u              = getJoystickSteeringRate(joystickX);

    // Making the GPS speed read negative assumes an immediate rover response to "backward drive" joystick commands
//    if(rover.joystickY < 1900 && rover.speed > 0)
//    {
//      // What gets loaded into the feedback message is modified only in this case
//      // rover.speed goes back to being positive after next time message.c "getRoverSpeed() gets called
//      rover.speed = rover.speed * -1;
//    }

    if(rover.E)
    {
      rover.mode = ESTOP;
    }
    else if(rover.B || rover.D || rover.J)
    {
      rover.mode = MAN_RDY;
      delay(1);
      snprintf(msg, MAX_MSG_LEN, "\r\n"); USART3_Stream(msg);
    }

    delay(1); // Limits loop speed to 1 kHz max
  }
  /* We've left the loop, should we send a loggerStop msg? */
//  if(wasRunning && man_log_mode)
//  {
//    /* If we've started an auto-nav session, we need to stop the logger, regardless of the reason for the state change */
//    /* TIM3 ISR finds this flag and message to send later on */
//    log_stop_f = 1;
//    TxBuf[5] = stopRecMsg;
//    man_log_mode = 0;
//  }
}

/*
* Function: autoRdyMode
* Params:   none
* Returns:  none
* Info:     Monitors appropriate buttons and system
*           before trying to move to AUTO_RUN mode
*/
void autoRdyMode(void)
{
  waitForButtonRelease();

  // Just be ready to move to a new state
  while(rover.mode == AUTO_RDY)
  {
    update(&rover);
    setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);

    if(ser_update_f)
    {
      if(rover.fix == 1)
      {
        snprintf(msg, MAX_MSG_LEN, "\rGPS Fix: Ready       "); USART3_Stream(msg);
      }
      else
      {
        snprintf(msg, MAX_MSG_LEN, "\rGPS Fix: Unavailable "); USART3_Stream(msg);
      }
      ser_update_f = 0;
    }

    if(rover.E)
    {
      rover.mode = ESTOP;
    }
    else if(rover.D || rover.J)
    {
      rover.mode = MAN_RDY;
    }
    else if(rover.A)  // Must have RTK fix to enter auto nav mode
    {
      if(rover.fix)
      {
        rover.mode = AUTO_RUN;
      }
    }
  }
  snprintf(msg, MAX_MSG_LEN, "\r\n"); USART3_Stream(msg);
}

/*
* Function: autoRunMode
* Params:   none
* Returns:  none
* Info:     Uses CAN messages to run in auto-navigation mode
*           Runs State Space Controller and Estimator
*           Starts and Stops KVASER CAN data logger device
*           Relies on a GPS fix and user input to start
*           and stop control
*           Mode will move to AUTO_RDY mode is GPS fix
*           is suddenly lost
*/
void autoRunMode(void)
{
  waitForButtonRelease();
  can_flush();
  uint8_t firstRun = 1;
  uint8_t wasRunning = 0;

  while(rover.mode == AUTO_RUN)
  {
    update(&rover);
    setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);

    if(rover.fix)
    {
      if(firstRun)
      {
        /* TIM3 ISR finds this flag, and this Can Msg in the Tx Buffer later on */
        log_start_f = 1;
        TxBuf[5] = startRecMsg;
        firstRun = 0;
        rover.trackHeading = 0.0f;
      }
      else
      {
        wasRunning = 1;
      }
      if(ser_update_f)
      {
        snprintf(msg, MAX_MSG_LEN, "\rXTE: %.2f\tPSI: %.1f\tDelta: %.2f\tU: %.2f  ", ((float)rover.xte)/1000, (float)rover.psi, (float)rover.delta, (float)rover.u); USART3_Stream(msg);
        ser_update_f = 0;
      }
    }
    else
    {
      /* We clearly cannot remain in AUTO navigation mode while GPS has no fix */
      rover.mode = AUTO_RDY;
      break;
    }
    if(rover.E)
    {
      rover.mode = ESTOP;
      break;
    }
    else if(rover.B || rover.D || rover.J)
    {
      /* User signals the end of auto nav session */
      rover.mode = AUTO_RDY;
      break;
    }
  }
  /* We've left the loop, should we send a loggerStop msg? */
  if(wasRunning)
  {
    /* If we've started an auto-nav session, we need to stop the logger, regardless of the reason for the state change */
    /* TIM3 ISR finds this flag and message to send later on */
    log_stop_f = 1;
    TxBuf[5] = stopRecMsg;
  }
}

/*
* Function: eStopMode
* Params:   none
* Returns:  none
* Info:     Stops all motor activity
*           Waits for E-Stop button to be reset
*           Moves robot back to INIT mode
*/
void eStopMode(void)
{
  waitForButtonRelease();

  while(rover.mode == ESTOP)
  {
    update(&rover);
    setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);

    /* We are here forever until the ESTOP buttons get reset */
    if(!rover.E)
    {
      rover.mode = INIT;
      break;
    }
  }
}

/*********************************************************************
* System Functions
*********************************************************************/
/*
* Function: run
* Params:   state variable as defined at the top of main.c
* Returns:  none
* Info:     This is the state machine switcher
*           Reads the current state of the state variable
*           And calls the corresponding mode loop
*           Robot stays in that mode loop until loop decides
*           to change system state. Then this function runs
*           again to determine which mode to move to. This
*           is sort of like our scheduler, deciding which
*           thread (mode) should be run.
*/
void run(uint8_t inputState)
{
  delay(2);
  LED(1);
  if(inputState == INIT)
  {
    snprintf(msg, MAX_MSG_LEN, "\r\nINIT\r\n"); USART3_Stream(msg);
    initMode();
  }
  else if(inputState == MAN_RDY)
  {
    //delay(2);
    snprintf(msg, MAX_MSG_LEN, "MAN RDY\r\n"); USART3_Stream(msg);
    manRdyMode();
  }
  else if(inputState == MAN_RUN)
  {
    snprintf(msg, MAX_MSG_LEN, "MAN RUN\r\n"); USART3_Stream(msg);
    manRunMode();
  }
  else if(inputState == AUTO_RDY)
  {
    //delay(2);
    snprintf(msg, MAX_MSG_LEN, "AUTO RDY\r\n"); USART3_Stream(msg);
    autoRdyMode();
  }
  else if(inputState == AUTO_RUN)
  {
    snprintf(msg, MAX_MSG_LEN, "AUTO RUN\r\n"); USART3_Stream(msg);
    autoRunMode();
  }
  else if(inputState == ESTOP)
  {
    snprintf(msg, MAX_MSG_LEN, "ESTOP\r\n"); USART3_Stream(msg);
    eStopMode();
  }
  else
  {
    // Stop the robot, we are in an unknown state
  }
}

/*
* Function: can_flush
* Params:   none
* Returns:  none
* Info:     Clears all CAN Rx Msg buffers of all messages
*           This can help reset the estimator state
*           and can help system restart from the newest msgs
*/
void can_flush(void)
{
  CAN_ITConfig(CAN1, CAN_IT_FMP0, DISABLE);
  can_buffer_flush(&gpsTime_buf);
  can_buffer_flush(&gpsPOS_buf);
  can_buffer_flush(&gpsVEL_buf);
  can_buffer_flush(&heading_buf);
  can_buffer_flush(&steer_buf);
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
}

/*
* Function: update
* Params:   pointer to a rover instance
* Returns:  void
* Info:     Call this function as often as humanly or (computationally) possible
*           This function keeps the system responsive and clued in on all
*           system inputs. It keeps the rover struct up to date,
*           the button and joystick and remote states up to date
*           All modes rely on variables that this function updates
*           Updates LCD screen at 5 Hz (according to TIM2 interrupt)
*           Updates Serial Terminal at 5 Hz
*           Makes sure robot states are updated with most recent CAN bus data
*           Always call this function all the time!!
*           Failure to call this function makes for a sad robot.  :(
*/
void update(Rover_t *rover)
{
  /* Update rover variables that come from any CAN msgs that arrived
   * Update rover variables that come from buttons and joysticks
   * Use all input/updated rover variables to calculate up-to-date output variables
   * If TIM2 interrupt has fired (fires at 5 Hz), update the LCD and serial terminal with new data
   * Kick the watchdog
   */
  consumeStateMsgs(rover);
  getJoystick(rover, ADCConvertedValues[1], ADCConvertedValues[0], JOYSTICK_DEADBAND_HALFWIDTH);
  getButtonStates(rover, &ADCConvertedValues[2], BUTTON_ADC_THRESHOLD);
  setSysOutputs(rover);
  setDataMsgs(rover, &TxBuf[TX_MSG_ORDER_SECOND]);

  /* Flag updated in Timer ISR at 5 Hz */
  if(lcd_update_f)
  {
    lcd_update(rover);
    lcd_update_f = 0;
  }

  /* Kick the watchdog */
  IWDG_ReloadCounter();
}

/*
* Function: consumeStateMsgs
* Params:   Rover stuct containing entire robot system state variables
* Returns:  The latest status of GPS fix, 0 or 1
* Info:     This functions runs after a full suite of scheduled CAN
*           messages arrive from the sensor nodes.
*           This function uses a ton of functions from message.c
*           to pull the important variables out of the CAN msg
*           packet format and into meaningful robot sensor variables.
*           Can be configured to used GPS heading instead of compass heading.
*           Empties out each message type buffer to ensure buffers are well-consumed.
*/
void consumeStateMsgs(Rover_t *rover)
{
  uint8_t i;
  uint8_t buf_size;

  //trigger_pin(1);

  // Consume all GPS Time Reference Messages, find most recently received one
  if(!can_rx_fifo_empty(&gpsTime_buf))
  {
    buf_size = can_rx_fifo_size(&gpsTime_buf);
    for(i = 0; i < buf_size; i++)
    {
      clearMsg(&RxMessage);
      can_fifo_read(&RxMessage, &gpsTime_buf);
    }
    // Unpack meaningful variables from most recent CAN msg
    getGPSTime(rover, &RxMessage);
    getSatFix(rover, &RxMessage);
    getCANState(rover, &RxMessage);
  }

  // Consume all GPS Position Messages, find most recently received one
  if(!can_rx_fifo_empty(&gpsPOS_buf))
  {
    buf_size = can_rx_fifo_size(&gpsPOS_buf);
    for(i = 0; i < buf_size; i++)
    {
      clearMsg(&RxMessage);
      can_fifo_read(&RxMessage, &gpsPOS_buf);
    }
    getXpos(rover, &RxMessage);
    getYpos(rover, &RxMessage);
    // Build points A, B, and C to find Cross Track Error
    getXTE(rover);
    getBearing(rover);
    getPSI(rover);
  }

  // Consume all GPS Velocity Messages, find most recently received one
  if(!can_rx_fifo_empty(&gpsVEL_buf))
  {
    buf_size = can_rx_fifo_size(&gpsVEL_buf);
    for(i = 0; i < buf_size; i++)
    {
      clearMsg(&RxMessage);
      can_fifo_read(&RxMessage, &gpsVEL_buf);
    }
    getXvel(rover, &RxMessage);
    getYvel(rover, &RxMessage);
    getSpeed(rover, &RxMessage);
    getTrackHeading(rover);
  }

  // Consume all Heading Messages
  if(!can_rx_fifo_empty(&gpsPOS_buf))
  {
    buf_size = can_rx_fifo_size(&heading_buf);
    for(i = 0; i < buf_size; i++)
    {
      clearMsg(&RxMessage);
      can_fifo_read(&RxMessage, &heading_buf);
    }
    // Don't use compass node to start with
    //getHeading(rover, &RxMessage);
  }

  // Consume all Steering Messages
  if(!can_rx_fifo_empty(&steer_buf))
  {
    buf_size = can_rx_fifo_size(&steer_buf);
    for(i = 0; i < buf_size; i++)
    {
      clearMsg(&RxMessage);
      can_fifo_read(&RxMessage, &steer_buf);
    }
    // Steering ADC Reading?
    getSteeringPotentiometer(rover, &RxMessage);
    // Steering Angle from potentiometer reading
    getDELTA(rover);
  }

  // Run State Estimator using updated state measurement
  // getStateEstimate(rover);

  // Generate Feedback signal u
  getFeedback(rover, FB_K1, FB_K2, FB_K3);

  //trigger_pin(0);
}

/*
* Function: waitForButtonRelease
* Params:   none
* Returns:  none
* Info:     This is used frequently for mode transitions
*           Used to prevent multiple mode transitions from a single button press
*/
void waitForButtonRelease(void)
{
  while(rover.A || rover.B || rover.C || rover.D || rover.J)
  {
    update(&rover);
    setFeedbackMessage(&rover, &TxBuf[TX_MSG_ORDER_FIRST], &FeedbackCmdMsg);
  }
  delay(100);
  LED(0);
}

/*
* Function: clearMsg
* Params:   Can Packet
* Returns:  none
* Info:     resets a CanRxMsg type in a single function call
*/
void clearMsg(CanRxMsg *msg)
{
  msg->StdId = 0;
  msg->IDE = CAN_Id_Standard;
  msg->DLC = 8;
  msg->RTR = CAN_RTR_Data;
  msg->Data[0] = 0;
  msg->Data[1] = 0;
  msg->Data[2] = 0;
  msg->Data[3] = 0;
  msg->Data[4] = 0;
  msg->Data[5] = 0;
  msg->Data[6] = 0;
  msg->Data[7] = 0;
}

/*
* Function: estimateStates
* Params:   measured system state vector [3 x 1], x = [x1; x2; x3];
* Returns:  estimate of state [3 x 1] or augmented xAug[6 x 1] struct with NUM_States as parameter
* Info:     Uses static variables and kalman filtering or Luenburger observer
*           to provide a cleaned-up version of system state to feedback generator.
*/
void estimateStates(void)
{
  #ifdef LUENBURGER

  #endif
  #ifdef KALMAN

  #endif
  #ifdef AUG_KALMAN

  #endif
}

/*
* Function: error
* Params:   null-terminated character array and halt flag (0, or 1)
* Returns:  none
* Info:     Presents a red serial terminal message in the case of an error.
*           Halt flag can leave the system sitting in an empty while loop
*           in case the error is bad enough to warrant fail-safe operation.
*/
void error(const char *c, uint8_t halt)
{
  snprintf(msg, MAX_MSG_LEN, "%s\r\n\nError: ", Red); USART3_Stream(msg);
  while(*(c))
  {
    USART3_putc(*c);
    c++;
  }
  printf("\r\n");
  if(halt)
  {
    snprintf(msg, MAX_MSG_LEN, "CPU Stopped  :(%s", none); USART3_Stream(msg);
    while(1)
    {
      // Blink RED LED forever
    }
  }
}
