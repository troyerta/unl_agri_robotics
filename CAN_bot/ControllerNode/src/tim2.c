#include "utils.h"
#include "stm32f4xx.h"
#include "tim2.h"

// 32 Bit Timer clocked at something crazy
void TIM2_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	/* TIM2 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	/* Time base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = 9000 - 1; // 90 MHz Clock down to 10 kHz (adjust per your clock)
	TIM_TimeBaseStructure.TIM_Period = 2000;        // 10 kHz down to 5 Hz (200 ms) (max LCD update speed)
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

  // TIM IT enable
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
  TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

  // Enable the TIM3 interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  TIM_Cmd(TIM2, ENABLE);
}


