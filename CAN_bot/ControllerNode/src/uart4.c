/*
 * usart.c

 *
 *  Created on: Oct 12, 2015
 *      Author: ttroyer2
 */

/********************************************************************************************************************
 *
 * Includes
 *
 *******************************************************************************************************************/
#include "stm32f4xx.h"
#include "stdint.h"
#include "stdio.h"
#include "uart4.h"

/********************************************************************************************************************
 *
 * Function Implementations
 *
 *******************************************************************************************************************/
void UART4_Configuration(uint32_t baudRate)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// Configure USART //
	USART_InitStructure.USART_BaudRate = baudRate;
	USART_InitStructure.USART_WordLength = USART_WordLength_9b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_Even;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);
	USART_Cmd(USART1, ENABLE);

	// USART GPIO Configure for PC0 and PC11
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; // PC.0 UART4_TX, PC.1 UART4_RX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Connect USART pins to AF //
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
}

char UART4_getc(void)
{
	//while(USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == RESET);
   // return (uint8_t)USART_ReceiveData(USART3);
	while((USART1->SR & USART_FLAG_RXNE) == 0);
	return USART1->DR;
}

void UART4_putc(char c)
{
	//USART_SendData(USART3, c);
	//while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);
	while((USART1->SR & USART_FLAG_TXE) == 0);
	USART1->DR = c;
	return;
}

void UART4_puts(char *str)
{
  while(*str)
  {
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, *str++);
  }
}


