#include "main.h"
#include "usart.h"
#include "tim2.h"

#include "utils.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "watchdog.h"

uint8_t init(uint8_t verbose, uint8_t watchdog)
{
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	uint8_t recovery = 0;
		
	// Capture error
	if(SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000))
	{
		while(1);
	}
		
	USART3_Configuration(115200);
	delay(1);
	printf("Init...\r\n");
	delay(10);
		
	if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET)
	{
		// Light an LED or something
		printf("\n\nIWDG RESET\r\n");
		RCC_ClearFlag();
		recovery = 1;
	}

	if(watchdog)
	{
		watchdogInit();
		IWDG_Enable();
	}

	return recovery;
}
