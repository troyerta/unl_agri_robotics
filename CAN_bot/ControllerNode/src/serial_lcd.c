/**
 * @file    serial_lcd.c
 * @brief   Serial LCD interface module code.
 *
 * @{
 */
#include "stdint.h"
#include "string.h"
#include <inttypes.h>
#include "stream.h"
#include "uart4.h"
#include "utils.h"
#include "tim2.h"
#include "main.h"
#include "message.h"
#include "serial_lcd.h"

// Use this to keep GCC from giving dumb warnings
// Obviously, ints are 32 bit here in our case
#pragma GCC diagnostic ignored "-Wformat"

//#define waitMicros(x) gptStart(&GPTD6, &gpt6cfg);gptPolledDelay(&GPTD6, x);
//#define waitMillis(x) chThdSleepMilliseconds(x);
static char msg[MAX_MSG_SIZE];
void sendByte(uint8_t byte);
uint8_t getByte(void);

uint8_t firstRun = 1;
static char rj[30];
static uint16_t str_i; // keeps track of the overall size of the entire serial message buffer
static uint16_t str_l; // keeps track of individual parts that we print, so we can tell the lcd how large each string is

uint8_t lcd_reset[] = { CMD_RESET_LCD };
uint8_t lcd_settings[] = { CMD_BKLIGHT_CTRL, 0,
                           CMD_CLR_SCREEN,
                           CMD_SET_CURSOR_INCREMENT, 0, 0,
                           CMD_WR_LCD_CURSOR_LOCATION, 0, 0,
                           CMD_FONT_SELECT, 0xB0, 0x01,
                           CMD_SET_BACKGROUND_COLOR, 0, 0, 0,
                           CMD_SET_FOREGROUND_COLOR, 0xFF, 0xFF, 0xFF,
                           CMD_BKLIGHT_CTRL, 255,
                          };



void thousandthsToWhole(int32_t input, int32_t *whole, int32_t *frac);
void uThousandthsToWhole(uint32_t input, uint32_t *whole, uint32_t *frac);
void hundredthsToWhole(uint16_t input, uint32_t *whole, uint32_t *frac);
void tenthsToWhole(uint32_t input, uint32_t *whole, uint32_t *frac);
void lcd_put_var(void);
void lcd_move_cursor(uint8_t x, uint8_t y);
void lcd_set_font_color(uint8_t r, uint8_t g, uint8_t b);

void LCDInit(void)
{

  //UART4_Configuration(19200);
  //TIM2_Config();
  init_USART1_DMA_Tx_Stream(19200);
  delay(100);

  // Send some setup commands
  //lcd_Reset();
  USART1_Stream((char *)&lcd_reset[0], 1);
  delay(100);

  //lcd_BackLight(0);
  //lcd_clear(); delay(100);

  //lcd_SetCursorINC(0, 0); delay(100);
  //lcd_SetCursor(1, 0); delay(100);
  //lcd_SetFont(0xB001); delay(100);
  //lcd_SetBackgroundColor(0, 0, 0); delay(100);
  //lcd_SetForegroundColor(0, 0, 0); delay(100);

//  for(i = 0; i < 255; i++)
//  {
//    // Send some setup commands
//    lcd_BackLight(i);
//    delay(3);
//  }

  USART1_Stream((char *)&lcd_settings[0], sizeof(lcd_settings));
  delay(200);

  lcd_print_background();

  delay(100);
}

/*
 * @param: 	byte 	- the byte to be sent over SPI to LCDTERM
 * @return: c  		- the byte LCDTERM sent back to the master
 * @desc:	Wrapper function for UART0_Sendchar() to add a 250 us
 * 			delay.
 */
void sendByte(uint8_t byte)
{
  UART4_putc(byte);
}

/*
 * @param:	void
 * @return:	UART0_Getchar()	- byte from LCDTERM
 * @desc:	Wrapper function for UART0_Getchar()
 */
uint8_t getByte(void)
{
  return UART4_getc();
}

/*
 * @param: 	void
 * @return:	void
 * @desc: 	CMD_CLR_SCREEN (0x25)
 * 			Clears the LCD screen. Also sets cursor back to 0,0
 * 			If scrolling is enabled text will start at top and
 * 			scroll once it hits the bottom.
 */
void lcd_clear(void)
{
  sendByte(CMD_CLR_SCREEN);
  //delay(1);
}

/*
 * @param: 	x 	- the x coordinate to set LCDTERM's cursor to
 * @param: 	y	- the y coordinate to set LCDTERM's cursor to
 * @return: void
 * @desc:	CMD_WR_LCD_CURSOR_LOCATION (0x30)
 * 			Sets the x and y position of LCDTERM's cursor.
 * 		  	This command will be put onto LCDTERM's queue.
 */
void lcd_SetCursor(uint8_t x, uint8_t y)
{
  sendByte(CMD_WR_LCD_CURSOR_LOCATION);
  sendByte(x);
  sendByte(y);
  //delay(15);
}

/*
 * @param:	x	- The amount of space in pixels between glyphs in the x direction
 * @param:	y 	- The amount of space in pixels between glyphs in the y direction
 * @return: void
 * @desc:	CMD_SET_CURSOR_INCREMENT (0x76)
 * 			This command sets the amount of space between each glyph.
 * 			There is a limit of a 3 pixel space between glyphs in both
 * 			x and y directions.
 * 			EX. Test String
 * 				T e s t  S t r i n g
 */
void lcd_SetCursorINC(uint8_t x, uint8_t y)
{
  sendByte(CMD_SET_CURSOR_INCREMENT);
	sendByte(x);
	sendByte(y);
	//delay(15);
}

/*
 * @param:	c	- The character you want to display to the screen.
 * @return:	void
 * @desc:	CMD_LCD_PUTC (0x72)
 * 			Prints the current character in the currently selected font.
 * 			The glyph's top left corner will start at the current x,y
 * 			location which LCDTERM keeps track of.
 * 			The default font code is 0xB001.
 * 			\r causes the cursor's x to be set back to 0.
 * 			\n causes the cursor's x to be set back to 0 and for y to increase
 * 			by the height of the last used glyph.
 * 			If you have a special character use that character's hex code
 * 			in order to print it.
 * 			EX. special char code: 0x81
 * 				lcd_putc(0x81);
 * 				lcd_putc('\x81');
 */
void lcd_Putc( const uint8_t c )
{
  sendByte(CMD_LCD_PUTC);
	sendByte(c);
	delay(15);
}

/*
 * @param:	str	- pointer to first slot of character array
 * @return: void
 * @desc:	CMD_LCD_PUTS (0x73)
 * 			Prints a null terminated string up to 255 characters in length.
 * 			The length parameter is a single byte and therefore can only
 * 			go as high as 255 without overflow.
 * 			Each character in the string has to also be in the current font.
 * 			The default font is 0xB001.
 * 			If there are special characters use their hex codes to use them.
 * 			EX. 	lcd_puts("Normal String.\n");
 * 					lcd_puts("Special char \x81."); // hex code 0x81 is used
 */
void lcd_Puts(char* str)
{
  uint32_t length = strlen(str);
  uint8_t k = 0;
  sendByte(CMD_LCD_PUTS);
  sendByte(length);
  for(k = 0; k < length; k++)
  {
    sendByte(str[k]);
  }
  //delay(1);
}

/*
 * @param:	r	- 0 - 255 red value
 * 			g	- 0 - 255 green value
 * 			b	- 0 - 255 blue value
 * @return: void
 * @desc:	CMD_SET_FOREGROUND_COLOR (0x51)
 * 			LCDTERM converts the RGB888 value given into its RGB565 value
 * 			that it will use for the foreground.
 */
void lcd_SetForegroundColor(uint8_t r, uint8_t g, uint8_t b)
{
  sendByte(CMD_SET_FOREGROUND_COLOR);
	sendByte(r);
	sendByte(g);
	sendByte(b);
	//delay(15);
}

/*
 * @param:	r	- 0 - 255 red value
 * 			g	- 0 - 255 green value
 * 			b	- 0 - 255 blue value
 * @return: void
 * @desc:	CMD_SET_BACKGROUND_COLOR (0x50)
 * 			LCDTERM converts the RGB888 value given into its RGB565 value
 * 			that it will use for the background.
 */
void lcd_SetBackgroundColor(uint8_t r, uint8_t g, uint8_t b)
{
	sendByte(CMD_SET_BACKGROUND_COLOR);
	sendByte(r);
	sendByte(g);
	sendByte(b);
	//delay(15);
}

/*
 * @param:	fontCode - the two byte font code identification
 * @return: void
 * @desc:	CMD_FONT_SELECT (0x44)
 *			Changes the current font to the font code given if that font
 *			exists.
 *			In order to find out what fonts are currently installed
 *			on LCDTERM just use the list fonts command. CMD_FONT_LIST (0x41)
 */
void lcd_SetFont(uint16_t fontCode)
{
	uint8_t upper, lower;
	upper = (fontCode >> 8);
	lower = fontCode;
	sendByte(CMD_FONT_SELECT);
	//delay(1);
	sendByte(upper);
	sendByte(lower);
	//delay(15);
}

/*
 * @param:	uint 	- unsigned integer to be printed
 * @return: void
 * @desc:	CMD_LCD_PUTUINT (0x74)
 * 			LCDTERM converts the 32 bit unsigned int into ASCII
 * 			and prints the number on the screen at the current x,y
 * 			locations.
 */
void lcd_Putuint(uint32_t uint)
{
	uint8_t a = uint >> 24;		// MSB first
	uint8_t b = uint >> 16;
	uint8_t c = uint >> 8;		// LSB last
	uint8_t d = uint;
	sendByte(CMD_LCD_PUTUINT);
	sendByte(a);				// MSB
	sendByte(b);
	sendByte(c);
	sendByte(d);				// LSB
	delay(15);
}

/*
 * @param:	nargs - number of arguments
 * 			start - number to start counting from
 * @return:	void
 * @desc:	CMD_INTERFACE_TEST (0x11)
 * 			Sends a series of bytes from start to start + nargs in order.
 * 			LCDTERM will print on the screen how many bytes were missed if any
 */
void lcd_UartTest(uint8_t nargs, uint8_t start)
{
	sendByte(CMD_INTERFACE_TEST);
	sendByte(nargs);
	sendByte(start);
	for(uint8_t volatile k = start;; ++k)
  {
		sendByte(k);
		if(k == nargs)
    {
      break;
    }
	}
	delay(15);
}

/*
 * @param: void
 * @return: void
 * @desc:	CMD_NO_OP (0x12)
 * 			no operation
 */
void lcd_NoOp(void)
{
  sendByte(CMD_NO_OP);
	sendByte(DMYBYTE);
	delay(15);
}

/*
 * @param: 	void
 * @return: void
 * @desc:	CMD_ESC_CMD (0x13)
 * 			Empties LCDTERMS command queue. Forces whatever command that was
 * 			being carried out to stop and clears the queue.
 */
void lcd_Escape(void)
{
	sendByte(CMD_ESC_CMD);
	sendByte(DMYBYTE);
	delay(30);	//The escape will have slave dequeue until empty. max of 1024
}

/*
 * @param:	returnVal -	pointer to an array that can hold at least 6 characters
 * @return	void
 * @desc:	CMD_LCD_ID(0x14)
 * 			Gets the ID (1, 2, 3) for the screen size and
 * 			the firmware version number of LCDTERM.
 * 			x.x.x
 */
void lcd_GetID(uint8_t* returnVal)
{
sendByte(CMD_LCD_ID);
	for(int k = 0; k < 6; ++k)
  {
		sendByte(DMYBYTE);
		returnVal[k] = getByte();
	}
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_RESET_LCD (0x15)
 * 			Resets LCDTERM to init settings.
 * 			Scrolling : off
 * 			foreground: white
 * 			background: black
 * 			cursor 	  : 0,0
 * 			This will also clear the screen.
 */
void lcd_Reset(void)
{
  sendByte(CMD_RESET_LCD);
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_FONT_LIST (0x41)
 * 			Lists the fonts installed on LCDTERM.
 * 			The format of the table is
 * 		EX.
 * 			Name			point size	font code
 * 			Arial_Narrow		10		  B001
 *
 * 			To change to a different font use the
 * 			lcd_setFont( fontCode ) function.
 */
void lcd_ListFonts(void)
{
	sendByte(CMD_FONT_LIST);
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_LIST_BITMAPS_IN_ROM (0x82)
 * 			Lists the fonts installed on LCDTERM.
 * 			The format of the table is
 * 		EX.
 * 			Name			Image Number
 * 			LCDTERM Logo		00
 */
void lcd_ListImages(void)
{
	sendByte(CMD_LIST_BITMAPS_IN_ROM);
	delay(15);
}

/*
 * @param:  void
 * @return: void
 * @desc:	CMD_SCROLLING_ON (0x32)
 * 			Turns scrolling feature on.
 * 			When text reaches the bottom. Instead of the screen
 * 			clearing it will shift all the text up by the height needed
 * 			for the next glyph to fit.
 */
void lcd_ScrollingOn(void)
{
	sendByte(CMD_SCROLLING_ON);
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_SCROLLING_OFF (0x33)
 * 			Turns scrolling feature off.
 * 			When text reaches the bottom of the screen. The screen
 * 			will automatically clear and set the cursor to 0,0.
 */
void lcd_ScrollingOff(void)
{
	sendByte(CMD_SCROLLING_OFF);
	delay(15);
}

/*
 * @param:	param	- 0 for sleep mode, 1 to wake up
 * @return:	void
 * @desc:	CMD_SLEEP_MODE(0x17)
 * 			Puts LCDTERM into sleep mode. When woken up
 * 			whatever was on the screen will still be there.
 */
void lcd_Sleep(uint8_t param)
{
	sendByte(CMD_SLEEP_MODE);
	sendByte( param );
	delay(15);
}

/*
 * @param:	imgNum 	- The image number you want to display
 * @return:	void
 * @desc:	CMD_LCD_DISPLAY_IMG_FROM_ROM (0x80)
 * 			Prints the selected image starting at the current x,y position.
 *    NOTE: If the image is wider than the current x postion will allow
 *    		the image will not output correctly. If the image is taller
 *    		than the current y position will allow then the image will
 *    		print starting at y location 0.
 */
void lcd_DisplayImage(uint8_t imgNum)
{
	sendByte(CMD_LCD_DISPLAY_IMG_FROM_ROM);
	sendByte(imgNum);
	delay(15);
}

/*
 * @param:	setting 	- 0 for off
 * 						- 1 for on
 * 						- 2-100 for PWM percentage
 * @return:	void
 * @desc:	CMD_BKLIGHT_CTRL (16)
 * 			changes the backlight to fully on, fully off
 * 			or anywhere inbetween based on a percentage.
 *
 */
void lcd_BackLight(uint8_t setting)
{
	sendByte(CMD_BKLIGHT_CTRL);
	sendByte( setting );
	//delay(15);
}

/*
 * @param:	x 	- number to store x location
 * 			y	- number to store y location
 * @return:	void
 * @desc:	CMD_RD_CURRENT_CURSOR_LOCATION (0x31)
 * 			gets the value of the cursor the instant this command
 * 			is sent. If LCDTERM is in the middle of printing it will
 * 			give you the value at that instant. It is a good idea
 * 			to allow for LCDTERM to finish it's commands before using
 * 			this one for that reason.
 */
void lcd_GetCursor(uint8_t *x, uint8_t *y)
{
	sendByte(CMD_RD_CUR_CURSOR_LOCATION);
	sendByte(DMYBYTE);
	*x = getByte();
	sendByte(DMYBYTE);
	*y = getByte();
	delay(15);
}

/*
 * @param:	void
 * @return:	returnByte - returns button that has/hasn't been pressed
 * @desc:	CMD_READ_KEY_PRESS (0x62)
 * 			Returns the first button in the LCDTERM button queue.
 * 			If no button has been pressed 0xF0 will be returned.
 * 		EX.
 * 			while(1){
 * 				uint8_t button = lcd_getButtons();
 * 				if(button == 0x01) { ... }
 * 				else if(button == 0x02) { ... }
 * 				else if(button == 0x03 { ... }
 * 				else  { // No button }
 * 			}
 */
uint8_t lcd_GetButtons(void)
{
	uint8_t returnByte = 0;
	sendByte(CMD_READ_KEY_PRESS);
	sendByte(DMYBYTE);
	returnByte = getByte();
	delay(40);			// longer delay for button queue logic + command processing time
	return returnByte;
}

/*
 * @param:	val	- the 8 bit value to print
 * @return:	void
 * @desc:	CMD_LCD_PUTHEX (0x75)
 * 			Prints the given value in hexadecimal format
 * 		EX.
 * 			lcd_puthex(0x5A);	// will print "0x5A " on LCDTERM
 */
void lcd_Puthex(uint8_t val)
{
	sendByte(CMD_LCD_PUTHEX);
	sendByte(val);
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_CLEAR_SW_BUFFER (0x60)
 * 			Clears LCDTERM's button queue.
 */
void lcd_ClearButtons(void)
{
	sendByte(CMD_CLEAR_SW_BUFFER);
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_PORTRAIT_MODE (0x34)
 * 			Puts LCDTERM into portrait mode.
 * 			Buttons are at the bottom of the screen.
 */
void lcd_PortraitMode(void)
{
	sendByte(CMD_PORTRAIT_MODE);
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_LANDSCAPE_MODE (0x35)
 * 		    Puts LCDTERM into landscape mode.
 * 			Buttons are to the right of the screen.
 */
void lcd_LandscapeMode(void)
{
	sendByte(CMD_LANDSCAPE_MODE);
	//delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_FONT_ERASE (0x45)
 * 			Erases all fonts on LCDTERM.
 *
 * 	  NOTE: After this command is sent
 * 	  		you will not be able to print
 * 	  		characters on the screen until
 * 	  		downloading another font pack.
 */
void lcd_EraseFonts(void)
{
	sendByte(CMD_FONT_ERASE);
	delay(15);
}

/*
 * @param:	void
 * @return:	void
 * @desc:	CMD_ERASE_IMAGE_SPACE (0x83)
 * 			Erases all images on LCDTERM.
 *
 * 	  NOTE: After this command is sent
 * 	  		there will be no images stored
 * 	  		on LCDTERM.
 */
void lcd_EraseImages(void)
{
	sendByte(CMD_ERASE_IMAGE_SPACE);
	delay(15);
}

/*
 * @param:	returnVal	- an array of equal or larger size of 'number'
 * 			number		- the number of button presses to read.
 * @return:	void
 * @desc:	CMD_READ_KEY_PRESS_BUFFER (0x61)
 *			Stores the responses into the given array (returnVal).
 *			0x01 - button 1
 *			0x02 - button 2
 *			0x03 - button 3
 *			0xF7 - no button has been pressed
 *		EX.
 *			uin8_t buttonPresses[4] = {0};
 *			lcd_getNbuttons( buttonPresses, 4 );
 *
 *			possible result:
 *			buttonPresses[0]: 0x01
 *			buttonPresses[1]: 0x02
 *			buttonPresses[2]: 0xF7
 *			buttonPresses[3]: 0xF7
 */
void lcd_getNbuttons(uint8_t *returnVal, uint8_t number)
{
	sendByte(CMD_READ_KEY_PRESS_BUFFER);
	for(int k = 0; k < number; ++k)
  {
		sendByte(DMYBYTE);
		returnVal[k] = getByte();
	}
	delay(15);
}

/*
 * @param:	xs	- x start location
 * 			xe	- x end location
 * 			ys	- y start location
 * 			ye	- y end location
 * @return:	void
 * @desc:	CMD_WR_LCD_BLOCK_PARAMS (0x70)
 * 			This will setup a rectangle to be filled in with raw data.
 * 			lcd_fillRect() is expected after this function is called.
 * 		EX.
 * 			(0,0)			 (25, 0)
 * 			__________________
 * 			|				 |
 * 			|				 |
 * 			|				 |
 * 			|				 |
 * 			|				 |
 * 			|________________|
 * 			(0, 20)          (25, 20)
 *
 * 			lcd_writeBlockParams( 0, 25, 0, 20 );
 */
void lcd_writeBlockParams(uint8_t xs, uint8_t xe, uint8_t ys, uint8_t ye )
{
  sendByte(CMD_WR_LCD_BLOCK_PARAMS);
	sendByte(xs);
	sendByte(xe);
	sendByte(ys);
	sendByte(ye);
	delay(15);
}

/*
 * @param:	size	- number of bytes in imgData
 * 			imgData	- raw data for the rectangle
 * @return: void
 * @desc:	CMD_FILL_RECTANGLE (0x71)
 * 			Each pixel requires two bytes of data in RGB565 format.
 * 			RRRRRGGG GGGBBBBB
 * 			Size should be 2 times the area setup by the
 * 			lcd_writeBlockParams() function.
 * 		EX.
 * 			Assume a rectangle with width 25 pixels and a height of 20 pixels.
 * 			size = 1000 = 25 * 20 * 2
 * 			imgData should be 1000 bytes of data.
 * 	  NOTE: The if the incorrect number of bytes is sent all bytes received by
 * 	  		LCDTERM will be taken as raw data for the image.
 */
void lcd_fillRect(uint32_t size_l, uint8_t *imgData)
{
	sendByte( CMD_FILL_RECTANGLE );
	sendByte( size_l >> 24 );			// MSB
	sendByte( (size_l >> 16) & 0xff );
	sendByte( (size_l >> 8) & 0xff );
	sendByte( size_l & 0xff );
	for(uint32_t k = 0; k < size_l; ++k)
  {
		sendByte( imgData[k] );
  }
	delay(15);
}

void lcd_printTitles(void)
{
  lcd_SetCursor(POS_TITLE_NE_X, POS_TITLE_NE_Y);
  snprintf(msg, MAX_MSG_SIZE, "XY:");
  lcd_Puts(msg);

  lcd_SetCursor(POS_TITLE_WA_X, POS_TITLE_WA_Y);
  snprintf(msg, MAX_MSG_SIZE, "WA:");
  lcd_Puts(msg);

  lcd_SetCursor(POS_TITLE_WB_X, POS_TITLE_WB_Y);
  snprintf(msg, MAX_MSG_SIZE, "WB:");
  lcd_Puts(msg);

  lcd_SetCursor(POS_TITLE_HEAD_X, POS_TITLE_HEAD_Y);
  snprintf(msg, MAX_MSG_SIZE, "Head:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_XTE_X, POS_TITLE_XTE_Y);
  snprintf(msg, MAX_MSG_SIZE, "XTE:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_PSI_X, POS_TITLE_PSI_Y);
  snprintf(msg, MAX_MSG_SIZE, "PSI:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_DELTA_X, POS_TITLE_DELTA_Y);
  snprintf(msg, MAX_MSG_SIZE, "Delta:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_U_X, POS_TITLE_U_Y);
  snprintf(msg, MAX_MSG_SIZE, "U:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_SPEED_X, POS_TITLE_SPEED_Y);
  snprintf(msg, MAX_MSG_SIZE, "Speed:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_FIX_X, POS_TITLE_FIX_Y);
  snprintf(msg, MAX_MSG_SIZE, "GPS:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_CAN_X, POS_TITLE_CAN_Y);
  snprintf(msg, MAX_MSG_SIZE, "CAN:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_MODE_X, POS_TITLE_MODE_Y);
  snprintf(msg, MAX_MSG_SIZE, "Mode:");
  lcd_Puts(msg);
  lcd_SetCursor(POS_TITLE_TIME_X, POS_TITLE_TIME_Y);
  snprintf(msg, MAX_MSG_SIZE, "Time:");
  lcd_Puts(msg);
}

void lcd_printNED(int32_t north, int32_t east)
{
  lcd_SetCursor(POS_NORTH_X, POS_NORTH_Y);
  snprintf(msg, MAX_MSG_SIZE, "%+-14ld", north);
  lcd_Puts(msg);

  lcd_SetCursor(POS_EAST_X, POS_EAST_Y);
  snprintf(msg, MAX_MSG_SIZE, "%+-14ld", east);
  lcd_Puts(msg);
}

void lcd_printWaypoints(int32_t wax, int32_t way, int32_t wbx, int32_t wby)
{
  int32_t whole;
  int32_t frac;

  thousandthsToWhole(wax, &whole, &frac);
  snprintf(msg, MAX_MSG_LEN, "%+-d"".""%d  ", whole, frac);
  lcd_SetCursor(POS_WAX_X, POS_WAX_Y);
  lcd_Puts(msg);

  thousandthsToWhole(way, &whole, &frac);
  snprintf(msg, MAX_MSG_LEN, "%+-d"".""%d  ", whole, frac);
  lcd_SetCursor(POS_WAY_X, POS_WAY_Y);
  lcd_Puts(msg);

  thousandthsToWhole(wbx, &whole, &frac);
  snprintf(msg, MAX_MSG_LEN, "%+-d"".""%d  ", whole, frac);
  lcd_SetCursor(POS_WBX_X, POS_WBX_Y);
  lcd_Puts(msg);

  thousandthsToWhole(wby, &whole, &frac);
  snprintf(msg, MAX_MSG_LEN, "%+-d"".""%d  ", whole, frac);
  lcd_SetCursor(POS_WBY_X, POS_WBY_Y);
  lcd_Puts(msg);
}

void lcd_printHeading(uint32_t heading)
{
  lcd_SetCursor(POS_HEAD_X, POS_HEAD_Y);
  snprintf(msg, MAX_MSG_SIZE, "%-+4d", heading);
  lcd_Puts(msg);
}

void lcd_printXTE(int32_t xte)
{
  lcd_SetCursor(POS_XTE_X, POS_XTE_Y);
  snprintf(msg, MAX_MSG_SIZE, "%-+7d", xte);
  lcd_Puts(msg);
}

void lcd_printPSI(int32_t psi)
{
  lcd_SetCursor(POS_PSI_X, POS_PSI_Y);
  snprintf(msg, MAX_MSG_SIZE, "%-+7d", psi);
  lcd_Puts(msg);
}

void lcd_printDELTA(int32_t delta)
{
  lcd_SetCursor(POS_DELTA_X, POS_DELTA_Y);
  snprintf(msg, MAX_MSG_SIZE, "%-+7d", delta);
  lcd_Puts(msg);
}

void lcd_printFix(uint32_t fix)
{
  lcd_SetCursor(POS_FIX_X, POS_FIX_Y);

  if(fix)
  {
    lcd_SetForegroundColor(0, 255, 0);
    snprintf(msg, MAX_MSG_SIZE, "OK");
  }
  else
  {
    lcd_SetForegroundColor(255, 0, 0);
    snprintf(msg, MAX_MSG_SIZE, "WAIT");
  }
  lcd_Puts(msg);
  lcd_SetForegroundColor(255, 255, 255);
}

void lcd_printU(int32_t u)
{
  lcd_SetCursor(POS_U_X, POS_U_Y);
  snprintf(msg, MAX_MSG_SIZE, "%-+4d", u);
  lcd_Puts(msg);
}

void lcd_printCAN(uint32_t can)
{
  lcd_SetCursor(POS_CAN_X, POS_CAN_Y);

  if(can)
  {
    lcd_SetForegroundColor(0, 255, 0);
    snprintf(msg, MAX_MSG_SIZE, "OK");
  }
  else
  {
    lcd_SetForegroundColor(255, 0, 0);
    snprintf(msg, MAX_MSG_SIZE, "ERR");
  }
  lcd_Puts(msg);
  lcd_SetForegroundColor(255, 255, 255);
}

void lcd_printTime(uint32_t time)
{
  lcd_SetCursor(POS_TIME_X, POS_TIME_Y);
  snprintf(msg, MAX_MSG_SIZE, "%05ld", time);
  lcd_Puts(msg);
}

void lcd_print_background(void)
{
  str_i = 0;

  lcd_move_cursor(POS_TITLE_NE_X, POS_TITLE_NE_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "XY:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_WA_X, POS_TITLE_WA_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "WA:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_WB_X, POS_TITLE_WB_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "WB:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_HEAD_X, POS_TITLE_HEAD_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "Head:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_XTE_X, POS_TITLE_XTE_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "XTE:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_PSI_X, POS_TITLE_PSI_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "PSI:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_DELTA_X, POS_TITLE_DELTA_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "Delta:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_U_X, POS_TITLE_U_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "U:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_SPEED_X, POS_TITLE_SPEED_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "Speed:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_FIX_X, POS_TITLE_FIX_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "GPS:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_CAN_X, POS_TITLE_CAN_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "CAN:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_MODE_X, POS_TITLE_MODE_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "Mode:");
  lcd_put_var();
  lcd_move_cursor(POS_TITLE_TIME_X, POS_TITLE_TIME_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "Time:");
  lcd_put_var();

  // Finally done formatting all data
  // Start the DMA stream and let it roll hands-free
  USART1_Stream(&msg[0], str_i);

}

/*
  This function creates a giant string which manifests as
  a single stream of LCD commands, placing updated data in
  all the right places. Emulate all these function calls
  by a single string sent to USART1_Stream(msg);
  lcd_printNED(1, 10000012);
  lcd_printWaypoints(-123, 323, 3243454, -13454);
  lcd_printHeading(122);
  lcd_printXTE(-12);
  lcd_printPSI(-13);
  lcd_printDELTA(54);
  lcd_printFix(0);
  lcd_printU(-12);
  lcd_printCAN(1);
  lcd_printTime(540);
*/
void lcd_update(Rover_t *rover)
{
  str_i = 0;
  int32_t whole;
  int32_t frac;
  uint32_t uWhole;
  uint32_t uFrac;

  // EASTING (X) Data
  lcd_move_cursor(POS_EAST_X, POS_EAST_Y);
  thousandthsToWhole(rover->pos.X, &whole, &frac);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-ld"".""%ld   ", whole, frac/10);
  lcd_put_var();

  // Northing (Y) Data
  lcd_move_cursor(POS_NORTH_X, POS_NORTH_Y);
  thousandthsToWhole(rover->pos.Y, &whole, &frac);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-ld"".""%ld   ", whole, frac/10);
  lcd_put_var();

  if(firstRun)
  {
    // Waypoint A Data
    lcd_move_cursor(POS_WAX_X, POS_WAX_Y);
    thousandthsToWhole(rover->line.A.X, &whole, &frac);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-ld"".""%01ld   ", whole, frac/10);
    lcd_put_var();
    lcd_move_cursor(POS_WAY_X, POS_WAY_Y);
    thousandthsToWhole(rover->line.A.Y, &whole, &frac);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-ld"".""%01ld   ", whole, frac/10);
    lcd_put_var();

    // Waypoint B Data
    lcd_move_cursor(POS_WBX_X, POS_WBX_Y);
    thousandthsToWhole(rover->line.B.X, &whole, &frac);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-ld"".""%01ld   ", whole, frac/10);
    lcd_put_var();
    lcd_move_cursor(POS_WBY_X, POS_WBY_Y);
    thousandthsToWhole(rover->line.B.Y, &whole, &frac);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-ld"".""%01ld  ", whole, frac/10);
    lcd_put_var();
    firstRun = 0;
  }
  // Heading Data
  lcd_move_cursor(POS_HEAD_X, POS_HEAD_Y);
  //hundredthsToWhole(rover->heading, &uWhole, &uFrac);
  //str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-ld"".""01%ld   ", uWhole, uFrac);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-1.1f   ", rover->trackHeading);

  lcd_put_var();

  // Cross Track Error Data
  lcd_move_cursor(POS_XTE_X, POS_XTE_Y);
  float tempXTE = rover->xte / 1000.0f;
  //thousandthsToWhole(rover->xte, &whole, &frac);
  //tempXTEstr_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-d"".""%02d   ", whole, frac/10);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-1.2f  ", tempXTE);
  lcd_put_var();

  // PSI Data
  lcd_move_cursor(POS_PSI_X, POS_PSI_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-1.2f   ", rover->psi);
  lcd_put_var();

  // DELTA Data
  lcd_move_cursor(POS_DELTA_X, POS_DELTA_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-1.1f ", rover->delta);
  lcd_put_var();

  // Setup Fix data and print it
  lcd_move_cursor(POS_FIX_X, POS_FIX_Y);
  if(rover->fix)
  {
    // Font color to green, and print "OK"
    lcd_set_font_color(0, 255, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "OK-- ");
  }
  else
  {
    // Font to red, and print "WAIT"
    lcd_set_font_color(255, 0, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "WAIT");
  }
  lcd_put_var();

  // Font back to white
  lcd_set_font_color(255, 255, 255);

  // U Data
  lcd_move_cursor(POS_U_X, POS_U_Y);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-1.1f  ", rover->u);
  lcd_put_var();

  // Speed Data
  lcd_move_cursor(POS_SPEED_X, POS_SPEED_Y);
  thousandthsToWhole(rover->speed, &whole, &frac);
  //str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-d  ", rover->speed);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%+-d"".""%d   ", whole, frac/100);
  lcd_put_var();

  // CAN driver state
  lcd_move_cursor(POS_CAN_X, POS_CAN_Y);
  if(rover->can)
  {
    // Font color to green, and print "OK"
    lcd_set_font_color(0, 255, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "OK--");
  }
  else
  {
    // Font color to red, and print "WAIT"
    lcd_set_font_color(255, 0, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "ERR");
  }
  lcd_put_var();

  // Font color back to white
  lcd_set_font_color(255, 255, 255);

  // Time Data
  lcd_move_cursor(POS_TIME_X, POS_TIME_Y);
  tenthsToWhole(rover->time, &uWhole, &uFrac);
  str_l = snprintf(&rj[0], MAX_MSG_LEN, "%lu"".""%lu", uWhole, uFrac);
  lcd_put_var();

  // Mode Data
  lcd_move_cursor(POS_MODE_X, POS_MODE_Y);
  if(rover->mode == INIT)
  {
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "INIT   ");
  }
  else if(rover->mode == MAN_RDY)
  {
    //lcd_set_font_color(255, 0, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "MAN  ");
  }
  else if(rover->mode == MAN_RUN)
  {
    lcd_set_font_color(0, 255, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "MAN  ");
  }
  else if(rover->mode == AUTO_RDY)
  {
    //lcd_set_font_color(255, 0, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "AUTO ");
  }
  else if(rover->mode == AUTO_RUN)
  {
    lcd_set_font_color(0, 255, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "AUTO ");
  }
  else if(rover->mode == ESTOP)
  {
    //lcd_set_font_color(255, 0, 0);
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "STOP ");
  }
  else
  {
    str_l = snprintf(&rj[0], MAX_MSG_LEN, "       ");
  }
  lcd_put_var();
  lcd_set_font_color(255, 255, 255);

  // Finally done formatting all data
  // Start the DMA stream and let it roll hands-free
  USART1_Stream(&msg[0], str_i);
}

// Abstract-away some common formatting tasks
void thousandthsToWhole(int32_t input, int32_t *whole, int32_t *frac)
{
  int32_t temp_frac;

  *whole = input / 1000;
  temp_frac = input - (*whole * 1000);

  if(temp_frac < 0)
  {
    temp_frac *= -1;
  }

  *frac = temp_frac;
}

void hundredthsToWhole(uint16_t input, uint32_t *whole, uint32_t *frac)
{
  *whole = input / 100;
  *frac = input - (*whole * 100);
}

void uThousandthsToWhole(uint32_t input, uint32_t *whole, uint32_t *frac)
{
  *whole = input / 1000;
  *frac = input - (*whole * 1000);
}

void tenthsToWhole(uint32_t input, uint32_t *whole, uint32_t *frac)
{
  *whole = input / 10;
  *frac = input - (*whole * 10);
}

void lcd_put_var(void)
{
  msg[str_i] = CMD_LCD_PUTS; str_i++;
  msg[str_i] = str_l; str_i++;
  str_i += snprintf(msg + str_i, MAX_MSG_LEN, "%s", rj);
}

void lcd_move_cursor(uint8_t x, uint8_t y)
{
  msg[str_i] = CMD_WR_LCD_CURSOR_LOCATION; str_i++;
  msg[str_i] = x; str_i++;
  msg[str_i] = y; str_i++;
}

void lcd_set_font_color(uint8_t r, uint8_t g, uint8_t b)
{
  msg[str_i] = CMD_SET_FOREGROUND_COLOR; str_i++;
  msg[str_i] = r; str_i++;  // Red
  msg[str_i] = g; str_i++;  // Green
  msg[str_i] = b; str_i++;  // Blue
}
