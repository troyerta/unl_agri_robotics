#include "utils.h"
#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "usart.h"
#include "can.h"
#include "string.h"
#include "message.h"

extern CanRxMsg RxMessage;
extern CanTxMsg TxMessage;

//static CanRxMsg rx_fifo[CAN_FIFO_LEN];
//static uint16_t rx_head;
//static uint16_t rx_tail;

// Circular Buffers for each message ID we expect through the filters
extern CanRxMsg gps_msgs[CAN_FIFO_LEN];
extern MsgBuf_t gps_buf;
extern CanRxMsg heading_msgs[CAN_FIFO_LEN];
extern MsgBuf_t heading_buf;
extern CanRxMsg steer_msgs[CAN_FIFO_LEN];
extern MsgBuf_t steer_buf;

uint8_t CAN1_Config(uint32_t *filterIDs)
{
  CAN_InitTypeDef       CAN_InitStructure;
  CAN_FilterInitTypeDef CAN_FilterInitStructure;
  GPIO_InitTypeDef      GPIO_InitStructure;
	uint8_t status        = 0;
  uint32_t i            = 0;

  /* CAN GPIOs configuration **************************************************/

  /* Enable GPIO clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOB, ENABLE);

  /* Connect CAN pins */
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1); // RX or use PA11
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1); // TX or use PA12

  /* Configure CAN RX and TX pins */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* CAN configuration ********************************************************/
  /* Enable CAN clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

  /* CAN register init */
  CAN_DeInit(CAN1);
  CAN_StructInit(&CAN_InitStructure);

  /* CAN cell init */
  CAN_InitStructure.CAN_TTCM = DISABLE;         // Time-triggered communication mode, (time stamps added to last two bytes of data)
  CAN_InitStructure.CAN_ABOM = DISABLE;         // Automatic Bus-off management
  CAN_InitStructure.CAN_AWUM = DISABLE;         // Automatic wakeup mode
  CAN_InitStructure.CAN_NART = ENABLE;         // Non-automatic retransimssion mode
  CAN_InitStructure.CAN_RFLM = DISABLE;         // Feature to optionally lock the FIFO contents from being overwritten by new msgs if FIFO is full
  CAN_InitStructure.CAN_TXFP = DISABLE;         // Tx mailbox contents by priority, not FIFO
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal; // Apart from loopback and silent modes
  CAN_InitStructure.CAN_SJW = CAN_SJW_2tq;      // How many time quanta to adjust by for synchronization? Max 2 - why? Because.

  /*
   * How to calculate this shit:
   * BF = (APB1_Clk/Desired_Baudrate);
   * # of Quanta per bit = TQ = BF/Prescalar;
   * Given that TQ = (1 + BS1 + BS2),
   * Choose BS1 and BS2 such that
   * BS1/(BS1+BS2) ~ 72%
   * for
   * BS1 = 1,....,16
   * BS2 = 1,....,8
   *
   * So get your APB1_clk rate from the system
   * and calculate BF using your desired baudrate.
   * Then pick a prescalar such that 8 < TQ < 25
   * Finally, set BS1 ~= 0.72 * TQ
   * and BS2 = (TQ - 1) - BS1
   * You're done! Baudrate is set, sampling point is near optimal
   */

  /* CAN Baudrate = 500kBps (CAN clocked at 45 MHz) */
//  CAN_InitStructure.CAN_BS1 = CAN_BS1_10tq;      // 10tq / (15tq - 1tq) = 71.4%
//  CAN_InitStructure.CAN_BS2 = CAN_BS2_4tq;       // 1tq + 10tq + 4tq = 15tq
//  CAN_InitStructure.CAN_Prescaler = 6;           // (45MHz / 500,000Baud) / 6 = 15tq per bit
//  CAN_Init(CAN2, &CAN_InitStructure);

  /* CAN Baudrate = 250kBps (CAN clocked at 45 MHz) */
  CAN_InitStructure.CAN_BS1 = CAN_BS1_10tq;
  CAN_InitStructure.CAN_BS2 = CAN_BS2_4tq;
  CAN_InitStructure.CAN_Prescaler = 12;
  status = CAN_Init(CAN1, &CAN_InitStructure);

  /* CAN filter init */
  i = 0;
  while(filterIDs[i])
  {
    CAN_FilterInitStructure.CAN_FilterNumber = i;
    CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
    CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh = (filterIDs[i]) << 5;
    CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
    CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
    CAN_FilterInit(&CAN_FilterInitStructure);
    i++;
  }

  // Enable the Rx interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  //rx_head = 0;
  //rx_tail = 0;
  //memset(&rx_fifo[0], 0, CAN_FIFO_LEN);

  /* Enable FIFO 0 message pending Interrupt */
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

	return status;
}

/* Return 1 if true, 0 otherwise. */
uint8_t can_rx_fifo_empty(MsgBuf_t *buffer)
{
  if (buffer->head == buffer->tail)
  {
    return 1;
  }
  return 0;
}

/* Return 1 if true, 0 otherwise. */
uint8_t can_rx_fifo_full(MsgBuf_t *buffer)
{
  if(((buffer->tail+1)%CAN_FIFO_LEN) == buffer->head)
  {
    return 1;
  }
  return 0;
}

/*
 * Append a message to our CAN message fifo.
 * Returns 1 if msg successfully appended to fifo.
 * Returns 0 if fifo is full.
 */
uint8_t can_rx_fifo_write(MsgBuf_t *buffer, CanRxMsg msg)
{
  if(can_rx_fifo_full(buffer))
  {
    return 0;
  }
  buffer->buffer[buffer->tail] = msg;
  buffer->tail = (buffer->tail+1) % CAN_FIFO_LEN;
  return 1;
}

/*
 * Read 1 message from fifo.
 * Returns 0 if fifo is empty, otherwise 1.
 */
uint8_t can_fifo_read(CanRxMsg *msg, MsgBuf_t *buffer)
{
  if(can_rx_fifo_empty(buffer))
  {
    return 0;
  }
  *msg = buffer->buffer[buffer->head];
  buffer->head = (buffer->head+1) % CAN_FIFO_LEN;
  return 1;
}

uint8_t can_rx_fifo_size(MsgBuf_t *buffer)
{
  uint8_t value;
  uint16_t head;
  uint16_t tail;

  CAN_ITConfig(CAN1, CAN_IT_FMP0, DISABLE);
  head = buffer->head;
  tail = buffer->tail;
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

  if(tail >= head)
  {
    value = tail - head;
  }
  else
  {
    value = CAN_FIFO_LEN + tail - head;
  }

  return value;
}

void can_buffer_flush(MsgBuf_t *buffer)
{
  buffer->head = 0;
  buffer->tail = 0;
}

void can_TxMsg_init(CanTxMsg *msg, uint32_t StdId, uint32_t ExtId, uint8_t IDE, uint8_t RTR, uint8_t DLC, uint8_t Data)
{
  uint8_t i = 0;

  msg->IDE = IDE;

  if(IDE == CAN_Id_Standard)
  {
    msg->StdId = StdId;
    msg->ExtId = 0;
  }
  else
  {
    msg->StdId = 0;
    msg->ExtId = ExtId;
  }

  msg->RTR = RTR;

  msg->DLC = DLC;

  for(i = 0; i < 8; i++)
  {
    msg->Data[i] = Data;
  }
}

void can_RxMsg_init(CanRxMsg *msg, uint32_t StdId, uint32_t ExtId, uint8_t IDE, uint8_t RTR, uint8_t DLC, uint8_t Data)
{
  uint8_t i = 0;

  msg->IDE = IDE;

  if(IDE == CAN_Id_Standard)
  {
    msg->StdId = StdId;
    msg->ExtId = 0;
  }
  else
  {
    msg->StdId = 0;
    msg->ExtId = ExtId;
  }

  msg->RTR = RTR;

  msg->DLC = DLC;

  for(i = 0; i < 8; i++)
  {
    msg->Data[i] = Data;
  }
}




