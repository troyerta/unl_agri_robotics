/*********************************************************************
* Includes
*********************************************************************/
#include "utils.h"
#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "arm_math.h"
#include "can.h"
#include "kalman.h"
#include "core_cm4.h"

#pragma GCC diagnostic ignored "-Wunused-variable"

/*********************************************************************
* Defines
*********************************************************************/
// SS Control SYstems with have P inputs, Q outputs, and N state variables
#define P 1   // System Inputs
#define Q 1   // System Outputs
#define N 3   // Number of states
#define M 3   // Number of observed/measured states

/*********************************************************************
* Variables
*********************************************************************/
// Syntax for making a matrix is this:
// MAT = {numRows, numCols, pData};

/* State Transition Matrix */
static float32_t AData[N*N] = { 0.9852f, 0.1765f, 0.1218f
                        -0.0071f, 0.9887f, 0.0673f
                        -0.1683f,-0.2737f, 0.6920f };
static arm_matrix_instance_f32 A = { N, N, &AData[0] };

/* Input Matrix */
static float32_t BData[N*P] = { 0.0148f,
                         0.0071f,
                         0.1683f };
static arm_matrix_instance_f32 B = { N, P, &BData[0] };

/* Output Matrix */
static float32_t CData[Q*N] = { 1.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 C = { Q, N, &CData[0] };

/* Feedforward Matrix */
static float32_t DData[Q*P] = { 0.0f };
static arm_matrix_instance_f32 D = { Q, P, &DData[0] };

/* State Vector */
static float32_t xData[N*1] = { 0.0f,
                         0.0f,
                         0.0f };
static arm_matrix_instance_f32 x = { N, 1, &xData[0] };

/* Input Vector */
static float32_t uData[N*1] = { 0.0f };
static arm_matrix_instance_f32 u = { P, 1, &uData[0] };

/* Remember Matrix Algebra Resource rules here:
 * if A(3x3)   and   B(3x1) are to be multiplied,
 *      ^ ^            ^ ^
 *      | |            | |
 *      | |_must match_| |
 *      |__result size___|
*/





// Allocate some resources for a linear Kalmann estimator

/* External Input Vector */
static float32_t zkIC[M*1] = { 0.0f,
                        0.0f,
                        0.0f };
static arm_matrix_instance_f32 zk = { M, 1, &zkIC[0] };

/* State Estimate */
static float32_t xkIC[N*1]  = { 0.0f,
                         0.0f,
                         0.0f };
static arm_matrix_instance_f32 xk = { N, 1, &xkIC[0] };

/* Previous State */
static float32_t xkPrevData[N*1] = { 0.0f,
                              0.0f,
                              0.0f };
static arm_matrix_instance_f32 xkPrev = { N, 1, &xkPrevData[0] };

/* State to Measurement Matrix */
static float32_t HData[M*N] = { 1.0f, 0.0f, 0.0f,
                         0.0f, 1.0f, 0.0f,
                         0.0f, 0.0f, 1.0f };
static arm_matrix_instance_f32 H = { M, N, &HData[0] };

/* Model Covariance Matrix */
static float32_t QData[N*N] = { 2.0f, 0.0f, 0.0f,
                         0.0f, 2.0f, 0.0f,
                         0.0f, 0.0f, 2.0f };
static arm_matrix_instance_f32 QCov = { N, N, &QData[0] };

/* Sensor Covariance Matrix */
static float32_t RData[M*M] = { 0.5f, 0.0f, 0.0f,
                         0.0f, 0.5f, 0.0f,
                         0.0f, 0.0f, 0.5f };
static arm_matrix_instance_f32 R = { M, M, &RData[0] };

/* Previous State Estimate */
static float32_t xkPredData[N*1] = { 0.0f,
                              0.0f,
                              0.0f };
static arm_matrix_instance_f32 xkPred = { N, 1, &xkPredData[0] };

/* Previous Covariance Estimate */
static float32_t PkPredData[N*N] = { 0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 PkPred = { N, N, &PkPredData[0] };

/* Previous Covariance Estimate */
static float32_t PkPrevData[N*N] = { 0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 PkPrev = { N, N, &PkPrevData[0] };

/* Next Covariance Estimate */
static float32_t PkData[N*N]     = { 0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 Pk = { N, N, &PkData[0] };

/* Kalman Gain */
static float32_t KData[N*M]      = { 0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 K = { N, M, &KData[0] };

// Intermediate matrices for Prediction - covariance
static float32_t APkPrev_data[N*N]   = { 0.0f };
static float32_t APkPrevAT_data[N*N] = { 0.0f };
static float32_t AT_data[N*N]        = { 0.0f };
static arm_matrix_instance_f32 APkPrev   = { N, N, &APkPrev_data[0] };
static arm_matrix_instance_f32 APkPrevAT = { N, N, &APkPrevAT_data[0] };
static arm_matrix_instance_f32 AT        = { N, N, &AT_data[0] };

// Intermediate matrices for kalman gain computation
static float32_t HT_data[N*M]    = { 0.0f };
static float32_t PkHT_data[M*N]  = { 0.0f };
static float32_t HPk_data[M*M]   = { 0.0f };
static float32_t HPkHT_data[M*M] = { 0.0f };
static float32_t L_data[M*M]     = { 0.0f };
static float32_t INVL_data[M*M]  = { 0.0f };
static arm_matrix_instance_f32 HT    = { N, M, &HT_data[0] };
static arm_matrix_instance_f32 PkHT  = { M, N, &PkHT_data[0] };
static arm_matrix_instance_f32 HPk   = { M, M, &HPk_data[0] };
static arm_matrix_instance_f32 HPkHT = { M, M, &HPkHT_data[0] };
static arm_matrix_instance_f32 L     = { M, M, &L_data[0] };
static arm_matrix_instance_f32 INVL  = { M, M, &INVL_data[0] };

// Intermediate matrices for estimation computation
static float32_t HPkPred_data[M*1]    = { 0.0f };
static float32_t zk_HPkPred_data[M*1] = { 0.0f };
static float32_t xk_delta_data[N*1]   = { 0.0f };
static arm_matrix_instance_f32 HPkPred    = { M, 1, &HPkPred_data[0] };
static arm_matrix_instance_f32 zk_HPkPred = { M, 1, &zk_HPkPred_data[0] };
static arm_matrix_instance_f32 xk_delta   = { N, 1, &xk_delta_data[0] };

// Intermediate matrices for Error Covariance Computation
static float32_t KH_data[N*N]     = { 0.0f };
static float32_t KHPk_data[N*N]   = { 0.0f };
static arm_matrix_instance_f32 KH     = { N, N, &KH_data[0] };
static arm_matrix_instance_f32 KHPk   = { N, N, &KHPk_data[0] };

/*********************************************************************
* Computation Structures for State Space Controller
*
* State Update:
* xd = A * x + B * u
*      |___|   |   |   - A[NxN] * x[Nx1] = Ax[Nx1]
*        |     |___|   - B[NxP] * u[Px1] = Bu[Nx1]
*        |_______|     - Ax[Nx1] + Bu[Nx1] = xd[Nx1]
*            |         - xd[Nx1]
*
* float32_t AxData[N*1];
* float32_t BuData[N*1];
*
* arm_matrix_instance Ax = { N, 1, &AxData[0] };
* arm_matrix_instance Bu = { N, 1, &BuData[0] };
*
* status = arm_mat_mult_f32(&A, &x, &Ax);
* status = arm_mat_mult_f32(&B, &u, &Bu);
* status = arm_mat_add_f32(&Ax, &Bu, &xd);
*
* Output Update:
* y = C * x + D * u
*     |___|   |   |   - C[QxN] * x[Nx1] = Cx[Qx1]
*       |     |___|   - D[QxP] * u[Px1] = Du[Qx1]
*       |_______|     - Cx[Qx1] + Du[Qx1] = y[Qx1]
*           |         - y[Qx1]
*
* float32_t CxData[Q*1];
* float32_t DuData[Q*1];
* arm_matrix_instance Cx = { Q, 1, &CxData[0] };
* arm_matrix_instance Du = { Q, 1, &DuData[0] };
*
* status = arm_mat_mult_f32(&C, &x, &Cx);
* status = arm_mat_mult_f32(&D, &u, &Du);
* status = arm_mat_add_f32(&Cx, &Du, &y);
*
*********************************************************************/


/*********************************************************************
* Computation Structures for Kalman Filter
*
* State Prediction:
* xkPred = A * xkPrev   <-- No intermediate structure needed A(NxN) * xkPrev(N*1) = xkPred(N*1), Augument this with control input if not regulator mode
*
* status = arm_mat_mult_f32(&A, &xkPrev, &xkPred);
*
*
* Error Covariance Prediction:
* Pk = ( A * PkPrev * A^T ) + Q
*        |______|      |      |   - A[NxN] * PkPrev[NxN]    = APkPrev[NxN]
*           |__________|      |   - APkPrev[NxN] * AT[NxN]  = APkPrevAT[NxN]
*                |____________|   - APkPrevAT[NxN] + Q[NxN] = Pk[NxN]
*                      |          - Pk[NxN]
*
* float32_t APkPrev[N*N];
* float32_t APkPrevAT[N*N];
* float32_t AT[N*N];
*
* status = arm_mat_trans_f32(&A, &AT);
* status = arm_mat_mult_f32(&A, &PkPrev, &APkPrev);
* status = arm_mat_mult_f32(&APkPrev, &AT, &APkPrevAT);
* status = arm_mat_add_f32(&APkPrevAT, &Q, &Pk);
*
* Kalman Gain Computation:
* K = Pk * H^T * INV( H * Pk * H^T + R )
*      |___|      |   |   |    |     |   - Pk[NxN] * HT[NxM]     = PkHT[NxM]
*        |        |   |___|    |     |   - H[MxN] * Pk[NxN]      = HPk[MxN]
*        |        |     |______|     |   - HPk[MxN] * HT[NxM]    = HPkHT[MxM]
*        |        |        |_________|   - HPkHT[MxM] + R[MxM]   = L[MxM]
*        |        |_____________|        - INV(L[MxM])           = INVL[MxM]
*        |_______________|               - PkHT[NxM] * INVL[MxM] = K[NxM]
*                |                       - K[NxM]
*
* float32_t PkHT[M*N];
* float32_t HPk[M*M];
* float32_t HT[N*M]; // H is MxN, yes?
* float32_t HPkHT[M*M];
* float32_t L[M*M];
* float32_t INVL[M*M];
*
* status = arm_mat_trans_f32(&H, &HT);
* status = arm_mat_mult_f32(&Pk, &HT, &PkHT);
* status = arm_mat_mult_f32(&H, &Pk, &HPk);
* status = arm_mat_mult_f32(&HPk, &HT, &HPkHT);
* status = arm_mat_add_f32(&HPkHT, &R, &L);
* status = arm_mat_inverse_f32(&L, &INVL);
* status = arm_mat_mult_f32(&PkHT, &INVL, &K);
*
* Estimate Computation:
* xk = xkPred + K * ( zk - H * xkPred )
*        |      |     |    |______|      - H[MxN] * xkPred[Nx1]        = HPkPred[Mx1]
*        |      |     |_______|          - zk[Mx1] - HPkPred[Mx1]      = zk_HPkPred[Mx1]
*        |      |_________|              - K[NxM] * zk_HPkPred[Mx1]    = xk_delta[Nx1]
*        |___________|                   - xkPred[Nx1] + xk_delta[Nx1] = xk[Nx1]
*              |                         - xk[Nx1]
*
* float32_t HPkPred[M*1];
* float32_t zk_HPkPred[M*1];
* float32_t xk_delta[N*1];
*
* status = arm_mat_mult_f32(&H, &xkPred, &HPkPred);
* status = arm_mat_sub_f32(&zk, &HPkPred, &zk_HPkPred);
* status = arm_mat_mult_f32(&K, &zk_HPkPred, &xk_delta);
* status = arm_mat_add_f32(&xkPred, &xk_delta, &xk);
*
* Error Covariance Computation:
* PkPrev = Pk - K * H * Pk
*           |   |___|   |                - K[NxM] * H[MxN] = KH[NxN]
*           |     |_____|                - KH[NxN] * Pk[NxN] = KHPk[NxN]
*           |________|                   - Pk[NxN] - KHPk[NxN] = PkPrev[NxN]
*               |                        - PkPrev[NxN]
*
* float32_t KH[N*N];
* float32_t KHPk[N*N];
* float32_t PkPrev[N*N];
*
* status = arm_mat_mult_f32(&K, &H, &KH);
* status = arm_mat_mult_f32(&KH, &Pk, &KHPk);
* status = arm_mat_sub_f32(&Pk, &KHPk, &PkPrev);
*
**********************************************************************/
arm_status kalman_update(void)
{
  arm_status status;

  // Prediction - state
  status = arm_mat_mult_f32(&A, &xkPrev, &xkPred);

  // Prediction - covariance
  status += arm_mat_trans_f32(&A, &AT);
  status += arm_mat_mult_f32(&A, &PkPrev, &APkPrev);
  status += arm_mat_mult_f32(&APkPrev, &AT, &APkPrevAT);
  status += arm_mat_add_f32(&APkPrevAT, &QCov, &Pk);

  // Gain Computation
  status += arm_mat_trans_f32(&H, &HT);
  status += arm_mat_mult_f32(&Pk, &HT, &PkHT);
  status += arm_mat_mult_f32(&H, &Pk, &HPk);
  status += arm_mat_mult_f32(&HPk, &HT, &HPkHT);
  status += arm_mat_add_f32(&HPkHT, &R, &L);
  status += arm_mat_inverse_f32(&L, &INVL);
  status += arm_mat_mult_f32(&PkHT, &INVL, &K);

  // Estimate
  status += arm_mat_mult_f32(&H, &xkPred, &HPkPred);
  status += arm_mat_sub_f32(&zk, &HPkPred, &zk_HPkPred);
  status += arm_mat_mult_f32(&K, &zk_HPkPred, &xk_delta);
  status += arm_mat_add_f32(&xkPred, &xk_delta, &xk);

  // Error Covariance
  status += arm_mat_mult_f32(&K, &H, &KH);
  status += arm_mat_mult_f32(&KH, &Pk, &KHPk);
  status += arm_mat_sub_f32(&Pk, &KHPk, &PkPrev);

  return status;
}

