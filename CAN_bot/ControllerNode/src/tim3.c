/*********************************************************************
* Intro to TIM3

TIM3 is used as the TTCAN source for accurate uS timing delays

Configured in 1-shot mode
The OVR interrupt is what actually sends a CAN packet
In the case of the GPS Node, the CAN TXE interrupt is allowed to TX additional msgs
since it has many msgs to send

The one shot timer is triggered by the reception of the reference msg in the CAN RX interrupt

*********************************************************************/

/*********************************************************************
* Includes
*********************************************************************/
#include "utils.h"
#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "tim3.h"
#include "stream.h"

//static char dbg[64];
extern uint8_t quick_f;

/*********************************************************************
* Defines
*********************************************************************/
#define NODE_DELAY 1000

/*********************************************************************
* Module Variables
*********************************************************************/


/*********************************************************************
* Public Functions
*********************************************************************/
void TIM3_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	// TIM3 clock enable, TIM3 clocked by APB1*2 = 45 MHz * 2 = 90 MHz
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  // Time base configuration - need 1 mS one-shot timer
	TIM_TimeBaseStructure.TIM_Prescaler     = 900 - 1; // 90 MHz Clock down to 1 MHz (adjust per your clock)
  //TIM_TimeBaseStructure.TIM_Period        = 1000 - 1; // 1 MHz down to 0.001 Hz (1 ms), limited to 16 bits for TIM3
  TIM_TimeBaseStructure.TIM_Period        = 700 - 1;  // 7mS delay to start with
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode   = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	TIM3->CNT = 0;

  // TIM IT enable
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
  TIM_ClearITPendingBit(TIM3, TIM_IT_Update);

  // Enable the TIM3 interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  // One-shot mode
  TIM_SelectOnePulseMode(TIM3, TIM_OPMode_Single);

	// TIM3 is enabled during the reception of the reference msg
  // See the CAN RX interrupt handler in can.c
	//TIM_Cmd(TIM3, ENABLE);

}


