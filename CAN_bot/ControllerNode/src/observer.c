/*********************************************************************
* Includes
*********************************************************************/
#include "utils.h"
#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "arm_math.h"
#include "can.h"
#include "kalman.h"
#include "core_cm4.h"

/*********************************************************************
* Defines
*********************************************************************/

// SS Control SYstems with have P inputs, Q outputs, and N state variables
#define P 1   // System Inputs
#define Q 1   // System Outputs
#define N 3   // Number of states
#define M 3   // Number of observed/measured states

/*********************************************************************
* Local Variables
*********************************************************************/

/* State Transition Matrix */
static float32_t AData[N*N] = { 0.9852f, 0.1765f, 0.1218f
                        -0.0071f, 0.9887f, 0.0673f
                        -0.1683f,-0.2737f, 0.6920f };
static arm_matrix_instance_f32 A = { N, N, &AData[0] };

/* Input Matrix */
static float32_t BData[N*P] = { 0.0148f,
                         0.0071f,
                         0.1683f };
static arm_matrix_instance_f32 B = { N, P, &BData[0] };

/* Output Matrix */
static float32_t CData[Q*N] = { 1.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 C = { Q, N, &CData[0] };

/* Feedforward Matrix */
static float32_t DData[Q*P] = { 0.0f };
static arm_matrix_instance_f32 D = { Q, P, &DData[0] };

// Intermediate Structures for Observer Calculations
static float32_t AxData[N*1]      = { 0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 Ax = { N, 1, &AxData[0] };
static float32_t BuData[N*1]      = { 0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 Bu = { N, 1, &BuData[0] };
static float32_t CxData[Q*1]      = { 0.0f };
static arm_matrix_instance_f32 Cx = { Q, 1, &CxData[0] };
static float32_t DuData[Q*1]      = { 0.0f };
static arm_matrix_instance_f32 Du = { Q, 1, &DuData[0] };

/*********************************************************************
* Public Functions
*********************************************************************/
void observer_model_init(arm_matrix_instance_f32 *a, arm_matrix_instance_f32 *b, arm_matrix_instance_f32 *c, arm_matrix_instance_f32 *d)
{
  A = *a;
  B = *b;
  C = *c;
  D = *d;
}

arm_status observer_update(arm_matrix_instance_f32* x, arm_matrix_instance_f32* u, arm_matrix_instance_f32* xd, arm_matrix_instance_f32* y)
{
  arm_status status;

  // Update the state vector
  status = arm_mat_mult_f32(&A, x, &Ax);
  status += arm_mat_mult_f32(&B, u, &Bu);
  status += arm_mat_add_f32(&Ax, &Bu, xd);

  // Collect System Outputs
  status += arm_mat_mult_f32(&C, x, &Cx);
  status += arm_mat_mult_f32(&D, u, &Du);
  status += arm_mat_add_f32(&Cx, &Du, y);

  return status;
}
