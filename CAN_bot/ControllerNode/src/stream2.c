#include "stdint.h"
#include "stream.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "stm32f4xx_dma.h"

static char tmp_str[MAX_MSG_LEN];
static char TxBuf[MAX_MSGS][MAX_MSG_LEN];
static SerialTxBuf_t USART1_TX_BUFF = { 0, 0, MAX_MSGS, &TxBuf[0][0] };

//extern SerialTxBuf_t USART1_TX_BUFF;

static uint8_t usart_tx_fifo_empty(SerialTxBuf_t *buffer);
static uint8_t usart_tx_fifo_full(SerialTxBuf_t *buffer);
static uint8_t usart_tx_fifo_write(SerialTxBuf_t *buffer, char *str, uint8_t len);
static uint8_t usart_fifo_read(char *msg, SerialTxBuf_t *buffer);

static DMA_InitTypeDef DMA_InitStruct;

void USART1_Stream(char *str, uint16_t length)
{
  uint16_t len = length;
  uint16_t i = 0;
  
  if(len > MAX_MSG_LEN)
  {
    len = MAX_MSG_LEN;
  }
  
  // If stream is busy, buffer the message, and DMA interrupts will sent it later
  if(DMA_GetCmdStatus(DMA2_Stream7))
  {
//    while(str[len] && len < MAX_MSG_LEN)
//    {
//      len++;
//    }
    usart_tx_fifo_write(&USART1_TX_BUFF, str, len);
  }
  // Stream is not busy, we can send the message via DMA right now
  else
  {
    // While getting msg length, record the msg to a tmp string
    // This allows us to use the same dbg string in the main program
//    while(str[len] && len < MAX_MSG_LEN)
//    {
//      tmp_str[len] = str[len];
//      len++;
//    }
    for(i = 0; i < len; i++)
    { // Copy to local buffer so that passed in pointer to immediately be re-used
      tmp_str[i] = str[i];
    }

    // DMA config, init, clear flags, interrupt enable, stream enable, USART triggering enable
    DMA_InitStruct.DMA_Channel            = DMA_Channel_4;
    DMA_InitStruct.DMA_BufferSize         = len;
    DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;
    DMA_InitStruct.DMA_Memory0BaseAddr    = (uint32_t) &tmp_str[0];
    DMA_Init(DMA2_Stream7, &DMA_InitStruct);
    
    // Clear all DMA1 Stream 3 status flags
    DMA2->HIFCR |= 0x0000003D << 22;
    DMA_ITConfig(DMA2_Stream7, DMA_IT_TC, ENABLE);
    DMA_Cmd(DMA2_Stream7, ENABLE);
    USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
  }
  
  // IMPORTANT
  // Clear the msg that was passed into this function
  // Uncomment memset call if your sprintf() does not guarantee NULL termination
  // OR if you plan to pass in const char arrays which are not NULL terminated
  // Now another application call to this function can re-use the same char array passed in previously
  //memset(&str[0], 0, len);
}

void DMA2_Stream7_IRQHandler(void)
{
  // Clear Interrupt Flags and whatnot
  if(DMA_GetITStatus(DMA2_Stream7, DMA_IT_TCIF7) == SET)
  {
    DMA_ClearITPendingBit(DMA2_Stream7, DMA_IT_TCIF7);
    
    // Then check the msg buffer to see if a message is waiting there
    if(usart_tx_fifo_empty(&USART1_TX_BUFF))
    {
      // Buffer empty, clean up stream config
      DMA_ITConfig(DMA2_Stream7, DMA_IT_TC, DISABLE);
      DMA_Cmd(DMA2_Stream7, DISABLE);
      USART_DMACmd(USART1, USART_DMAReq_Tx, DISABLE);
      // Clear flags for good measure of cleanup
      DMA2->HIFCR |= 0x0000003D << 22;
    }
    // Message Buffer is found to have pending messages
    else
    {
      // Load next msg into static tmp_str[100]
      usart_fifo_read(&tmp_str[0], &USART1_TX_BUFF);
      
      // tmp_str[0] contains the message length, since we packed it in that way
      // DMA config, init, clear flags, stream enable, USART triggering enable - TC interrupt already enabled
      DMA_InitStruct.DMA_Channel            = DMA_Channel_4;
      DMA_InitStruct.DMA_BufferSize         = (uint32_t)tmp_str[0];
      DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;
      DMA_InitStruct.DMA_Memory0BaseAddr    = (uint32_t)&tmp_str[1];
      DMA_Init(DMA2_Stream7, &DMA_InitStruct);
      DMA2->HIFCR |= 0x0000003D << 22;
      DMA_Cmd(DMA2_Stream7, ENABLE);
      USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
    }
  }
}

void init_USART1_DMA_Tx_Stream(uint32_t baud)
{
  // Standard peripheral library routines
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
	
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// Configure USART //
	USART_InitStructure.USART_BaudRate            = baud;
	USART_InitStructure.USART_WordLength          = USART_WordLength_9b;
	USART_InitStructure.USART_StopBits            = USART_StopBits_1;
	USART_InitStructure.USART_Parity              = USART_Parity_Even;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);
	USART_Cmd(USART1, ENABLE);
  
  //USART_SendData(USART1, '\r');
  //while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
  //USART_SendData(USART1, '\n');
  //while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

	// USART GPIO Configure for PC10 and PC11
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9 | GPIO_Pin_10; // PC.10 USART3_TX, PC.11 USART3_RX
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Connect USART pins to AF //
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);  
  
  /* Set DMA options */
	DMA_InitStruct.DMA_DIR                = DMA_DIR_MemoryToPeripheral;
	DMA_InitStruct.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_MemoryInc          = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStruct.DMA_MemoryDataSize     = DMA_MemoryDataSize_Byte;
	DMA_InitStruct.DMA_Mode               = DMA_Mode_Normal;
	DMA_InitStruct.DMA_Priority           = DMA_Priority_Low;
	DMA_InitStruct.DMA_FIFOMode           = DMA_FIFOMode_Disable;
	DMA_InitStruct.DMA_FIFOThreshold      = DMA_FIFOThreshold_Full;
	DMA_InitStruct.DMA_MemoryBurst        = DMA_MemoryBurst_Single;
	DMA_InitStruct.DMA_PeripheralBurst    = DMA_PeripheralBurst_Single;	
  
  // Config NVIC and such as
  NVIC_InitStructure.NVIC_IRQChannel                   = DMA2_Stream7_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  //USART_SendData(USART1, '\n');
  //while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
}

uint8_t usart_tx_fifo_empty(SerialTxBuf_t *buf)
{
  if (buf->head == buf->tail)
  {
    return 1;
  }
  return 0;
}

uint8_t usart_tx_fifo_full(SerialTxBuf_t *buf)
{
  if(((buf->tail+1)%MAX_MSGS) == buf->head)
  {
    return 1;
  }
  return 0;
}

// Add to buffer tail, take from buffer head
uint8_t usart_tx_fifo_write(SerialTxBuf_t *buf, char *str, uint8_t len)
{
  uint8_t i = 0;
  
  if(usart_tx_fifo_full(buf))
  {
    return 0;
  }
  
  // Store length at the beginning of the string
  TxBuf[buf->tail][0] = (uint8_t)len;
  
  for(i = 0; i <= len; i++)
  {
    TxBuf[buf->tail][i+1] = str[i];
  }
  
  // Increment tail position for the next write
  // We do this last to ensure that the TC interrupt is able to decrement the buffer size when necessary
  buf->tail = (buf->tail+1) % MAX_MSGS;
  
  return 1;
}

uint8_t usart_fifo_read(char *msg, SerialTxBuf_t *buf)
{
  uint8_t i = 0;
  uint8_t len;
  
  if(usart_tx_fifo_empty(buf))
  {
    return 0;
  }
  
  len = TxBuf[buf->head][0];
  
  // Copy buffer contents out to tmp-str[100], including the length information at TxBuf[buf->head][0]
  // Loop needs to run for len+1 times to include length info
  for(i = 0; i <= len; i++)
  {
    msg[i] = TxBuf[buf->head][i];
  }
  
  // Increment head and wrap, requires power of two buffer MAXLENGTH
  buf->head = (buf->head+1) % MAX_MSGS;
  
  return 1;
}
