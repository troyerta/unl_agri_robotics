/*********************************************************************
* Includes
*********************************************************************/
#include "utils.h"
#include <stdio.h>
#include "stdlib.h"
#include "stdint.h"
#include "arm_math.h"
#include "can.h"
#include "kalman.h"
#include "core_cm4.h"

/*********************************************************************
* Defines
*********************************************************************/
#define ACTUATOR_MIN -30.0f;
#define ACTUATOR_MAX 30.0f;

static float32_t K_gains[1*3] = { 0.0f, 0.0f, 0.0f };
static arm_matrix_instance_f32 K = { 1, 3, &K_gains[0] };


/*********************************************************************
* Public Functions
*********************************************************************/
uint8_t getLqrU(arm_matrix_instance_f32 *state)
{
  float u, x1, x2, x3 = 0.0f;
  uint8_t status = 0;
  x1 = state->pData[0];
  x2 = state->pData[1];
  x3 = state->pData[2];
  
  u = (K.pData[0] * x1) + (K.pData[1] * x2) + (K.pData[2] * x3);
  
  status = f32_checkBounds(u, -30.0f, 30.0f);
  
  return status;
}




