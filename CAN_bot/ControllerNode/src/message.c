#include "main.h"
#include <math.h>
#include "usart.h"
#include "tim2.h"
#include "can.h"
#include "utils.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "stream.h"
#include "message.h"

// Used by getPos() functions to communication valid new readings to the getTrackingHeading()
// The tracking Heading function with ack the flag by setting it to zero again
static uint8_t positionIsUpdated_flag = 0;

//static char msg[150];

uint8_t setAutoSpeed(Rover_t *rover, int16_t speed)
{
  uint8_t ret = 0;
  int16_t temp;

  // Basic check on the variable to make sure it is in bounds
  if(s16_checkBounds(speed, AUTO_SPEED_MIN, AUTO_SPEED_MAX))
  {
    if(speed < AUTO_SPEED_MIN)
    {
      temp = AUTO_SPEED_MIN;
    }
    else
    {
      temp = AUTO_SPEED_MAX;
    }
    ret = 1;
  }
  else
  {
    ret = 0;
    temp = speed;
  }

  rover->auto_speed_setPoint = temp;

  return ret;
}

// These functions take input/output pointers, and return flags if data is out of range
uint8_t getGPSTime(Rover_t *rover, CanRxMsg *RxMessage)
{
  uint32_t temp = 0;
  uint8_t ret = 0;

  // Data field 4 contains time's LSByte, and Data field 1 has time's MSByte
  temp = (uint32_t)RxMessage->Data[1] << 24;
  temp |= (uint32_t)RxMessage->Data[2] << 16;
  temp |= (uint32_t)RxMessage->Data[3] << 8;
  temp |= (uint32_t)RxMessage->Data[4];

  // Basic check on the variable to make sure it is in bounds
//  if(u32_checkBounds(delta_temp, DELTATIME_MIN, DELTATIME_MAX))
//  {
//    ret = 1;
//  }
//  else
//  {
//    ret = 0;
//    rover->time = temp;
//  }

  rover->time = temp;

  return ret;
}

uint8_t getSatFix(Rover_t *rover, CanRxMsg *RxMessage)
{
  uint8_t temp = 0;
  uint8_t ret = 0;

  // Fix is in Data[0]
  temp = RxMessage->Data[0];

  // Basic check on the variable to make sure it is in bounds
  if(u8_checkBounds(temp, SATELLITEFIX_MIN, SATELLITEFIX_MAX))
  {
    ret = 1;
  }
  else
  {
    ret = 0;
    rover->fix= temp;
  }

  return ret;
}

uint8_t getCANState(Rover_t *rover, CanRxMsg *RxMessage)
{
  uint8_t ret = 0;
  static uint8_t firstRun = 1;
  uint32_t time_diff = 0;
  static uint32_t last_time_stamp = 0;
  static uint8_t soft_state = 0;

  // Basic integrator

  if(firstRun)
  {
    last_time_stamp = rover->time;
    firstRun = 0;
  }
  else
  {
    // Make sure the time data is consequetive
    time_diff = rover->time - last_time_stamp;
  }

  // Increment or decrement the integrator state
  if(time_diff > 1)
  {
     // CAN messages APPEAR to be skipped
    if(soft_state < CAN_STATE_INTEGRATION_CONST)
    {
      soft_state++;
    }
  }
  else
  {
    if(soft_state > 0)
    {
      soft_state--;
    }
  }

  // Fix the hard state if integrator reaches the threshold that indicates CAN bus problems
  if(soft_state >= CAN_STATE_INTEGRATION_CONST)
  {
    // We're missing time stamp messages, some are getting dropped
    rover->can = 0;
    time_diff = 0;
  }
  else
  {
    // GPS time messages are arrived very consequetively in time, no dropped time messages
    rover->can = 1;
  }

  last_time_stamp = rover->time;

  return ret;
}

uint8_t getXpos(Rover_t *rover, CanRxMsg *RxMessage)
{
  //static int32_t temp;
  uint8_t ret = 0;

  //uint32_t uNorth;
  int32_t north = 0;

  north = RxMessage->iData[1];

  // Basic check on the variable to make sure it is in bounds
//  if(s32_checkBounds(temp, X_MIN, X_MAX))
//  {
//    ret = 1;
//  }
//  else
//  {
//    ret = 0;
//    rover->pos.X= temp;
//  }
  positionIsUpdated_flag = 1;
  rover->pos.X= north;

  return ret;
}

uint8_t getYpos(Rover_t *rover, CanRxMsg *RxMessage)
{
  //static int32_t temp;
  uint8_t ret = 0;
  int32_t east = 0;
  //uint32_t uEast;

  east = RxMessage->iData[0];

  // Basic check on the variable to make sure it is in bounds
//  if(s32_checkBounds(temp, Y_MIN, Y_MAX))
//  {
//    ret = 1;
//  }
//  else
//  {
//    ret = 0;
//    rover->pos.Y= temp;
//  }
  positionIsUpdated_flag = 1;
  rover->pos.Y= east;

  return ret;
}

uint8_t getXvel(Rover_t *rover, CanRxMsg *RxMessage)
{
  int32_t temp = 0;
  uint8_t ret = 0;

  #ifdef FILTER_VEL
  // Data is in floating point format
  static float Q = 1;
  static float R = 100;
  static float x = 0;
  static float P = 500;

  // Intermediate values for internal calculation
  float xp = 0;
  float K  = 0;
  float Pp = 0;
  #endif

  temp |= RxMessage->Data[7];
  temp <<= 8;
  temp |= RxMessage->Data[6];
  temp <<= 8;
  temp |= RxMessage->Data[5];
  temp <<= 8;
  temp |= RxMessage->Data[4];

  #ifdef FILTER_VEL
  // Filter the speed values
  xp = x;
  Pp = P + Q;
  K = Pp * (1 / (P + R));
  x = xp + (K * (temp - xp));
  P = Pp - (K * Pp);

  temp = (int16_t)x;
  #endif

  // Basic check on the variable to make sure it is in bounds
  if(s32_checkBounds(temp, dX_MIN, dX_MAX))
  {
    ret = 1;
  }
  else
  {
    ret = 0;
    rover->dx= temp;
  }

  return ret;
}

uint8_t getYvel(Rover_t *rover, CanRxMsg *RxMessage)
{
  int32_t temp = 0;
  uint8_t ret = 0;

  #ifdef FILTER_VEL
  // Data is in floating point format
  static float Q = 1;
  static float R = 100;
  static float x = 0;
  static float P = 500;

  // Intermediate values for internal calculation
  float xp = 0;
  float K  = 0;
  float Pp = 0;
  #endif

  temp |= RxMessage->Data[3];
  temp <<= 8;
  temp |= RxMessage->Data[2];
  temp <<= 8;
  temp |= RxMessage->Data[1];
  temp <<= 8;
  temp |= RxMessage->Data[0];

  #ifdef FILTER_VEL
  // Filter the speed values
  xp = x;
  Pp = P + Q;
  K = Pp * (1 / (P + R));
  x = xp + (K * (temp - xp));
  P = Pp - (K * Pp);

  temp = (int16_t)x;
  #endif

  // Basic check on the variable to make sure it is in bounds
  if(s32_checkBounds(temp, dY_MIN, dY_MAX))
  {
    ret = 1;
  }
  else
  {
    ret = 0;
    rover->dy= temp;
  }

  return ret;
}

uint8_t getSpeed(Rover_t *rover, CanRxMsg *RxMessage)
{
  uint32_t t;
  int32_t n;
  int32_t temp = 0;
  int32_t square1 = 0;
  int32_t square2 = 0;
  uint8_t ret = 0;

  #ifdef FILTER_SPEED
  // Data is in floating point format
  static float Q = 1;
  static float R = 100;
  static float x = 0;
  static float P = 500;

  // Intermediate values for internal calculation
  float xp = 0;
  float K  = 0;
  float Pp = 0;
  #endif

  square1 = rover->dx * rover->dx;
  square2 = rover->dy * rover->dy;

  // Update this to faster version?
  /* Slow Square root */
  //temp = sqrt((square1 + square2));

  /* Newton's method (much faster) */
  t = ((square1 + square2) / 16) + 1; /* Initial guess */
  for(n = 16; n; --n)
  {
    t = ((t*t+(square1 + square2))/t)/2;
  }

  temp = (int32_t)t;

  #ifdef FILTER_SPEED
  // Filter the speed values
  xp = x;
  Pp = P + Q;
  K = Pp * (1 / (P + R));
  x = xp + (K * (temp - xp));
  P = Pp - (K * Pp);

  temp = (int16_t)x;
  #endif

  // Making the GPS speed read negative assumes an immediate rover response to "backward drive" joystick commands
  if(rover->mode == MAN_RUN && rover->joystickY < 1900 && rover->speed != 0)
  {
    temp = temp * -1;
  }

  rover->speed = temp;

  return ret;
}

uint8_t getGpsHeading(Rover_t *rover, CanRxMsg *RxMessage)
{
  uint32_t temp = 0;
  uint8_t ret   = 0;

  temp |= (uint32_t)RxMessage->Data[3] << 24;
  temp |= (uint32_t)RxMessage->Data[2] << 16;
  temp |= (uint32_t)RxMessage->Data[1] << 8;
  temp |= (uint32_t)RxMessage->Data[0];

  // Basic check on the variable to make sure it is in bounds
  if(u32_checkBounds(temp, GPS_HEADING_MIN, GPS_HEADING_MAX))
  {
    ret = 1;
  }
  else
  {
    ret = 0;
    rover->gpsHeading = temp;
  }

  return ret;
}

uint8_t getHeading(Rover_t *rover, CanRxMsg *RxMessage)
{
  uint16_t temp = 0;
  uint8_t ret   = 0;

  temp |= (uint16_t)RxMessage->Data[1] << 8;
  temp |= (uint16_t)RxMessage->Data[0];

  // Basic check on the variable to make sure it is in bounds
  if(u16_checkBounds(temp, HEADING_MIN, HEADING_MAX))
  {
    ret = 1;
  }
  else
  {
    ret = 0;
    rover->heading = temp;
  }

  return ret;
}

uint8_t getSteeringPotentiometer(Rover_t *rover, CanRxMsg *RxMessage)
{
  uint16_t temp = 0;
  uint8_t ret = 0;

  temp |= RxMessage->Data[1];
  temp <<= 8;
  temp |= RxMessage->Data[0];

  // Basic check on the variable to make sure it is in bounds
  if(u32_checkBounds(temp, STEERING_MIN, STEERING_MAX))
  {
    ret = 1;
  }
  else
  {
    ret = 0;
    rover->steering = temp;
  }

  return ret;
}

uint8_t getXTE(Rover_t *rover)
{
  // Do the entire calculation in mm
  int64_t sum1 = 0;
  int32_t diff1 = 0, diff2 = 0, diff3 = 0, diff4 = 0, diff5 = 0, diff6 = 0, diff7 = 0;
  int64_t prod1 = 0, prod2 = 0, prod3 = 0, prod4 = 0;
  int64_t Numerator = 0, Denom = 0;
  uint8_t out = 0;
  int32_t temp = 0;

  // Work on the numerator of the equation
  diff1 = rover->line.A.X/10 - rover->pos.X/10;  // ax - cx
  diff2 = rover->line.B.Y/10 - rover->pos.Y/10;  // by - cy
  diff3 = rover->line.A.Y/10 - rover->pos.Y/10;  // ay - cy
  diff4 = rover->line.B.X/10 - rover->pos.X/10;  // bx - cx
  prod1 = diff1 * diff2;       // (ax-cx)*(by-cy)
  prod2 = diff3 * diff4;       // (ay-cy)*(bx-cx)
  diff5 = prod1 - prod2;       // ((ax-cx)*(by-cy)-(ay-cy)*(bx-cx))
  Numerator = -1 * diff5;      // -1*((ax-cx)*(by-cy)-(ay-cy)*(bx-cx))

  // Compute the denominator
  diff6 = rover->line.B.Y/10 - rover->line.A.Y/10;            // by - ay
  diff7 = rover->line.B.X/10 - rover->line.A.X/10;            // bx - ax
  prod3 = diff6 * diff6;                    // (by-ay).^2
  prod4 = diff7 * diff7;                    // (bx-ax).^2
  sum1  = prod3 + prod4;                    // (by-ay).^2 + (bx-ax).^2
  //Denom = (int32_t)(Q_rsqrt((float)sum1));  // sqrt((by-ay).^2 + (bx-ax).^2)
  Denom = sqrt(sum1);

  temp = Numerator / Denom;

  if(s32_checkBounds(temp, XTE_MIN, XTE_MAX))
  {
    out = 1;
//    if(temp >= XTE_MAX)
//    {
//      rover->xte = XTE_MAX;
//    }
//    else
//    {
//      rover->xte = XTE_MIN;
//    }
    rover->xte = 10 * temp;
  }
  else
  {
    out = 0;
    //rover->xte = 10 * temp;
    rover->xte = 10 * temp;
  }

//  if( rover->xte > 0 && rover->xte < 1000)
//  {
//    rover->xte *= -1;
//  }
  //rover->xte = 10 * Numerator / Denom;

  return out;
}

// Use the last two valid GPS positions to calculate the rover heading
uint8_t getTrackHeading(Rover_t *rover)
{
  float temp = 0.0f;
  uint8_t ret = 0;
  float theta = 0.0f;
  float dX = 0.0f, dY = 0.0f;
  static float sticky_heading = 0.0f;

  // Collect the velocity vector xy components in m/S from mm/S
  dX = (float)((int32_t)rover->dx / (float)1000.0f);
  dY = (float)((int32_t)rover->dy / (float)1000.0f);

  // returns radians to theta
  theta = atan2f(dX, dY);

  // convert radians to deg
  temp = (float)-57.2957795 * theta;

  /*
  * Now we need to convert the -pi to pi output range of the
  * GCC atan2 function to a 0 to 2pi range on the polar coordinate system
  */

  // Which quadrant is our velocity vector in?
  if(dX > 0.0f && dY < 0.0f) // Q4
  {
    temp += (float)450.0f;
  }
  else // Q1, Q2, Q3
  {
    temp += (float)90.0f;
  }

  //Convert cartesian/polar angle to something relative to true north
  if(temp <= 90.0f)
  {
    temp = (float)90.0f - temp;
  }
  else
  {
    temp = (float)360.0f - (temp - (float)90.0f);
  }

  //rover->trackHeading = temp;
  // Basic check on the variable to make sure it is in bounds
//  if(f32_checkBounds(temp, BEARING_MIN, BEARING_MAX))
//  {
//    ret = 1;
//    if(temp >= BEARING_MAX)
//    {
//      rover->trackHeading = BEARING_MAX;
//    }
//    else
//    {
//      rover->trackHeading = BEARING_MIN;
//    }
//  }
//  else
//  {
//    ret = 0;
//    rover->trackHeading = temp;
//  }

  /* Use sticky heading if speed is too low to discern heading */
  if(rover->speed < 300)
  {
    rover->trackHeading = sticky_heading;
  }
  else
  {
    rover->trackHeading = temp;
    sticky_heading = temp;
  }

  return ret;
}

/*
* This function converts the waypoint coordinates into a heading relative to truth North - the
* same coordinate system that the compass and Gps heading give us. We then compute PSI as ranging
* from -180 to 180 degrees.
*
*
*
*/
uint8_t getBearing(Rover_t *rover)
{
  static float theta = 0.0f, temp = 0.0f;
  uint8_t ret = 0;

  float yDiff = (float)(rover->line.B.Y - rover->line.A.Y) / 1000.0f;
  float xDiff = (float)(rover->line.B.X - rover->line.A.X) / 1000.0f;

  theta = atan2f(xDiff, yDiff);

  if(theta < 0.0f)
  {
    theta += (float)6.283185;
  }

  temp = (float)57.2957795 * theta;

  // Basic check on the variable to make sure it is in bounds
  if(u32_checkBounds(temp, BEARING_MIN, BEARING_MAX))
  {
    ret = 1;
  }
  else
  {
    ret = 0;
    rover->bearing = temp;
  }

  rover->bearing = temp;

  return ret;
}

uint8_t getPSI(Rover_t *rover)
{
  float tempHeading = 0.0f;
  float tempBearing = 0.0f;
  float tempPsi = 0.0f;
  float fullDiff = 0.0f;
  uint8_t ret = 0;

  // Transform the heading reading we have (whichever one we choose to use),
  // by taking it to the local bearing coordinate system

  // Compass heading is in centi-degrees
  //tempHeading = (float)rover->heading / 100.0f;

  // GPS heading is in milli-degrees (and is less noisy  :) )
  tempHeading = (float)rover->trackHeading;


  // Transform vectors from a truth north scale 0-359
  // to a -180 to 180 deg range
  if( rover->bearing > 180.0f )
  {
    tempBearing = rover->bearing - 360.0f;
  }
  else
  {
    tempBearing = rover->bearing;
  }

  if( tempHeading > 180.0f )
  {
    tempHeading -= 360.0f;
  }

  fullDiff = tempHeading - tempBearing;

  if( fullDiff > 180.0f )
  {
    tempPsi = fullDiff - 360.0f;
  }
  else if( fullDiff < -180.0f )
  {
    tempPsi = fullDiff + 360.0f;
  }
  else
  {
    tempPsi  = fullDiff;
  }

  // Basic check on the variable to make sure it is in bounds
//  if(f32_checkBounds(tempPsi, PSI_MIN, PSI_MAX))
//  {
//    ret = 1;
//  }
//  else
//  {
//    ret = 0;
//    rover->psi = tempPsi;
//  }

  rover->psi = tempPsi;

  return ret;
}

// Use lookup table (statically a part of the message module) to get the current steering angle
uint8_t getDELTA(Rover_t *rover)
{
  float temp = 0.0f;
  uint8_t ret = 0;

  // Steering needs to be transformed to degrees using calibrated equation
  temp = (float)rover->steering * -0.0224f + 46.778f;;

  // Basic check on the variable to make sure it is in bounds
  if(f32_checkBounds(temp, DELTA_MIN, DELTA_MAX))
  {
    ret = 1;
    if(temp >= DELTA_MAX)
    {
      rover->delta = DELTA_MAX;
    }
    else
    {
      rover->delta = DELTA_MIN;
    }
  }
  else
  {
    ret = 0;
    rover->delta = temp;
  }

  return ret;
}

/*
* Function: getJoystick
* Params:   pointer to Rover_t, ADC readings for X and Y on the joystick, and deadband width
* Returns:  status of returned values' range
* Info:     ADC measurements are generally noisy, filter them down a bit to smooth
*           robot commands to drive and steer. We don't want noisy commands send to motors
*/
uint8_t getJoystick(Rover_t *rover, uint16_t x, uint16_t y, uint16_t margin)
{
  uint8_t ret = 0;
  uint16_t tempX = x;
  uint16_t tempY = y;

  #ifdef FILTER_JOYSTICK
  // 1D Kalman Filter to smooth the readings
  // Data is in floating point format
  static float Qx = 1;
  static float Rx = 10;
  static float xx = 0;
  static float Px = 500;

  // Data is in 16 bit signed integer format
  static float Qy = 1;
  static float Ry = 10;
  static float xy = 0;
  static float Py = 500;

  // Intermediate values for internal calculation
  float xp = 0;
  float K  = 0;
  float Pp = 0;

  // Filter the x values
  xp = xx;
  Pp = Px + Qx;
  K = Pp * (1 / (Pp + Rx));
  xx = xp + (K * (x - xp));
  Px = Pp - (K * Pp);

  // Filter the y values
  xp = xy;
  Pp = Py + Qy;
  K = Pp * (1 / (Pp + Ry));
  xy = xp + (K * (y - xp));
  Py = Pp - (K * Pp);

  tempX = (uint16_t)xx;
  tempY = (uint16_t)xy;
  #endif

  rover->joystickX = (uint16_t)tempX;
  rover->joystickY = (uint16_t)tempY;

  #ifdef JOYSTICK_DEADBAND_ENABLE
  if(tempX < (2057 + margin) && tempX > (2057 - margin))
  {
    rover->joystickX = 2048;
  }
  else
  {
    rover->joystickX = tempX;
  }
  if(tempY < (2104 + margin) && tempY > (2104 - margin))
  {
    rover->joystickY = 2048;
  }
  else
  {
    rover->joystickY = tempY;
  }
  #endif

  return ret;
}

/*
* Function: getButtonStates
* Params:   pointer to Rover_t, ADC readings for button pins, and state toggle threshold (ADC counts required to register a button press)
* Returns:  none
* Info:     Updates the Rover_t button states - here we assumed buttons are debounced, button states are used to toggle system modes
*/
uint8_t getButtonStates(Rover_t *rover, volatile uint16_t *ADCConvertedValues, uint16_t threshold)
{
  uint8_t ret = 0;

  if(ADCConvertedValues[0] > threshold) /* Remote controller buttons A-D */
  {
    rover->A = 1;
  }
  else
  {
    rover->A = 0;
  }
  if(ADCConvertedValues[1] > threshold)
  {
    rover->B = 1;
  }
  else
  {
    rover->B = 0;
  }
  if(ADCConvertedValues[2] > threshold)
  {
    rover->C = 1;
  }
  else
  {
    rover->C = 0;
  }
  if(ADCConvertedValues[3] > threshold)
  {
    rover->D = 1;
  }
  else
  {
    rover->D = 0;
  }
  if(ADCConvertedValues[4] > threshold) /* E Stop switches */
  {
    rover->E = 1;
  }
  else
  {
    rover->E = 0;
  }
  if(ADCConvertedValues[5] < threshold) /* Joystick Button has a pullup resistor, goes to GND for button press */
  {
    rover->J = 1;
  }
  else
  {
    rover->J = 0;
  }

  return ret;
}

uint8_t getStateEstimate(Rover_t *rover)
{
  uint8_t ret = 0;

  return ret;
}

uint8_t getFeedback(Rover_t *rover, float K1, float K2, float K3)
{
  uint8_t ret = 0;
  float temp = 0.0f;

  /* Supposed to be in deg/S, but
  *  XTE is int32_t in mm,
  *  PSI is float in deg,
  *  delta is int32_t in deg
  *  So, do this all in float in deg/S?
  *  Or do this in mdeg/S with int32_t?
  *  u is currently in int32_t and so is steer_cmd */

  temp = (K1 * (float)((float)rover->xte / 1000.0f)) +	(K2 * rover->psi) + (K3 * rover->delta);

  // Basic check on the variable to make sure it is in bounds
  if(f32_checkBounds(temp, U_MIN, U_MAX))
  {
    ret = 1;
    if(temp >= U_MAX)
    {
      rover->u = U_MAX;
    }
    else
    {
      rover->u = U_MIN;
    }
  }
  else
  {
    ret = 0;
    rover->u = temp;
  }
  //rover->u = temp;

  return ret;
}

/*
* Function: setSysOutputs
* Params:   pointer to Rover_t
* Returns:  flag
* Info:     Reads system operating mode, and decides on the appropriate cmds to send the motors
* Here we can load either joystick steering rates, or steering rate generated from getFeedback()
* This is the function that determines which system outputs are actually sent to the steering motor node in the feedback msg
* These are just the controller set-points for the actuators to try to match
*/
uint8_t setSysOutputs(Rover_t *rover)
{
  uint8_t ret = 0;
  static int16_t temp;

  /* Do nothing states */
  if(rover->mode == INIT || rover->mode == AUTO_RDY || rover->mode == MAN_RDY || rover->mode == ESTOP)
  {
    rover->steer_cmd      = 0;
    rover->speed_setPoint = 0;
    rover->auto_speed_cmd = 0;
  }
  /* Send joystick positions as cmds in manual mode */
  else if(rover->mode == MAN_RUN)
  {
    rover->steer_cmd      = (int16_t)(((int16_t)rover->joystickX - (int16_t)2048) * 2);  // Joystick ADC readings becomes signed result
    rover->speed_setPoint = (int16_t)(((int16_t)rover->joystickY - (int16_t)2048) * 2);
    rover->auto_speed_cmd = 0;
  }
  /* Send controller output and speed setpoint in autonomous mode */
  else if(rover->mode == AUTO_RUN)
  {
    float tempU = rover->u;

    if(tempU > U_MAX)
    {
      tempU = U_MAX;
    }
    else if(tempU < U_MIN)
    {
      tempU = U_MIN;
    }

    if(tempU < -0.11f)
    {
      temp = (int16_t)(tempU * 141.19f - 407.08f);              // from deg/S to steering motor command
    }
    else if(tempU > 0.11f)
    {
      temp = (int16_t)(tempU * 144.76f + 301.42f);
    }
    else
    {
      temp = (int16_t)0;
    }

    if(s16_checkBounds(temp, STEER_CMD_MIN, STEER_CMD_MAX))
    {
      ret = 1;
      if(temp >= STEER_CMD_MAX)
      {
        rover->steer_cmd = STEER_CMD_MAX;
      }
      else
      {
        rover->steer_cmd = STEER_CMD_MIN;
      }
    }
    else
    {
      ret = 0;
      rover->steer_cmd = temp;
    }
    //rover->steer_cmd      = (int16_t)(((int16_t)rover->joystickX - (int16_t)2048) * 2);  // Joystick ADC readings becomes signed result
    rover->speed_setPoint = 0;  // in mm/S
    rover->auto_speed_cmd = (2.0745 * rover->auto_speed_setPoint) + 100;
  }
  else    // WTF kind of mode are we in anyways? Stop the robot now!
  {
    rover->steer_cmd      = 0;
    rover->speed_setPoint = 0;
    rover->auto_speed_cmd = 0;
  }

  return ret;
}

/*
* Function: setFeedbackMessage
* Params:   pointer to Rover_t and CAN TX FIFO, and message data to add to buffer
* Returns:  none
* Info:     Loads the TX buffer with all the appropriate messages
*/
void setFeedbackMessage(Rover_t *rover, CanTxMsg *TxBufferSlot, CanTxMsg *FeedbackMsg)
{
  int16_t temp = 0;

  // Write the steering rate command
  temp = rover->steer_cmd;
  FeedbackMsg->Data[0] =  temp & 0x00FF;
  FeedbackMsg->Data[1] = (temp & 0xFF00) >> 8;

  // Write the measured GPS speed
  // This data could be modified later if we determine the speed should
  // be negative(-) for reverse manual driving
  temp = rover->speed;
  FeedbackMsg->Data[2] =  temp & 0x00FF;
  FeedbackMsg->Data[3] = (temp & 0xFF00) >> 8;

  // Write the speed set point for the drive node to follow
  temp = rover->speed_setPoint;
  FeedbackMsg->Data[4] =  temp & 0x00FF;
  FeedbackMsg->Data[5] = (temp & 0xFF00) >> 8;

  // Finally, write the steering angle set point - not used in linear SS controller
  // Mostly just used in Pure Pursuit algorithms
  temp = rover->auto_speed_cmd;
  FeedbackMsg->Data[6] =  temp & 0x00FF;
  FeedbackMsg->Data[7] = (temp & 0xFF00) >> 8;

  // Add message to a position in the TX FIFO queue
  *TxBufferSlot = *FeedbackMsg;  // Must do this each time!
}

/*
* Function: setDataMsgs
* Params:   pointer to Rover_t and CAN TX FIFO
* Returns:  none
* Info:     Fills up 4 CAN messages with things of interest to our data logger
*           Good place to TX important rover states and variables
*/
void setDataMsgs(Rover_t *rover, CanTxMsg *TxBufferSlot)
{
  uint8_t u8Temp    = 0;
  uint16_t u16Temp  = 0;
  uint32_t u32Temp  = 0;

  /* Data Msg 0 is for Control System states */

  /* "Data 0" msg
   * (x,y)        - two int32_ts in mm

   * "Data 1" msg
   * TrackHeading - float    from 0 to 360.0      , store 0-360 in two bytes, and 2 decimal places in one byte
   * PSI          - float    from -180.0 to +180.0, store 0-360 in two bytes, and 2 decimal places in one byte
   * speed        - int16_t  from -+ int range

   * "Data 2" msg
   * y            - int32_t  from -+ 32,000 mm, uses along 2 bytes in mm
   * delta        - float    from -+ 60000 mdeg   , store 0-120 in one byte, and 2 decimal places in one byte
   * dBias        - int16_t    from -+ 1000.0 mm    , store whole in two bytes, and 1 decimal place in one byte

   * "Data 3" msg
   * psiBias      - float    from -180.0 to +180.0, store 0-360 in two bytes, and 2 decimal places in one byte
   * u            - int16_t  from -+ int range
   * ADC pot      - uint16_t from -+ int range
   */

  /* Store X and Y position in "Data 0" */
  u32Temp = (uint32_t)(rover->pos.X + 2147483648);
  TxBufferSlot[0].Data[0] = (uint8_t)((u32Temp & 0xFF000000) >> 24);
  TxBufferSlot[0].Data[1] = (uint8_t)((u32Temp & 0x00FF0000) >> 16);
  TxBufferSlot[0].Data[2] = (uint8_t)((u32Temp & 0x0000FF00) >> 8);
  TxBufferSlot[0].Data[3] = (uint8_t)(u32Temp & 0x000000FF);

  u32Temp = (uint32_t)(rover->pos.Y + 2147483648);
  TxBufferSlot[0].Data[4] = (uint8_t)((u32Temp & 0xFF000000) >> 24);
  TxBufferSlot[0].Data[5] = (uint8_t)((u32Temp & 0x00FF0000) >> 16);
  TxBufferSlot[0].Data[6] = (uint8_t)((u32Temp & 0x0000FF00) >> 8);
  TxBufferSlot[0].Data[7] = (uint8_t)(u32Temp & 0x000000FF);

  /* Store TrackHeading, PSI, and Speed in "Data 1" */
  u16Temp = rover->trackHeading;  // A number from 0 to 360, decimal places truncated
  u8Temp = (uint8_t)((rover->trackHeading - u16Temp) * 100); // 2 decimal places saved
  TxBufferSlot[1].Data[0] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[1].Data[1] = (uint8_t)(u16Temp & 0x00FF);
  TxBufferSlot[1].Data[2] = u8Temp;

  u16Temp = (rover->psi + 180.0f);  // A number from -180 to 180, shifted to 0 to 360, decimal places truncated
  u8Temp = (uint8_t)(((rover->psi + 180.0f) - (float)u16Temp) * 100.0f); // 2 decimal places saved
  TxBufferSlot[1].Data[3] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[1].Data[4] = (uint8_t)(u16Temp & 0x00FF);
  TxBufferSlot[1].Data[5] = u8Temp;

  u16Temp = (uint16_t)(rover->speed + 32768);
  TxBufferSlot[1].Data[6] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[1].Data[7] = (uint8_t)(u16Temp & 0x00FF);

  /* Store xte, delta, and dBias in "Data 2" */
  u16Temp = (uint16_t)(rover->xte + 32768);
  TxBufferSlot[2].Data[0] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[2].Data[1] = (uint8_t)(u16Temp & 0x00FF);

  u8Temp = (uint16_t)(rover->delta + 60.0f);
  TxBufferSlot[2].Data[2] = u8Temp;
  u8Temp = (uint8_t)(((rover->delta + 60.0f) - (float)u8Temp) * 100.0f); // 2 decimal places saved
  TxBufferSlot[2].Data[3] = u8Temp;

  u16Temp = (uint16_t)(rover->delta_bias + 32768);
  TxBufferSlot[2].Data[4] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[2].Data[5] = (uint8_t)(u16Temp & 0x00FF);

  // Temporarily send steer_cmd for now, in bytes 0 and 1
  u16Temp = (uint16_t)(rover->steer_cmd + 32768);  // A number from -180 to 180, shifted to 0 to 360, decimal places truncated
  TxBufferSlot[2].Data[6] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[2].Data[7] = (uint8_t)(u16Temp & 0x00FF);

  /* Store PSI Bias, U, and SteeringADC in "Data3" */
  u16Temp = (rover->psi_bias + 180.0f);  // A number from -180 to 180, shifted to 0 to 360, decimal places truncated
  u8Temp = (uint8_t)(((rover->psi_bias + 180.0f) - (float)u16Temp) * 100.0f); // 2 decimal places saved
  TxBufferSlot[3].Data[0] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[3].Data[1] = (uint8_t)(u16Temp & 0x00FF);
  TxBufferSlot[3].Data[2] = u8Temp;

  /* Store floating point U signal */
  u8Temp = (uint16_t)(rover->u + 25.0f); // Store whole part
  TxBufferSlot[3].Data[3] = u8Temp;
  u8Temp = (uint8_t)(((rover->u + 25.0f) - (float)u8Temp) * 100.0f); // 2 decimal places saved
  TxBufferSlot[3].Data[4] = u8Temp;

//  u16Temp = (uint16_t)(rover->u + 32768);
//  TxBufferSlot[3].Data[3] = (uint8_t)((u16Temp & 0xFF00) >> 8);
//  TxBufferSlot[3].Data[4] = (uint8_t)(u16Temp & 0x00FF);

  u16Temp = rover->steering;
  TxBufferSlot[3].Data[5] = (uint8_t)((u16Temp & 0xFF00) >> 8);
  TxBufferSlot[3].Data[6] = (uint8_t)(u16Temp & 0x00FF);

}
