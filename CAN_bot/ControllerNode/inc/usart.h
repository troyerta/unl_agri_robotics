/*
 * usart.h
 *
 *  Created on: Oct 12, 2015
 *      Author: ttroyer2
 */

#ifndef USART_H_
#define USART_H_

#define Color_Off		(char*)"\033[0m"

// Regular Colors
#define Black				(char*)"\033[0;30m"        // Black
#define Red					(char*)"\033[0;31m"          // Red
#define Green				(char*)"\033[0;32m  "      // Green
#define Yellow			(char*)"\033[0;33m"       // Yellow
#define Blue				(char*)"\033[0;34m"         // Blue
#define Purple			(char*)"\033[0;35m"       // Purple
#define Cyan				(char*)"\033[0;36m"         // Cyan
#define White				(char*)"\033[0;37m"        // White

// Bold
#define BBlack			(char*)"\033[1;30m"       // Black
#define BRed				(char*)"\033[1;31m"         // Red
#define BGreen			(char*)"\033[1;32m"       // Green
#define BYellow			(char*)"\033[1;33m"      // Yellow
#define BBlue				(char*)"\033[1;34m"        // Blue
#define BPurple			(char*)"\033[1;35m"      // Purple
#define BCyan				(char*)"\033[1;36m"        // Cyan
#define BWhite			(char*)"\033[1;37m"       // White

// Underline
#define UBlack			(char*)"\033[4;30m"       // Black
#define URed				(char*)"\033[4;31m"         // Red
#define UGreen			(char*)"\033[4;32m"       // Green
#define UYellow			(char*)"\033[4;33m"      // Yellow
#define UBlue				(char*)"\033[4;34m"        // Blue
#define UPurple			(char*)"\033[4;35m"      // Purple
#define UCyan				(char*)"\033[4;36m"        // Cyan
#define UWhite			(char*)"\033[4;37m"       // White

// Text Background
#define On_Black		(char*)"\033[40m"       // Black
#define On_Red			(char*)"\033[41m"         // Red
#define On_Green		(char*)"\033[42m"       // Green
#define On_Yellow		(char*)"\033[43m"      // Yellow
#define On_Blue			(char*)"\033[44m"        // Blue
#define On_Purple		(char*)"\033[45m"      // Purple
#define On_Cyan			(char*)"\033[46m"        // Cyan
#define On_White		(char*)"\033[47m"       // White

// Full Background
#define Back_Blue 	(char*)"\033[44;51;37m"
 
#define none   			(char*)"\033[0m"        /* to flush the previous property */
  
#define cursor_up (char*)"\033[12A"

/********************************************************************************************************************
 *
 * Function Prototypes
 *
 *******************************************************************************************************************/
void USART2_Configuration(void);
void USART2_putc(char c);
uint8_t USART2_getc(void);

void USART3_Configuration(uint32_t baudRate);
void USART3_putc(char c);
char USART3_getc(void);
void USART3_sendString(char *str);

void USART6_Configuration(void);
void USART6_putc(char c);
uint8_t USART6_getcTimeout(uint8_t* cp);

#endif /* USART_H_ */
