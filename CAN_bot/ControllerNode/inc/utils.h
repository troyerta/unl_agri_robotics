#include "stdint.h"
#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "math.h"

#define ON		(uint32_t)1
#define OFF		(uint32_t)0
#define HIGH 	(uint32_t)1
#define LOW		(uint32_t)0

void f2s(float input, char* str, uint8_t afterpoint);
void delay(__IO uint32_t nTime);
void TimingDelay_Decrement(void);

uint32_t u8_checkBounds(uint8_t input, uint8_t min, uint8_t max);
uint32_t u16_checkBounds(uint16_t input, uint16_t min, uint16_t max);
uint32_t u32_checkBounds(uint32_t input, uint32_t min, uint32_t max);
uint32_t s8_checkBounds(int8_t input, int8_t min, int8_t max);
uint32_t s16_checkBounds(int16_t input, int16_t min, int16_t max);
uint32_t s32_checkBounds(int32_t input, int32_t min, int32_t max);
uint32_t f32_checkBounds(float input, float min, float max);
float Q_rsqrt(float number);

void LED_Setup(void);
void init_GPIOs(void);
void LED(uint32_t state);
void GPIO9(uint32_t state);
void GPIO6(uint32_t state);
void GPIO7(uint32_t state);
void init_input_pin(void);

void EXTILine3_Config(void);
void EXTILine4_Config(void);

void init_triggerPin(void);
void trigger_pin(uint32_t state);

void printDone(void);

void startSetup(void);
void endSetup(void);

void initButton(void);
uint8_t button_update(void);

int16_t map(int16_t input, int16_t minIn, int16_t maxIn, int16_t minOut, int16_t maxOut);
