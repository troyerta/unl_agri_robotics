#include "stdint.h"
#include "stm32f4xx.h"

// Public functions
void v2x_init(void);
void v2x_reset(void);
void v2x_calibrate(uint8_t SRT_STP);
uint16_t v2x_read(void);
