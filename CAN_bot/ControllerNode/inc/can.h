
#define CAN_FIFO_LEN 4

typedef struct
{
  uint8_t head;
  uint8_t tail;
  const uint8_t size;
  CanRxMsg * const buffer;
} MsgBuf_t;

uint8_t CAN1_Config(uint32_t *filterIDs);
uint8_t can_rx_fifo_empty(MsgBuf_t *buffer);
uint8_t can_rx_fifo_full(MsgBuf_t *buffer);
uint8_t can_rx_fifo_write(MsgBuf_t *buffer, CanRxMsg msg);
uint8_t can_fifo_read(CanRxMsg *msg, MsgBuf_t *buffer);
uint8_t can_rx_fifo_size(MsgBuf_t *buffer);
void can_buffer_flush(MsgBuf_t *buffer);
void can_TxMsg_init(CanTxMsg *msg, uint32_t StdId, uint32_t ExtId, uint8_t IDE, uint8_t RTR, uint8_t DLC, uint8_t Data);
void can_RxMsg_init(CanRxMsg *msg, uint32_t StdId, uint32_t ExtId, uint8_t IDE, uint8_t RTR, uint8_t DLC, uint8_t Data);

//#define MSGBUF_DEF(x,y) CanRxMsg x##_space[y]; MsgBuf_t x = {0,0,y,x##_space}
