/**
 * @file    serial_lcd.h
 * @brief   Serial LCD interface module header.
 *
 * @{
 */

#define CMD_CLR_SCREEN                0x25
#define CMD_WR_LCD_CURSOR_LOCATION    0x30
#define CMD_SET_CURSOR_INCREMENT      0x76
#define CMD_LCD_PUTC                  0x72
#define CMD_LCD_PUTS                  0x73
#define CMD_SET_FOREGROUND_COLOR      0x51
#define CMD_SET_BACKGROUND_COLOR      0x50
#define CMD_FONT_SELECT               0x44
#define CMD_LCD_PUTUINT               0x74
#define CMD_INTERFACE_TEST            0x11
#define CMD_NO_OP                     0x12
#define CMD_ESC_CMD                   0x13
#define CMD_LCD_ID                    0x14
#define CMD_RESET_LCD                 0x15
#define CMD_FONT_LIST                 0x41
#define CMD_LIST_BITMAPS_IN_ROM       0x82
#define CMD_SCROLLING_ON              0x32
#define CMD_SCROLLING_OFF             0x33
#define CMD_SLEEP_MODE                0x17
#define CMD_LCD_DISPLAY_IMG_FROM_ROM  0x80
#define CMD_BKLIGHT_CTRL              0x16
#define CMD_RD_CUR_CURSOR_LOCATION    0x31
#define CMD_READ_KEY_PRESS            0x62
#define CMD_LCD_PUTHEX                0x75
#define CMD_CLEAR_SW_BUFFER           0x60
#define CMD_PORTRAIT_MODE             0x34
#define CMD_LANDSCAPE_MODE            0x35
#define CMD_FONT_ERASE                0x45
#define CMD_ERASE_IMAGE_SPACE         0x83
#define CMD_READ_KEY_PRESS_BUFFER     0x61
#define CMD_WR_LCD_BLOCK_PARAMS       0x70
#define CMD_FILL_RECTANGLE            0x71
#define DMYBYTE                       0x00

#define MAX_MSG_SIZE 250

//Positions of Text Title to print out
#define POS_TITLE_NE_X     2
#define POS_TITLE_WA_X     2
#define POS_TITLE_WB_X     2
#define POS_TITLE_HEAD_X   2
#define POS_TITLE_XTE_X    2
#define POS_TITLE_PSI_X    2
#define POS_TITLE_DELTA_X  2
#define POS_TITLE_U_X      2
#define POS_TITLE_CAN_X    2
#define POS_TITLE_FIX_X    2
#define POS_TITLE_TIME_X   2
#define POS_TITLE_SPEED_X  80
#define POS_TITLE_MODE_X   80

#define POS_TITLE_NE_Y     1
#define POS_TITLE_WA_Y     15
#define POS_TITLE_WB_Y     29
#define POS_TITLE_HEAD_Y   43
#define POS_TITLE_XTE_Y    57
#define POS_TITLE_PSI_Y    71
#define POS_TITLE_DELTA_Y  85
#define POS_TITLE_U_Y      99
#define POS_TITLE_CAN_Y    113
#define POS_TITLE_FIX_Y    127
#define POS_TITLE_TIME_Y   141
#define POS_TITLE_SPEED_Y  85
#define POS_TITLE_MODE_Y   113

// 6 - 10 items to print out
#define POS_NORTH_X  75
#define POS_EAST_X   20
#define POS_WAX_X    26
#define POS_WAY_X    75
#define POS_WBX_X    26
#define POS_WBY_X    75
#define POS_HEAD_X   36
#define POS_XTE_X    36
#define POS_PSI_X    36
#define POS_DELTA_X  36
#define POS_FIX_X    36
#define POS_U_X      36
#define POS_CAN_X    36
#define POS_TIME_X   36
#define POS_SPEED_X  90
#define POS_MODE_X   80

#define POS_NORTH_Y  1
#define POS_EAST_Y   1
#define POS_WAX_Y    15
#define POS_WAY_Y    15
#define POS_WBX_Y    29
#define POS_WBY_Y    29
#define POS_HEAD_Y   43
#define POS_XTE_Y    57
#define POS_PSI_Y    71
#define POS_DELTA_Y  85
#define POS_U_Y      99
#define POS_CAN_Y    113
#define POS_FIX_Y    127
#define POS_TIME_Y   141
#define POS_SPEED_Y  99
#define POS_MODE_Y   127

#ifdef __cplusplus
extern "C" {
#endif
  void LCDInit(void);
  void lcd_clear(void);
  void lcd_SetCursor(uint8_t x, uint8_t y);
  void lcd_SetCursorINC(uint8_t x, uint8_t y);
  void lcd_Putc(uint8_t c);
  void lcd_Puts(char* str);
  void lcd_SetForegroundColor(uint8_t r, uint8_t g, uint8_t b);
  void lcd_SetBackgroundColor(uint8_t r, uint8_t g, uint8_t b);
  void lcd_SetFont(uint16_t fontCode);
  void lcd_Putuint(uint32_t uint);
  void lcd_UartTest(uint8_t nargs, uint8_t start);
  void lcd_NoOp(void);
  void lcd_Escape(void);
  void lcd_GetID(uint8_t* returnVal);
  void lcd_Reset(void);
  void lcd_ListFonts(void);
  void lcd_ListImages(void);
  void lcd_ScrollingOn(void);
  void lcd_ScrollingOff(void);
  void lcd_Sleep(uint8_t param);
  void lcd_DisplayImage(uint8_t imgNum);
  void lcd_BackLight(uint8_t setting);
  void lcd_GetCursor(uint8_t *x, uint8_t *y);
  uint8_t lcd_GetButtons(void);
  void lcd_Puthex(uint8_t val);
  void lcd_ClearButtons(void);
  void lcd_PortraitMode(void);
  void lcd_LandscapeMode(void);
  void lcd_EraseFonts(void);
  void lcd_EraseImages(void);
  void lcd_getNbuttons(uint8_t *returnVal, uint8_t number);
  void lcd_writeBlockParams(uint8_t xs, uint8_t xe, uint8_t ys, uint8_t ye);
  void lcd_fillRect(uint32_t size_l, uint8_t *imgData);
  void lcd_printTitles(void);
  void lcd_printNED(int32_t north, int32_t east);
  void lcd_printWaypoints(int32_t wax, int32_t way, int32_t wbx, int32_t wby);
  void lcd_printHeading(uint32_t heading);
  void lcd_printXTE(int32_t xte);
  void lcd_printPSI(int32_t psi);
  void lcd_printDELTA(int32_t delta);
  void lcd_printFix(uint32_t fix);
  void lcd_printU(int32_t u);
  void lcd_printCAN(uint32_t can);
  void lcd_printTime(uint32_t time);
  void lcd_print_background(void);
  void lcd_update(Rover_t *rover);

#ifdef __cplusplus
}
#endif


