#ifndef PINS_H_INCLUDED
#define PINS_H_INCLUDED

#define ON   1
#define OFF  0
#define HIGH 1
#define LOW  0

void LED_Setup(void);
void LED(uint32_t state);

#endif /* PINS_H_INCLUDED */
