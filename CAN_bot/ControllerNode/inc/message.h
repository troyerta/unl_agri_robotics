/* System States */
#define INIT      0
#define MAN_RDY   1
#define MAN_RUN   2
#define AUTO_RDY  3
#define AUTO_RUN  4
#define ESTOP     5

/* CAN System Message ID Definitions */
/* Should be universally used on all CAN Nodes */
#define TIME_REF_ID      100
#define LAT_ID           401
#define LON_ID           402
#define NED_POS_ID       200
#define NED_VEL_ID       201
#define NED_HEAD_ID      202
#define COMPASS_HEAD_ID  204
#define STEER_ID         206
#define SRT_LOG_ID       500
#define STP_LOG_ID       501
#define FEEDACK_ID       300
#define VEL_CMD          320
#define DATA0_ID         450
#define DATA1_ID         451
#define DATA2_ID         452
#define DATA3_ID         453

/* CAN Tx FIFO Buffer Slot Positions */
#define TX_MSG_ORDER_FIRST  0
#define TX_MSG_ORDER_SECOND 1
#define TX_MSG_ORDER_THIRD  2
#define TX_MSG_ORDER_FOURTH 3
#define TX_MSG_ORDER_FIFTH  4
#define TX_MSG_ORDER_SIXTH  5
#define NUM_MSGS            6

/* Some system options */
#define FILTER_JOYSTICK
#define FILTER_VEL
#define JOYSTICK_DEADBAND_ENABLE
#ifdef  JOYSTICK_DEADBAND_ENABLE
  #define JOYSTICK_DEADBAND_HALFWIDTH  20
#endif
#define FILTER_SPEED
#define CAN_STATE_INTEGRATION_CONST 1
#define BUTTON_ADC_THRESHOLD        2000


/* Defined Ranges for System Variable Range checks */
/* Used mostly in message.c get___(); functions */
#define DELTATIME_MIN     (uint32_t)0
#define DELTATIME_MAX     (uint32_t)10
#define SATELLITEFIX_MIN  (uint8_t)0
#define SATELLITEFIX_MAX  (uint8_t)1
#define X_MIN             (int32_t)-1000000   // + or - 1 km
#define X_MAX             (int32_t)1000000
#define Y_MIN             (int32_t)-1000000
#define Y_MAX             (int32_t)1000000
#define dX_MIN            (int32_t)-10000     // 10 m/S
#define dX_MAX            (int32_t)10000
#define dY_MIN            (int32_t)-10000
#define dY_MAX            (int32_t)10000
#define AUTO_SPEED_MIN    (int16_t)100
#define AUTO_SPEED_MAX    (int16_t)2000
#define GPS_HEADING_MIN   (int32_t)0
#define GPS_HEADING_MAX   (int32_t)360000
#define HEADING_MIN       (int32_t)0
#define HEADING_MAX       (int32_t)36000
#define STEERING_MIN      (int32_t)0
#define STEERING_MAX      (int32_t)4095
#define XTE_MIN           (int32_t)-3000
#define XTE_MAX           (int32_t)3000
#define BEARING_MIN       (float)0.0f
#define BEARING_MAX       (float)360.0f
#define PSI_MIN           (int32_t)-180.0f
#define PSI_MAX           (int32_t)180.0f
#define DELTA_MIN         (int32_t)-25
#define DELTA_MAX         (int32_t)25
#define U_MIN             (float)-25.0f
#define U_MAX             (float)25.0f
#define STEER_CMD_MIN     (int16_t)-4095
#define STEER_CMD_MAX     (int16_t)4096

/* A point defined in NED, in mm */
typedef struct NED_point {
  int32_t X;
  int32_t Y;
} NED_point_t;

/* A line defined by two points, in NED mm */
typedef struct NED_line {
  NED_point_t A;
  NED_point_t B;
} NED_line_t;

/* Rover / robot struct containing ALL important rover states */
/* All get___(); functions write to an instance of this */
/* Many variables here are derived from a select few which are collected from sensors */
typedef struct RoverData {
  uint32_t time;          // Rx'd from GPS, a 100 mS counter, zero'd out at system reset and power-up
  uint8_t fix;            // Boolean GPS fix, indicates valid GPS input
  NED_point_t pos;        // The rover position in mm NE (y,x)
  NED_line_t line;        // The bearing line that the rover is currently following, in mm NE (y,x)
  int32_t dx;             // Easting velocity vector
  int32_t dy;             // Northing velocity vector
  int16_t speed;          // Magnitude of velocity vector rx'd from GPS, dx and dy mm/S
  int16_t speed_desired;  // NOT the same thing as the setPoint. setPoints are ALWAYs commanded to the robot, _desired can only be potentially sent, depending on the current state or mode of the robot, set this one at the beginning of program execution
  int16_t speed_setPoint; // desired drive velocity cmd in mm/S
  float trackHeading;     // The heading calculated from the GPS velocity vector in degrees, relative to true North
  uint32_t gpsHeading;    // The angle that the rover is from the base station, rx'd from GPS - in polar coordinates degrees
  uint16_t heading;       // Collected from either a compass or two past GPS position samples, or current GPS velocity vector, units in degrees relative to true north
  uint16_t steering;      // The ADC value received from the steering node, used to derive delta
  float bearing;          // Angle of current line defined by two waypoints, (degrees) relative to true North
  int32_t xte;            // The all-important cross track error variable
  float psi;              // Heading error, calculated from heading and bearing
  float psi_bias;
  float delta;          // Steering angle according to O'Connor's model, and many others
  int16_t auto_speed_setPoint; // Desired drive speed, in mm/s
  int16_t auto_speed_cmd;
  int16_t delta_bias;
  float u;              // in deg/S, gets loaded into steer_cmd in linear SS controller
  uint16_t joystickX; // Collected from ADC channel
  uint16_t joystickY; // Collected from ADC Channel
  uint8_t A;
  uint8_t B;
  uint8_t C;
  uint8_t D;
  uint8_t E;
  uint8_t J;
  int16_t steer_cmd;      // in deg/S, used as final place to store the intended steering rate set point
  uint8_t can;            // binary variable indicating CAN bus activity
  uint8_t mode;           // Operation mode of the robot, filled with some macro from message.h
} Rover_t;

uint8_t setAutoSpeed(Rover_t *rover, int16_t speed);
uint8_t getGPSTime(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getSatFix(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getCANState(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getXpos(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getYpos(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getXvel(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getYvel(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getSpeed(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getTrackHeading(Rover_t *rover);
uint8_t getGpsHeading(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getHeading(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getSteeringPotentiometer(Rover_t *rover, CanRxMsg *RxMessage);
uint8_t getXTE(Rover_t *rover);
uint8_t getBearing(Rover_t *rover);
uint8_t getPSI(Rover_t *rover);
uint8_t getDELTA(Rover_t *rover);
uint8_t getJoystick(Rover_t *rover, uint16_t x, uint16_t y, uint16_t margin);
uint8_t getButtonStates(Rover_t *rover, volatile uint16_t *ADCInput, uint16_t threshold);
uint8_t getStateEstimate(Rover_t *rover);
uint8_t getFeedback(Rover_t *rover, float K1, float K2, float K3);
uint8_t setSysOutputs(Rover_t *rover);
void setFeedbackMessage(Rover_t *rover,
                        CanTxMsg *TxBufferSlot,
                        CanTxMsg *FeedbackMsg);
void setDataMsgs(Rover_t *rover, CanTxMsg *TxBufferSlot);

