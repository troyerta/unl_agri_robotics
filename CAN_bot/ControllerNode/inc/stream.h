
// Keep this as a power of 2, required by fancy head/tail wrapping
#define MAX_MSGS    32

// This can be whatever
// Any message too long will be trimmed to this length
#define MAX_MSG_LEN 256

// Structure to hide buffer from application
typedef struct
{
  uint8_t head;
  uint8_t tail;
  const uint8_t size;
  char * buffer;
} SerialTxBuf_t;

// Only two exported functions available to application
void USART3_Stream(char *str);
void init_USART3_DMA_Tx_Stream(uint32_t baud);

void USART1_Stream(char *str, uint16_t length);
void init_USART1_DMA_Tx_Stream(uint32_t baud);

